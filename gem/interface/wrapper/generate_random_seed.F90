!>\file generate_random_seed.F90
!>\brief Generate random seeds.
!!
!! @author Minwei QIAN Summer (2017) and  Deji Akingunola (2021)
!
!
 subroutine generate_random_seed(Minx, Maxx, Miny, Maxy, kount)
   use phy_put_mod
   use phys_parm_defs, only : pp_rdm_num_pert
   implicit none

   integer, intent(in) :: Minx, Maxx, Miny, Maxy
   integer, intent(in) :: kount

   integer :: i, j, k, istat
   integer :: n_size
   real, parameter :: R_MAX = 2.0**30, R_ONE = 1.0
   integer, dimension(:),     allocatable :: iseed
   real,    dimension(:,:,:), pointer     :: F_ran

   call random_seed (SIZE = n_size)
   allocate (iseed(n_size))

   iseed(:) = abs(kount) * 2 + 2 + pp_rdm_num_pert
   call random_seed(PUT = iseed(1:n_size))

   allocate(F_ran(Minx:Maxx, Miny:Maxy, 4))
   call random_number(F_ran(:, :, :))

   F_ran(Minx:Maxx, Miny:Maxy, :) = F_ran(Minx:Maxx, Miny:Maxy, :) * R_MAX

   F_ran(:, :, :) = max(F_ran(:, :, :), R_ONE)
   F_ran(:, :, :) = max(F_ran(:, :, :), R_ONE)

   istat = phy_put(F_ran, 'N_ISEEDROW', F_npath='V', F_bpath='P', F_quiet=.false.)

   return
 end subroutine generate_random_seed
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
