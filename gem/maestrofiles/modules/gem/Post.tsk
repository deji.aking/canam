#!/bin/bash

 _checkpoint 0 "GEM_module Post.tsk"

### Keep lines 5~25 although they have been moved to Sortie.tsk
### get model delt (time step of GEM run)
delt=$(rpy.nml_get Step_dt -n step -f ${SEQ_EXP_HOME}/configs/gem_settings.nml | sed 's/\.//')

### get rstr interval (this is the interval of each Runmod job)
eval $(rpy.nml_get Fcst_rstrt_S -n step -f ${SEQ_EXP_HOME}/configs/gem_settings.nml -k)
rstr=$(echo ${Fcst_rstrt_S} | tr '[:lower:]' '[:upper:]')
[[ $rstr == *D ]] && rstr=$((${rstr%D}*24)) || rstr=${rstr%H}

### get freq interval (this is the interval of each StageOUT/Sortie/Post job)
freq=$(rpy.nml_get Out3_postproc_fact -n Out -f ${SEQ_EXP_HOME}/configs/gem_settings.nml)
freq=$(echo ${freq} | tr '[:lower:]' '[:upper:]')
[[ $freq == *D ]] && freq=$((${freq%D}*24)) || freq=${freq%H}

### get steps in a full year
yearstep=$(((3600/delt)*8760))
### get steps in each freq interval
freqstep=$(((3600/delt)*freq))
### get steps in each rstr interval
rstrstep=$(((3600/delt)*rstr))

### get the last step of the current Sortie/Post slice
laststep=${Post#*_}
laststep=${laststep%^last}
laststep=$((10#$laststep))

yendstep=$(((laststep/yearstep)*yearstep))
if [[ $((laststep%yearstep)) -eq 0 || $((yearstep%rstrstep)) -ne 0 && $((laststep-freqstep)) -lt $yendstep ]]; then
  inityear=${SEQ_DATE:0:4}
  thisyear=$((inityear+yendstep/yearstep))
  thishour=$((yendstep/yearstep*8760))

  ### kludge to modify outcfg.out on-the-fly to avoid 50000 steps limitation
  for stepline in $(grep "steps=[0-9],hour," $dirconfig/outcfg.out) ; do
    hour1=$((thishour-8*8760))
    hour2=$((thishour+8*8760))
    [[ $hour1 -le 0 ]] && hour1=0

    steps=${stepline%%,*}
    sed -i "/${steps},/s/<[0-9]*.,[0-9]*.,/<${hour1},${hour2},/" $dirconfig/outcfg.out
  done

  # GEM seems has problem to read big file. Here is the kludge to update ANALYSIS (transient forcing data) every 10 years.
  # Note that GEM_anal specified in configexp.cfg also contains initial atmos and soil states
  # restart=${bemach}:${SEQ_EXP_HOME}/hub/${bemach}/work/${SEQ_DATE}/${SEQ_CONTAINER}/Runmod+000/work/cfg_0000
  #if [[ $((thisyear%10)) == 0 ]]; then
  #  bemach=$(nodeinfo -n ${SEQ_CONTAINER}/StageIN -f res | grep machine= | cut -d '=' -f 2)
  #  stagein=${bemach}:${SEQ_EXP_HOME}/hub/${bemach}/work/${SEQ_DATE}/${SEQ_CONTAINER}/StageIN/output/cfg_0000
  #  scp $(dirname ${GEM_anal#*:})/ar5emis_gu064_rcp45_${stop_yrmo}_*.rpn  ${stagein}/ANALYSIS
  #fi

  mkdir -p $dirpost
  cd $dirpost

  ### postyear and postmon are postjob and diagjob to be done
  m1=1; m2=12
  postyear=$((thisyear-1))
  for postmon in $(seq -f %02g $m1 $m2); do
    ($TASK_BIN/gem5.postmon $runid $postyear $postmon > $RUNPATH/listings/postjob_${runid}_${postyear}${postmon}.log 2>&1) &
    sleep 3
  done

  wait
else
  echo "*** This Post slice is NOT a year-end slice. Nothing has been done ***"
fi

 _checkpoint 1 "GEM_module Post.tsk"
