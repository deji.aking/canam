date0=$(date "+%s")
date1=${date0}
_checkpoint() {
   set +x
   datetmp=$(date "+%s")
   if [[ "${1:-x}" == "0" ]] ; then
      printf "\nCheckpoint: $(date '+%Y%m%d:%H%M%S') ====> ${2:-${0##*/}} Starts ###########\n\n"
   elif [[ "${1:-x}" == "1" ]] ; then
      printf "\nCheckpoint: $(date '+%Y%m%d:%H%M%S') ====> ${2:-${0##*/}} Ends  ; Elapsed = $((datetmp - date0)) (since last $((datetmp - date1))) seconds ###########\n\n"
   else
      printf "\nCheckpoint: $(date '+%Y%m%d:%H%M%S') ${1:-${0##*/}} ; Elapsed = $((datetmp - date0)) (since last $((datetmp - date1))) seconds\n\n"
   fi
   date1=${datetmp}
   set -x
}
_checkpoint 0 "GEM_module container.cfg"


export GEM_DEV_SUITE=${GEM_DEV_SUITE:-${SEQ_EXP_HOME}/..}
export PATH=${GEM_DEV_SUITE}/gem/bin/${ORDENV_PLAT}:${GEM_DEV_SUITE}/gem/bin:${PATH}
export PYTHONPATH=${GEM_DEV_SUITE}/gem/lib:${GEM_DEV_SUITE}/gem/bin:${PYTHONPATH}

USER_AFSISIO=${AFSISIO}

# Obtain GEM package and check compatibility
found_error=0
if [[ "x${GEM_version}" == "x__MY_BNDL__" ]] ; then
   
   GEM_check_version=0
   found_error=1
   message="Error: You need to specify a GEM_version, installed version number or a full path to the experiment."

elif [[ -d "${GEM_version:-__MY_BNDL__}" ]] ; then

   here=$(pwd)
   GEM_check_version=0
   if [[ -e "${GEM_version}/.setenv.dot" ]] ; then
      cd ${GEM_version}
      . ./.setenv.dot
   elif [[ -e "${GEM_version}/.ssmuse_gem" ]] ; then
      cd ${GEM_version}
      . ./.ssmuse_gem
   fi
   cd ${here}
   #TODO: check that gem was properly loaded
   
else

   default_bundle_location=/fs/ssm/eccc/mrd/rpn/MIG/GEM
   if [[ ! -e ${default_bundle_location} ]] ; then default_bundle_location=GEM ; fi
   GEM_bundle_location=${GEM_bundle_location:-${default_bundle_location}}

   if [[ -z "${ATM_MODEL_VERSION}" ]] ; then
      if [[ -n "${GEM_version}" ]] ; then
         . r.load.dot ${GEM_bundle_location}/${GEM_version} ${GEM_update}
      else
         found_error=1
         message="Error: A value for GEM_version is required if no GEM is loaded in the environment"
      fi
   else
      if [[ -n "${GEM_version}" && $(normpath.py -p ${ATM_MODEL_BNDL##*GEM/}) != $(normpath.py -p ${GEM_version}) ]] ; then
         found_error=1
         message="Error: GEM bundle ${ATM_MODEL_BNDL} loaded in environment, but ${GEM_version} requested in configuration"
      fi
   fi

fi

if [[ ${GEM_check_version:-1} -gt 0 ]] ; then
   . ${SEQ_EXP_HOME}/modules/${SEQ_MODULE}/VERSION
   if [[ ${CURRENT_VERSION} != ${GEM_MOD_VERSION} && \
            $(grep "<${CURRENT_VERSION}==${GEM_MOD_VERSION}>"  ${SEQ_EXP_HOME}/modules/${SEQ_MODULE}/.compatibility | wc -l) -eq 0 ]] ; then
      found_error=1
      message="Error: Expected Maestro module ${GEM_MOD_VERSION} for GEM/${GEM_version} but found ${CURRENT_VERSION}"
   fi
fi

if [[ ${found_error} -gt 0 ]] ; then
   echo ${message} >&2
   nodelogger -n ${SEQ_NODE} -s abort -d ${SEQ_DATE} ${SEQ_LOOP_ARGS} -m "${message}"
   exit 1
fi

echo $PATH | tr ":" "\n"

export AFSISIO=${USER_AFSISIO:-${ATM_MODEL_DFILES}}

# External interface
expname=${GEM_exp:-$(basename ${SEQ_EXP_HOME})}
MOD_GEM_settings=${GEM_settings:-undefined}
MOD_GEM_outcfg=${GEM_outcfg:-'<no value>'}
MOD_GEM_bindir=${GEM_ovbin:-release}
model_analysis=${GEM_anal:-'<no value>'}
model_inrep=${GEM_inrep:-'<no value>'}
model_input=${GEM_model_input:-'<no value>'}
MOD_GEM_restart=${GEM_model_restart:-'<no value>'}
MOD_GEM_climato=${GEM_climato:-${ATM_MODEL_DFILES}/bcmk/climato}
MOD_GEM_geophy=${GEM_geophy:-${ATM_MODEL_DFILES}/bcmk/geophy}
MOD_GEM_phy_intable=${GEM_phy_intable:-${rpnphy}/include/physics_input_table_GENERIC}
MOD_GEM_ozone=${GEM_ozone:-${AFSISIO}/datafiles/constants/ozoclim_phy45}
MOD_GEM_radtab=${GEM_radtab:-${AFSISIO}/datafiles/constants/irtab5_std}
MOD_GEM_const=${GEM_const:-${AFSISIO}/datafiles/constants/thermoconsts}
MOD_GEM_cache=${GEM_cache:-'<no value>'}
check_namelist=${GEM_check_settings:-1}
check_partition=${GEM_check_partition:-1}
user_headscript=${GEM_headscript_E:-'<no value>'}
domain_cfgs=${GEM_cfg:-'0:0'}
MOD_GEM_xfer=${GEM_xfer:-"${TRUE_HOST}:${SEQ_EXP_HOME}/hub/${TRUE_HOST}/gridpt/prog/gem"}
xfer_cmd_str=${GEM_xfer_command:-"sscp -r"}
xfer_cmd_str0=${xfer_cmd_str%% *}

model_nthreads=${GEM_nthreads:-0x0}     #number of threads within Runmod.tsk (smtdyn x smtphy)

MOD_GEM_d2z=${GEM_d2z:-1}               #run bemol to re-assemble MPI tiles
MOD_GEM_dplusp=${GEM_dplusp:-1}         #assemble dynamics and physics outputs
MOD_GEM_yyoutgrid=${GEM_yyoutgrid:-'U'} #output grid for yin-yang: U or GLB
MOD_GEM_xcasc_rep=${GEM_xcasc_rep}      #backend directory to link cascade files
MOD_GEM_prefix=${GEM_prefix:-''}        #do a move for lazy people
MOD_GEM_xferl=${GEM_xferl:-0}           #transfer model listings files
MOD_GEM_etik=${GEM_etik:-''}            #stamp ('etiket') for FST output files
MOD_GEM_clean=${GEM_clean:-0}           #module will self clean (warning: will break rerunability)
MOD_GEM_clean=0                         # this feature needs tuning
MOD_GEM_timing=${GEM_timing:-0}         #produce timing information
MOD_GEM_inorder=${GEM_inorder:-1}       #ordered listings (option -inorder to Um_run*)
MOD_GEM_save_rstrt=${GEM_save_rstrt:-0} #save restart image for each time slice
MOD_GEM_tailsubmit=${GEM_tailsubmit}
MOD_GEM_cpl_expname=${GEM_cpl_expname}  #activate coupling system
ENVFILE=${GEM_addenv:-${modelutils}/include/${BASE_ARCH}/Um_envbatch.dot} #host-specific environment definitions

# Internal definitions
export SETMEX=-ex
export MOD_GEM_debug=${GEM_debug:-0}

MOD_GEM_use_serdate=${GEM_use_serdate:-0}

# Establish multi-domain configuration
DOMAIN_number=0  #TEMPORARY - required to avoid an invalid set namespace on Linux (fix in maestro_1.4.0)
DOMAIN_start=$(printf "%04d" $(echo ${domain_cfgs} | cut -d : -f1))
DOMAIN_end=$(printf "%04d" $(echo ${domain_cfgs} | cut -d : -f2))
DOMAIN_total=$((DOMAIN_end - DOMAIN_start + 1))
DOMAIN_wide=$(echo ${domain_cfgs} | cut -d : -f3)
DOMAIN_wide=${DOMAIN_wide:-${DOMAIN_total}}

_checkpoint 1 "GEM_module container.cfg"
