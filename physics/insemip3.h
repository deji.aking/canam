#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

#if ((defined(pla) && defined(pam)) || ! defined(pla))
!
!     * jul 30, 2018 - m.lazare.     remove non-transient cpp if blocks.
!     * aug 10, 2017 - m.lazare.     no maskrow passed in call to xtemiss10.
!     * aug 07, 2017 - m.lazare.     initial git version.
!     * feb 12, 2015 - k.vonsalzen/  new version for gcm18:
!     *                m.lazare.     - modified call to new subroutine
!     *                                xtemiss10.
!     * jun 03, 2013 - k.vonsalzen/  previous version insemip2 for gcm17:
!     *                m.lazare.     - clean-up and merging with pla code.
!     *                              - revised calls to new xtemiss9,
!     *                                xtevolc4,xteob4,xteant4,xteaero4,
!     *                                xtewf5,xteob4,xtesoa3.
!     *                              - add diagnostic calculation of "ALTI".
!     *                              - removed "HISTEMI" fields.
!     * apr 28, 2012 - k.vonsalzen/  previous version insemip for gcm16.
!     *                y.peng/       - revised calls to new xtemiss8,
!     *                m.lazare.       xtevolc3,xteob3,xteant3,xteaero3,
!     *                                xttemi2,xtewf4.
!     * apr 27, 2010 - k.vonsalzen.  previous version insemi for gcm15i.
!     * apply tracer emissions.
!---------------------------------------------------------------------
!     * initialization.
!
      xemis(il1:il2,:,:)=0.
!
!---------------------------------------------------------------------
!     * calculate height and pressure.
!
      call pahgt2(pf,ph,zf,zh,throw,tfrow,shj,shtj,pressg,rgas, &
             grav,ilg,ilev,lev,il1,il2,ilevp1)
      do l=1,ilev
        dp(il1:il2,l)=pressg(il1:il2)*dshj(il1:il2,l)
      end do
      do l=1,ilev
        zfs(il1:il2,l)=zf(il1:il2,l)+phys_arrays%phisrow(il1:il2)/grav
      end do
      zfs(il1:il2,ilev+1)=0.
!
!     * save altitude.
!
      do l=2,ilev
        phys_arrays%altirow(il1:il2,l)=phys_arrays%altirow(il1:il2,l) + &
                      0.5*(zfs(il1:il2,l)+zfs(il1:il2,l-1))*saverad
      end do
      l=1
      phys_arrays%altirow(il1:il2,l)=phys_arrays%altirow(il1:il2,l) + (zfs(il1:il2,l) + &
                    0.5*(zfs(il1:il2,l)-zfs(il1:il2,l+1)))*saverad
!
!---------------------------------------------------------------------
!     * natural emissions.
!
      call xtevolc4(xemis,pressg,dshj,zf, &
               phys_arrays%escvrow,phys_arrays%ehcvrow,phys_arrays%esevrow,phys_arrays%ehevrow,phys_arrays%esvcrow, &
               phys_arrays%esverow,dsuem2,psuef2,iso2,saverad,isvchem,ipam, &
               ilg,il1,il2,ilev,lev,ntrac,dtadv)
      call xtesoa3(xemis,pressg,dshj,phys_arrays%eostrow,ipam,ogpp, &
              ioco,iocy,ilg,il1,il2,ilev,lev,ntrac,dtadv)
      call xtemiss10(xemis,xrow,ilg,il1,il2,ilev,lev,ntrac,dtadv,kount, &
                throw(1,2),gtrowbs, &
                ustarbs,phys_arrays%gtrot(1,iowat),pressg,dshj,phys_arrays%dmsorow,phys_arrays%edmsrow, &
                shj,phys_arrays%sicnrow,phys_arrays%edsorow,phys_arrays%edslrow,zspdso,zspdsbs, &
                phys_arrays%flndrow,fwatrow,fnrol, &
                phys_arrays%spotrow,phys_arrays%st01row, &
                phys_arrays%st02row,phys_arrays%st03row,phys_arrays%st04row,phys_arrays%st06row, &
                phys_arrays%st13row,phys_arrays%st14row,phys_arrays%st15row,phys_arrays%st16row, &
                phys_arrays%st17row,phys_arrays%suz0row,phys_arrays%pdsfrow,isvdust, &
                phys_arrays%fallrow,phys_arrays%fa10row,phys_arrays%fa2row,phys_arrays%fa1row, &
                phys_arrays%duwdrow,phys_arrays%dustrow,phys_arrays%duthrow,phys_arrays%usmkrow, &
                bsfrac,smfrac,phys_arrays%esdrow,ipam,sicn_crt, &
                saverad,isvchem)
!
!     * anthropogenic and anthropogenically influenced emissions.
!
#if defined emists
#ifndef pla
      call xttemi2(xemis,x2demisa,x2demiss,x2demisk,x2demisf, &
              phys_arrays%fbbcrow,phys_arrays%fairrow,pressg,dshj,zf,zfs,dtadv,levwf, &
              levair,il1,il2,lev,ilg,ilev,ntrac)
#else
      call xttemip(xemis,docem1,docem2,docem3,docem4,dbcem1, &
              dbcem2,dbcem3,dbcem4,dsuem1,dsuem2,dsuem3, &
              dsuem4,psuef1,psuef2,psuef3,psuef4, &
              phys_arrays%sairrow,phys_arrays%ssfcrow,phys_arrays%sbiorow,phys_arrays%sshirow, &
              phys_arrays%sstkrow,phys_arrays%sfirrow,phys_arrays%bairrow,phys_arrays%bsfcrow,phys_arrays%bbiorow, &
              phys_arrays%bshirow,phys_arrays%bstkrow,phys_arrays%bfirrow,phys_arrays%oairrow,phys_arrays%osfcrow, &
              phys_arrays%obiorow,phys_arrays%oshirow,phys_arrays%ostkrow,phys_arrays%ofirrow,phys_arrays%fbbcrow, &
              phys_arrays%fairrow,pressg,dshj,zf,zfs,dtadv,levwf, &
              levair,iso2,il1,il2,lev,ilg,ilev,ntrac)
#endif
#endif
!
!     * adjust tracer mixing ratios to account for emissions.
!
      xrow(il1:il2,2:lev,:)=xrow(il1:il2,2:lev,:) &
                                          +xemis(il1:il2,1:ilev,:)
!
!     * diagnose emissions.
!
#if defined xtrachem
#if defined emists
        phys_arrays%eaisrow(il1:il2)=phys_arrays%eaisrow(il1:il2)+phys_arrays%sairrow(il1:il2)*saverad
        phys_arrays%esfsrow(il1:il2)=phys_arrays%esfsrow(il1:il2)+phys_arrays%ssfcrow(il1:il2)*saverad
        phys_arrays%estsrow(il1:il2)=phys_arrays%estsrow(il1:il2)+phys_arrays%sstkrow(il1:il2)*saverad
        phys_arrays%efisrow(il1:il2)=phys_arrays%efisrow(il1:il2)+phys_arrays%sfirrow(il1:il2)*saverad
        phys_arrays%eaibrow(il1:il2)=phys_arrays%eaibrow(il1:il2)+phys_arrays%bairrow(il1:il2)*saverad
        phys_arrays%esfbrow(il1:il2)=phys_arrays%esfbrow(il1:il2)+phys_arrays%bsfcrow(il1:il2)*saverad
        phys_arrays%estbrow(il1:il2)=phys_arrays%estbrow(il1:il2)+phys_arrays%bstkrow(il1:il2)*saverad
        phys_arrays%efibrow(il1:il2)=phys_arrays%efibrow(il1:il2)+phys_arrays%bfirrow(il1:il2)*saverad
        phys_arrays%eaiorow(il1:il2)=phys_arrays%eaiorow(il1:il2)+phys_arrays%oairrow(il1:il2)*saverad
        phys_arrays%esforow(il1:il2)=phys_arrays%esforow(il1:il2)+phys_arrays%osfcrow(il1:il2)*saverad
        phys_arrays%estorow(il1:il2)=phys_arrays%estorow(il1:il2)+phys_arrays%ostkrow(il1:il2)*saverad
        phys_arrays%efiorow(il1:il2)=phys_arrays%efiorow(il1:il2)+phys_arrays%ofirrow(il1:il2)*saverad
#endif
#endif
#endif

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
