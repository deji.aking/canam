
!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xttemip(xemis,docem1,docem2,docem3,docem4,dbcem1, &
                   dbcem2,dbcem3,dbcem4,dsuem1,dsuem2,dsuem3, &
                   dsuem4,psuef1,psuef2,psuef3,psuef4, &
                   sairrow,ssfcrow,sbiorow,sshirow, &
                   sstkrow,sfirrow,bairrow,bsfcrow,bbiorow, &
                   bshirow,bstkrow,bfirrow,oairrow,osfcrow, &
                   obiorow,oshirow,ostkrow,ofirrow,fbbcrow, &
                   fairrow,pressg,dshj,zf,zfs,ztmst,levwf, &
                   levair,iso2,il1,il2,lev,ilg,ilev,ntrac)
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: levair      !< Number of vertical layers for aircraft emissions input data \f$[unitless]\f$
  integer, intent(in) :: levwf      !< Number of vertical layers for wildfire emissions input data \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: psuef4
  real, intent(in) :: ztmst
  !
  real, intent(in) :: psuef1 !< Variable description\f$[units]\f$
  real, intent(in) :: psuef2 !< Variable description\f$[units]\f$
  real, intent(in) :: psuef3 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev)  :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev)  :: zf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev + 1) :: zfs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levwf) :: fbbcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levair) :: fairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ssfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sbiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sstkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sfirrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bsfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bbiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bstkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bfirrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: oairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: osfcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: obiorow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: oshirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ostkrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ofirrow !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: docem1 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: docem2 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: docem3 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: docem4 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dbcem1 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dbcem2 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dbcem3 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dbcem4 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dsuem1 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dsuem2 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dsuem3 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev) :: dsuem4 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: ws = 32.064
  real, parameter :: wso2 = 64.059
  real, parameter :: wh2so4 = 98.073
  real, parameter :: wnh3 = 17.030
  real, parameter :: wamsul = wh2so4 + 2. * wnh3
  real, parameter :: wrat = wamsul/wso2
  real, parameter :: oc2pom = 1.4
  real, dimension(6), parameter :: zwf = [100., 500., 1000., 2000., 3000., 6000.]
  real, dimension(25), parameter :: zair = &
            [610., 1220., 1830., 2440., 3050., 3660., 4270., &
            4880., 5490., 6100., 6710., 7320., 7930., 8540., &
            9150., 9760.,10370.,10980.,11590.,12200.,12810., &
           13420.,14030.,14640.,15250. ]
  !==================================================================
  !
  real :: bbbe
  real :: bffe
  real :: dul
  real :: dz
  real :: fact
  integer :: il
  integer :: k
  integer :: l
  real :: obbe
  real :: offe
  real :: rlh
  real :: ruh
  real :: sbbe
  real :: sffe
  real :: wgt
  real :: zmax
  real :: zmin
  integer, dimension(ilg) :: llow
  integer, dimension(ilg) :: lhgh
  !
  !-------------------------------------------------------------------
  !     * check dimensions.
  !
  if (levwf  /= 6) call xit("xttemi", - 1)
  if (levair /= 25) call xit("xttemi", - 2)
  !
  !-------------------------------------------------------------------
  !     * surface emissions. insert 2d emissions into first model
  !     * layer above ground.
  !
  do il = il1,il2
    fact = ztmst * grav/(dshj(il,ilev) * pressg(il))
    !
    !       * fossil-fuel surface emissions.
    !
    sffe = max(ssfcrow(il) - sbiorow(il) - sshirow(il),0.)
    bffe = max(bsfcrow(il) - bbiorow(il) - bshirow(il),0.)
    offe = max(osfcrow(il) - obiorow(il) - oshirow(il),0.)
    xemis(il,ilev,iso2) = xemis(il,ilev,iso2) + fact * sffe * (1. - psuef2)
    dsuem2(il,ilev) = dsuem2(il,ilev) + sffe * fact/ztmst &
                      * psuef2 * wamsul/ws
    dbcem2(il,ilev) = dbcem2(il,ilev) + bffe * fact/ztmst
    docem2(il,ilev) = docem2(il,ilev) + offe * fact/ztmst * oc2pom
    !
    !       * biomass/biofuel burning emissions.
    !
    sbbe = sbiorow(il)
    bbbe = bbiorow(il)
    obbe = obiorow(il)
    xemis(il,ilev,iso2) = xemis(il,ilev,iso2) + fact * sbbe * (1. - psuef1)
    dsuem1(il,ilev) = dsuem1(il,ilev) + sbbe * fact/ztmst &
                      * psuef1 * wamsul/ws
    dbcem1(il,ilev) = dbcem1(il,ilev) + bbbe * fact/ztmst
    docem1(il,ilev) = docem1(il,ilev) + obbe * fact/ztmst * oc2pom
    !
    !       * shipping surface emissions.
    !
    sbbe = sshirow(il)
    bbbe = bshirow(il)
    obbe = oshirow(il)
    xemis(il,ilev,iso2) = xemis(il,ilev,iso2) + fact * sbbe * (1. - psuef4)
    dsuem4(il,ilev) = dsuem4(il,ilev) + sbbe * fact/ztmst &
                      * psuef4 * wamsul/ws
    dbcem4(il,ilev) = dbcem4(il,ilev) + bbbe * fact/ztmst
    docem4(il,ilev) = docem4(il,ilev) + obbe * fact/ztmst * oc2pom
  end do
  !
  !-------------------------------------------------------------------
  !     * determine layer corresponding to stack emissions (100-300m).
  !
  zmin = 100.
  zmax = 300.
  dz = zmax - zmin
  llow(il1:il2) = 0
  lhgh(il1:il2) = 0
  do l = ilev,1, - 1
    do il = il1,il2
      if (zf(il,l) <= zmin) llow(il) = l
      if (zf(il,l) <= zmax) lhgh(il) = l
    end do
  end do
  !
  !     * fossil-fuel stack emissions.
  !
  do l = 1,ilev
    do il = il1,il2
      fact = ztmst * grav/(dshj(il,l) * pressg(il))
      if (l == llow(il) .and. l == lhgh(il) ) then
        wgt = 1.
      else if (l == lhgh(il) ) then
        wgt = (zmax - zf(il,l))/dz
      else if (l > lhgh(il) .and. l < llow(l) .and. l > 1) then
        wgt = (zf(il,l - 1) - zf(il,l))/dz
      else if (l == llow(il) .and. l > 1) then
        wgt = (zf(il,l - 1) - zmin)/dz
      else
        wgt = 0.
      end if
      xemis(il,l,iso2) = xemis(il,l,iso2) + wgt * sstkrow(il) * fact &
                         * (1. - psuef2)
      dsuem2(il,l) = dsuem2(il,l) + wgt * sstkrow(il) * fact/ztmst &
                     * psuef2 * wamsul/ws
      dbcem2(il,l) = dbcem2(il,l) + wgt * bstkrow(il) * fact/ztmst
      docem2(il,l) = docem2(il,l) + wgt * ostkrow(il) * fact/ztmst * oc2pom
    end do
  end do
  !
  !-------------------------------------------------------------------
  !     * open vegetation burning emissions.
  !
  do k = 1,levwf
    ruh = zwf(k)
    if (k == 1) then
      rlh = 0.
    else
      rlh = zwf(k - 1)
    end if
    dul = ruh - rlh
    !
    !       * determine layer corresponding to emissions.
    !
    llow(il1:il2) = 0
    lhgh(il1:il2) = 0
    !
    do l = ilev,1, - 1
      do il = il1,il2
        if (zf(il,l) <= rlh)   llow(il) = l
        if (zf(il,l) <= ruh)   lhgh(il) = l
        if (rlh <= 0.)           llow(il) = ilev
      end do
    end do
    do l = 1,ilev
      do il = il1,il2
        fact = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == llow(il) .and. l == lhgh(il) ) then
          wgt = 1.
        else if (l == lhgh(il) ) then
          wgt = ( ruh - zf(il,l) )/dul
        else if (l > lhgh(il) .and. l < llow(l) .and. l > 1) then
          wgt = (zf(il,l - 1) - zf(il,l))/dul
        else if (l == llow(il) .and. l > 1) then
          wgt = ( zf(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        xemis(il,l,iso2) = xemis(il,l,iso2) + wgt * sfirrow(il) * fact &
                           * fbbcrow(il,k) * (1. - psuef1)
        dsuem1(il,l) = dsuem1(il,l) + wgt * sfirrow(il) * fact * fbbcrow(il,k) &
                       /ztmst * psuef1 * wamsul/ws
        dbcem1(il,l) = dbcem1(il,l) + wgt * bfirrow(il) * fact * fbbcrow(il,k) &
                       /ztmst
        docem1(il,l) = docem1(il,l) + wgt * ofirrow(il) * fact * fbbcrow(il,k) &
                       /ztmst * oc2pom
      end do
    end do
  end do
  !
  !-------------------------------------------------------------------
  !     * aircraft emissions.
  !
  do k = 1,levair
    ruh = zair(k)
    if (k == 1) then
      rlh = 0.
    else
      rlh = zair(k - 1)
    end if
    dul = ruh - rlh
    !
    !       * determine layer corresponding to emissions.
    !
    llow(il1:il2) = 0
    lhgh(il1:il2) = 0
    !
    do l = ilev + 1,1, - 1
      do il = il1,il2
        if (zfs(il,l) <= rlh)   llow(il) = l
        if (zfs(il,l) <= ruh)   lhgh(il) = l
        if (rlh <= 0.)           llow(il) = ilev
      end do
    end do
    do l = 1,ilev
      do il = il1,il2
        fact = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == llow(il) .and. l == lhgh(il) ) then
          wgt = 1.
        else if (l == lhgh(il) ) then
          wgt = ( ruh - zfs(il,l) )/dul
        else if (l > lhgh(il) .and. l < llow(l) .and. l > 1) then
          wgt = (zfs(il,l - 1) - zfs(il,l))/dul
        else if (l == llow(il) .and. l > 1) then
          wgt = ( zfs(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        xemis(il,l,iso2) = xemis(il,l,iso2) + wgt * sairrow(il) * fact &
                           * fairrow(il,k) * (1. - psuef3)
        dsuem3(il,l) = dsuem3(il,l) + wgt * sairrow(il) * fact * fairrow(il,k) &
                       /ztmst * psuef3 * wamsul/ws
        dbcem3(il,l) = dbcem3(il,l) + wgt * bairrow(il) * fact * fairrow(il,k) &
                       /ztmst
        docem3(il,l) = docem3(il,l) + wgt * oairrow(il) * fact * fairrow(il,k) &
                       /ztmst * oc2pom
      end do
    end do
  end do
  !
  return
end subroutine xttemip
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
