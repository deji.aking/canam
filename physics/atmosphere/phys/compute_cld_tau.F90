!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine compute_cld_tau(wrkb, clda, & ! output
                           clw, cic, rel, rei, dz, & ! input
                           wrka, cldwatmin, ilg, il1, il2, ilev, &
                           nxloc,nbs)

  !     18 apr 2018 - jason cole  initial version
  !
  ! compute the cloud-mean cloud optical thickness at 550 nm and to it add
  ! the aerosol optical thickness from the first (visible) band, aka
  ! wrka.

  implicit none

  !
  ! output
  !

  real, dimension(ilg,nbs),intent(out) :: wrkb !< CLOUD PLUS AEROSOL VISIBLE OPITCAL THICKNESS

  real, dimension(ilg),intent(out) :: clda !< TOTAL CLOUD FRACTION THAT IS CONSISTENT WITH WRKB

  !
  ! input
  !

  real, dimension(ilg,ilev,nxloc),intent(in) :: clw !< CLOUD LIQUID WATER                  (UNITS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: cic !< CLOUD ICE WATER                     (UNITS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: rel !< CLOUD LIQUID WATER EFFECTIVE RADIUS (MICRONS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: rei !< CLOUD ICE WATER EFFECTIVE DIAMETER  (MICRONS)\f$[units]\f$

  real, dimension(ilg,nbs),intent(in) :: wrka !< AEROSOL OPTICAL THICKNESS FOR EACH BAND\f$[units]\f$

  real, dimension(ilg,ilev),intent(in) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$

  real, intent(in) :: cldwatmin   !< Minimum threshold for cloud water content \f$[kg/kg]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: nxloc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! local
  !

  real, parameter :: r_one = 1.0
  real, parameter :: r_zero  = 0.0

  real, dimension(ilg) :: cumtau ! accumulate cloud optical thickness in vertical
  real, dimension(ilg) :: sumtau ! accumulate cloud optical thickness from each subcolumn
  real, dimension(ilg) :: count_cldy
  real, dimension(ilg) :: cldt

  real :: invrel
  real :: rei_loc
  real :: invrei
  real :: tau_vis
  real :: tauliqvis
  real :: tauicevis
  real :: clw_path
  real :: cic_path
  real :: r_nxloc

  integer :: i
  integer :: l
  integer :: ib
  integer :: icol

  ! initialize variables

  r_nxloc = real(nxloc)

  do ib = 1, nbs
    do i = il1, il2
      wrkb(i,ib) = r_zero
    end do ! i
  end do ! ib

  do i = il1, il2
    cldt(i)   = r_zero
    clda(i)   = r_zero
    sumtau(i) = r_zero
  end do

  do icol = 1, nxloc

    do i = il1, il2
      count_cldy(i) = r_zero
      cumtau(i)     = r_zero
    end do

    do l = 1, ilev
      do i = il1, il2

        tau_vis  = r_zero
        clw_path = clw(i,l,icol) * dz(i,l)
        cic_path = cic(i,l,icol) * dz(i,l)

        ! calculation for cldt
        if (clw_path > cldwatmin) then
          count_cldy(i) = r_one
        end if
        if (cic_path > cldwatmin) then
          count_cldy(i) = r_one
        end if

        ! calculation of cloud optical thickness

        if ((clw_path > cldwatmin  .or. &
            cic_path > cldwatmin)) then

          ! compute cloud optical thickness for this volume

          invrel = r_one/rel(i,l,icol)

          rei_loc = 1.5396 * rei(i,l,icol) ! convert to generalized dimension
          invrei  = r_one/rei_loc

          if (clw_path > cldwatmin) then
            tauliqvis = clw(i,l,icol) &
                        * (4.483e-04 + invrel * (1.501 + invrel &
                        * (7.441e-01 - invrel * 9.620e-01)))
          else
            tauliqvis = r_zero
          end if

          if (cic_path > cldwatmin) then
            tauicevis = cic(i,l,icol) &
                        * ( - 0.303108e-04 + 0.251805e+01 * invrei)
          else
            tauicevis = r_zero
          end if

          tau_vis = (tauliqvis + tauicevis) * dz(i,l)
        end if

        cumtau(i) = cumtau(i) + tau_vis

      end do ! i
    end do ! l

    do i = il1, il2
      cldt(i)   = cldt(i) + count_cldy(i)
      sumtau(i) = sumtau(i) + cumtau(i)
    end do ! i

  end do ! icol

  do ib = 1, nbs
    do i = il1, il2
      if (cldt(i) > r_zero) then
        wrkb(i,ib) = sumtau(i)/cldt(i)
      else
        wrkb(i,ib) = r_zero
      end if

      clda(i) = cldt(i)/r_nxloc

      ! add in the aerosols from band 1 (visible)
      wrkb(i,ib) = wrkb(i,ib) + wrka(i,1)
    end do ! i
  end do ! ib

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
