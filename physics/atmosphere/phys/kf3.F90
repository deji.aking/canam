!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
function kf3(rk,arg)
  !
  !     * jun 15/2013 - m.lazare.   new version for gcm17+:
  !     *                           - uses {rf3,rd3} instead of {rf2,rd2}.
  !     * sep 11/2006 - f.majaess.  previous version kf2, hard-coding
  !     *                           real(8).
  !     * mar 24/1999 - j scinocca. original version kf in "COMM".
  !
  !     * function called by gwlooku
  !     * used to evaluate incomplete elliptic integrals
  !     * based on ellf of numerical recipes
  !
  implicit none
  real*8, intent(in) :: rk !< Variable description\f$[units]\f$
  real*8, intent(in) :: arg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  real*8 :: kf3
  real*8 :: x
  real*8 :: y
  real*8 :: z
  real*8 :: rksq

  real*8 :: rf3
  external rf3
  !==============================================================
  rksq = rk ** 2

  x = cos(arg) ** 2
  y = 1.e0 - rksq * sin(arg) ** 2
  z = 1.e0

  kf3 = sin(arg) * rf3(x,y,z)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
