subroutine tkelabt(ltket,ilev,im)

  !     * sep 13/20 - m.lazare. new routine for tke.
  !
  !     * defines level index values for tke tiled field "XLMPAT".
  !
  implicit none

  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: im !<
  integer :: l !<
  integer :: m !<
  integer :: mltk !<

  integer, intent(inout), dimension(im*ilev) :: ltket !<
  !-----------------------------------------------------------------------
  mltk=0
  do l=1,ilev
    do m=1,im
      mltk        = mltk+1
      ltket(mltk) = 1000*l + m
    end do
  end do ! loop 100

  return
end
