!> \file
!>\brief Longwave radiative transfer calculation accounting for subgrid-scale horizontal variability.
!!
!! @author Jiangnan Li
!
subroutine lwtran4(fu, fd, slwf, tauci, omci, gci, fl, taual, &
                   taug, bf, bs, urbf, dbf, em0, cld, cldm, anu, &
                   nct, ncd, ncu, ncum, ncdm, lev1, &
                   cut, maxc, il1, il2, ilg, lay, lev)
  !
  !     * feb 11,2009 - j.cole.   new version for gcm15h:
  !     *                         - correct bug in specification of abse0.
  !     * dec 05,2007 - m.lazare. previous version lwtran3 for gcm15g:
  !     *                         - s,emisw,scatbk,scatfw,term1,s1,t
  !     *                           now internal work arrays.
  !     *                         - support added for emissivity<1.
  !     * nov 22/2006 - m.lazare. previous version lwtran2 for gcm15f:
  !     *                         - bound of 1.e-10 used for 1-cld
  !     *                           instead of subtracting an epsilon.
  !     *                         - bound of 1.e-10 used for denominator
  !     *                           for ssalb.
  !     *                         - work arays for this routine now
  !     *                           are automatic arrays and their
  !     *                           space is not passed in.
  !     * may 14,2003 - j.li.     previous version lwtran up to gcm15e.
  !----------------------------------------------------------------------c
  !     calculation of longwave radiative transfer using absorption      c
  !     approximation. the finite cloud effect is properly considered    c
  !     with random and full overlap assumption. cloud subgrid           c
  !     variability is included (based on li, 2002 jas p3302; li and     c
  !     barker jas p3321).                                               c
  !                                                                      c
  !     fu:     upward infrared flux                                     c
  !     fd:     downward infrared flux                                   c
  !     slwf:   input solar flux at model top level for each band        c
  !     tauci:  cloud optical depth for the infrared                     c
  !     omci:   cloud single scattering albedo times optical depth       c
  !     gci:    cloud asymmetry factor times omci                        c
  !     fl:     square of cloud asymmetry factor                         c
  !     taual:  aerosol optical depth for the infrared                   c
  !     taug:   gaseous optical depth for the infrared                   c
  !     bf:     blackbody intensity integrated over each band at each    c
  !             level in units w / m^2 / sr. therefor a pi factor needed c
  !             for flux                                                 c
  !     bs:     the blackbody intensity at the surface.                  c
  !     urbf:   u times the difference of log(bf) for two neighbor       c
  !             levels used for exponential source function              c
  !     dbf:    difference of bf for two neighbor levels used for        c
  !             linear source function                                   c
  !     em0:    surface emission                                         c
  !     cld:    cloud fraction                                           c
  !     cldm:   maximum portion in each cloud block, in which the exact  c
  !             solution for subgrid variability is applied li and       c
  !             barker jas p3321).                                       c
  !     anu:    nu factor for cloud subgrid variability                  c
  !     s:      total scattering                                         c
  !     scatbk: backward scattering                                      c
  !     scatfw: forward scattering                                       c
  !     scatsm: internal scattering                                      c
  !     taum:   taum(1) a factor related to tau in zeta factor for       c
  !             linear source term; taum(2) the cumulated taum(1) for    c
  !             subgrid variability calculation                          c
  !     xu:     the emission part in the upward flux transmission        c
  !             (li, 2002 jas p3302)                                     c
  !     xd:     the emission part in the downward flux transmission      c
  !     dtr:    direct transmission                                      c
  !     fy:     upward flux for pure clear portion (1) and pure cloud    c
  !             portion (2)                                              c
  !     fx:     the same as fy but for the downward flux                 c
  !     fw:     a term for transfer within cldm                          c
  !     nct:    the highest cloud top level for the longitude and        c
  !             latitude loop (ilg)                                      c
  !     ncd:    layer inside a cloud block accounted from cloud top      c
  !     ncu:    layer inside a cloud block accounted from cloud bottom   c
  !     ncum:   maximum loop number cloud vertical correlation accounted c
  !             from lower level to higher level                         c
  !     ncdm:   maximum loop number cloud vertical correlation accounted c
  !             from higher level to lower level                         c
  !----------------------------------------------------------------------c
  implicit none
  real :: abse0
  real :: anutau
  real :: bkins
  real :: cow
  real, intent(in) :: cut
  real :: dtr2
  real :: dtrgw
  real :: embk
  real :: embs
  real :: epsd
  real :: epsu
  real :: fmbk
  real :: fwins
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: kk
  integer :: km1
  integer :: km2
  integer :: km3
  integer :: kp1
  integer :: kp2
  integer :: kx
  integer :: kxm
  integer :: l1
  integer :: l2
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer, intent(in) :: maxc
  integer :: nanu
  real :: p1
  real :: p2
  real :: rtaul1
  real :: ru
  real :: sanu
  real :: sf
  real :: ssalb
  real :: sx
  real :: sy
  real :: tau2
  real :: taudtr
  real :: taul1
  real :: taul2
  real :: ubeta
  real :: w
  real :: wgrcow
  real :: wt
  real :: x
  real :: x2
  real :: x3
  real :: y
  real :: y2
  real :: y3
  real :: z
  real :: zeta
  !
  real, intent(inout), dimension(ilg,2,lev) :: fu !< Upward infrared flux \f$[W/m^2]\f$
  real, intent(inout), dimension(ilg,2,lev) :: fd !< Downward infrared flux \f$[W/m^2]\f$

  real, intent(in), dimension(ilg,lay)      :: tauci !< Cloud optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: omci !< Cloud single sacttering albedo \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: gci !< Cloud symmetry factor \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: fl !< Cloud optical depth scaling factor \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: taual !< Aerosol optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: taug !< Gaseous optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lev)      :: bf !< Blackbody intensity integrated 
                                                  !! over each band at each level \f$[W/m^2/sr]\f$
  real, intent(in), dimension(ilg,lay)      :: urbf !< Diffuse factor times the difference of
                                                    !! log(BF) for two neighbor levels used in
                                                    !! exponential source function \f$[units]\f$
  real, intent(in), dimension(ilg,lay)      :: dbf !< Difference of bf for two neighbor levels
                                                   !! used in linear source function \f$[W/m^2/sr]\f$
  real, intent(in), dimension(ilg,lay)      :: cld !< Maximum portion in each cloud block \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: cldm !< Maximum portion in each cloud block \f$[1]\f$
  real, intent(in), dimension(ilg,lay)      :: anu !< \f$\nu\f$ factor for horizontal variability \f$[1]\f$

  real, intent(in), dimension(ilg)          :: em0 !< Surface emisivity \f$[1]\f$
  real, intent(in), dimension(ilg)          :: bs !< Blackbody intensity integrated 
                                                  !! over each band at each level \f$[W/m^2/sr]\f$
  real, intent(in), dimension(ilg)          :: slwf !< Input solar flux at model top level \f$[W/m^2]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  real, dimension(ilg,4,lay) :: scatsm
  real, dimension(ilg,4,lay) :: taum
  real, dimension(ilg,4,lay) :: xd
  real, dimension(ilg,4,lay) :: xu
  real, dimension(ilg,4,lay) :: dtr
  real, dimension(ilg,4,lev) :: fx
  real, dimension(ilg,4,lev) :: fy
  real, dimension(ilg,4,lev) :: fw
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: s
  real, dimension(ilg,lay) :: emisw
  real, dimension(ilg,lay) :: scatbk
  real, dimension(ilg,lay) :: scatfw
  real, dimension(ilg) :: term1
  real, dimension(ilg) :: s1
  real, dimension(ilg) :: t
  integer, intent(in), dimension(ilg)     :: nct !< Level index for highest cloud top \f$[unitless]\f$
  integer, intent(in), dimension(ilg,lay) :: ncd !< Level index inside cloud block, counted from top \f$[unitless]\f$
  integer, intent(in), dimension(ilg,lay) :: ncu !< Level index inside cloud block, counted from bottom \f$[unitless]\f$
  integer, intent(in), dimension(lay)     :: ncum !< Maxmimum loop number cloud vertical correlation 
                                                  !! counting from lower level to higher level \f$[unitless]\f$
  integer, intent(in), dimension(lay)     :: ncdm !< Maxmimum loop number cloud vertical correlation 
                                                  !! counting from higher level to lower level \f$[unitless]\f$
  !
  data  ru / 1.6487213 /
  !
  !----------------------------------------------------------------------c
  !     initialization for first layer. calculate the downward flux in   c
  !     the second layer                                                 c
  !     combine the optical properties for the infrared,                 c
  !     1, aerosol + gas; 2, cloud + aerosol + gas.                      c
  !     fd (fu) is down (upward) flux, fx (fy) is the incident flux      c
  !     above (below) the considered layer.                              c
  !     gaussian integration and diffusivity factor, ru (li jas 2000)    c
  !     above maxc, exponential source function is used                  c
  !     below maxc, linear source function is used                       c
  !----------------------------------------------------------------------c
  !
  l1 =  lev1
  l2 =  lev1 + 1
  do i = il1, il2
    fd(i,1,lev1)              =  slwf(i)
    fd(i,2,lev1)              =  slwf(i)
    fx(i,1,lev1)              =  fd(i,1,lev1)
    fx(i,2,lev1)              =  fd(i,2,lev1)
  end do ! loop 100
  !
  do k = l2, maxc
    km1 = k - 1
    do i = il1, il2
      taul1                   =  taual(i,km1) + taug(i,km1)
      rtaul1                  =  taul1 * ru
      dtr(i,1,km1)            =  exp ( - rtaul1)
      ubeta                   =  urbf(i,km1) / (taul1 + 1.e-20)
      epsd                    =  ubeta + 1.0
      epsu                    =  ubeta - 1.0
      !
      if (abs(epsd) > 0.001) then
        xd(i,1,km1)           = (bf(i,k) - bf(i,km1) * &
                                dtr(i,1,km1)) / epsd
      else
        xd(i,1,km1)           =  rtaul1 * bf(i,km1) * dtr(i,1,km1)
      end if
      !
      if (abs(epsu) > 0.001) then
        xu(i,1,km1)           = (bf(i,k) * dtr(i,1,km1) - &
                                bf(i,km1)) / epsu
      else
        xu(i,1,km1)           =  rtaul1 * bf(i,k) * dtr(i,1,km1)
      end if
      !
      fd(i,1,k)               =  fd(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
      fd(i,2,k)               =  fd(i,2,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
      fx(i,1,k)               =  fd(i,2,k)
      fx(i,2,k)               =  fd(i,2,k)
    end do ! loop 125
  end do ! loop 150
  !
  !----------------------------------------------------------------------c
  !     add the layers downward from the second layer to the surface.    c
  !     determine the xu for the upward path.                            c
  !     using exponential source function for clr flux calculation and   c
  !     also for all sky flux in cloud free layers.                      c
  !----------------------------------------------------------------------c
  !
  if (maxc < lev) then
    do k = maxc + 1, lev
      km1 = k - 1
      km2 = km1 - 1
      do i = il1, il2
        taul1                 =  taual(i,km1) + taug(i,km1)
        rtaul1                =  taul1 * ru
        dtr(i,1,km1)          =  exp ( - rtaul1)
        !
        ubeta                 =  urbf(i,km1) / (taul1 + 1.e-20)
        epsd                  =  ubeta + 1.0
        epsu                  =  ubeta - 1.0
        !
        if (abs(epsd) > 0.001) then
          xd(i,1,km1)         = (bf(i,k) - bf(i,km1) * &
                                dtr(i,1,km1)) / epsd
        else
          xd(i,1,km1)         =  rtaul1 * bf(i,km1) * dtr(i,1,km1)
        end if
        !
        if (abs(epsu) > 0.001) then
          xu(i,1,km1)         = (bf(i,k) * dtr(i,1,km1) - &
                                bf(i,km1)) / epsu
        else
          xu(i,1,km1)         =  rtaul1 * bf(i,k) * dtr(i,1,km1)
        end if
        !
        fd(i,1,k)             =  fd(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
        if (cld(i,km1) < cut) then
          fd(i,2,k)           =  fd(i,2,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
          fx(i,1,k)           =  fd(i,2,k)
          fx(i,2,k)           =  fd(i,2,k)
        else
          taul2               =  tauci(i,km1) + taug(i,km1)
          ssalb               =  omci(i,km1) / (taul2 + 1.e-10)
          sf                  =  ssalb * fl(i,km1)
          w                   = (ssalb - sf) / (1.0 - sf)
          cow                 =  1.0 - w
          taum(i,1,km1)       = (cow * taul2 * (1.0 - sf) + &
                                taual(i,km1)) * ru
          zeta                =  dbf(i,km1) / taum(i,1,km1)
          tau2                =  taum(i,1,km1) + taum(i,1,km1)
          !
          sanu                =  anu(i,km1)
          x                   =  sanu / (sanu + taum(i,1,km1))
          y                   =  sanu / (sanu + tau2)
          !
          if (sanu <= 0.50) then
            dtr(i,2,km1)      =  sqrt(x)
            dtr2              =  sqrt(y)
            emisw(i,km1)      =  zeta * (sqrt(1.0 + tau2) - 1.0)
            embk              =  1.0 - sqrt(1.0 + tau2 + tau2)
          else if (sanu > 0.50 .and. sanu <= 1.0) then
            wt                =  2.0 * sanu - 1.0
            sx                =  sqrt(x)
            sy                =  sqrt(y)
            dtr(i,2,km1)      =  sx + (x - sx) * wt
            dtr2              =  sy + (y - sy) * wt
            p1                =  sqrt(1.0 + tau2) - 1.0
            emisw(i,km1)      =  zeta * (p1 + (log(1.0 + &
                                taum(i,1,km1)) - p1) * wt)
            p2                =  1.0 - sqrt(1.0 + tau2 + tau2)
            embk              =  p2 - (log(1.0 + tau2) + p2) * wt
          else if (sanu > 1.0 .and. sanu <= 2.0) then
            wt                =  sanu - 1.0
            dtr(i,2,km1)      =  x + (x * x - x) * wt
            dtr2              =  y + (y * y - y) * wt
            z                 =  sanu / (sanu - 1.0)
            p1                =  log(1.0 + taum(i,1,km1))
            emisw(i,km1)      =  zeta * (p1 + (z * (1.0 - x) - p1) * &
                                wt)
            p2                =  - log(1.0 + tau2)
            embk              =  p2 + (z * (y - 1.0) - p2) * wt
          else if (sanu > 2.0 .and. sanu <= 3.0) then
            x2                =  x * x
            y2                =  y * y
            wt                =  sanu - 2.0
            dtr(i,2,km1)      =  x2 + (x * x2 - x2) * wt
            dtr2              =  y2 + (y * y2 - y2) * wt
            z                 =  sanu / (sanu - 1.0)
            emisw(i,km1)      =  z * zeta * &
                                (1.0 - x + (x - x2) * wt)
            embk              =  z * (y - 1.0 + (y2 - y) * wt)
          else if (sanu > 3.0 .and. sanu <= 4.0) then
            x2                =  x * x
            y2                =  y * y
            x3                =  x2 * x
            y3                =  y2 * y
            wt                =  sanu - 3.0
            dtr(i,2,km1)      =  x3 + (x2 * x2 - x3) * wt
            dtr2              =  y3 + (y2 * y2 - y3) * wt
            z                 =  sanu / (sanu - 1.0)
            emisw(i,km1)      =  z * zeta * &
                                (1.0 - x2 + (x2 - x3) * wt)
            embk              =  z * (y2 - 1.0 + (y3 - y2) * wt)
            !
            !----------------------------------------------------------------------c
            !     for anu > 4, the inhomoeneity effect is very weak, for saving    c
            !     the integer :: anu is assumed. for anu > 20, homogenous is assumed  c
            !----------------------------------------------------------------------c
            !
          else if (sanu > 4.0 .and. sanu <= 20.0) then
            nanu              =  nint(sanu)
            dtr(i,2,km1)      =  x ** nanu
            dtr2              =  y ** nanu
            z                 =  sanu / (sanu - 1.0)
            emisw(i,km1)      =  z * zeta * (1.0 - dtr(i,2,km1) / x)
            embk              =  z * (dtr2 / y - 1.0)
          else
            dtr(i,2,km1)      =  exp( - taum(i,1,km1))
            dtr2              =  exp( - tau2)
            z                 =  sanu / (sanu - 1.0)
            emisw(i,km1)      =  z * zeta * (1.0 - dtr(i,2,km1) / x)
            embk              =  z * (dtr2 / y - 1.0)
          end if
          !
          xd(i,2,km1)         =  bf(i,k) - bf(i,km1) * &
                                dtr(i,2,km1) - emisw(i,km1)
          xu(i,2,km1)         =  bf(i,km1) - bf(i,k) * &
                                dtr(i,2,km1) + emisw(i,km1)
          !
          wgrcow              =  w * gci(i,km1) / cow
          taudtr              =  taum(i,1,km1) * dtr(i,2,km1)
          !
          scatfw(i,km1)       =  wgrcow * x * taudtr
          scatbk(i,km1)       =  0.5 * wgrcow * (dtr2 - 1.0)
          !
          x                   =  wgrcow * (2.0 * emisw(i,km1) + &
                                (0.5 * embk - taudtr) * zeta)
          scatsm(i,1,km1)     =  - scatbk(i,km1) * bf(i,k) - &
                                scatfw(i,km1) * bf(i,km1) - x
          scatsm(i,2,km1)     =  - scatbk(i,km1) * bf(i,km1) - &
                                scatfw(i,km1) * bf(i,k) + x
          !
          if (k == l2) then
            fx(i,1,k)         =  fx(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
            fx(i,2,k)         =  fx(i,2,km1) * dtr(i,2,km1) + &
                                xd(i,2,km1)
          else if (cld(i,km1) <= cld(i,km2)) then
            fx(i,1,k)         = ( fx(i,2,km1) + (1.0 - cld(i,km2)) / &
                                (max(1.0 - cld(i,km1),1.e-10)) * &
                                (fx(i,1,km1) - fx(i,2,km1)) ) * &
                                dtr(i,1,km1) + xd(i,1,km1)
            fx(i,2,k)         =  fx(i,2,km1) * dtr(i,2,km1) + &
                                xd(i,2,km1)
          else if (cld(i,km1) > cld(i,km2)) then
            fx(i,1,k)         =  fx(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
            fx(i,2,k)         = (fx(i,1,km1) + &
                                cld(i,km2) / cld(i,km1) * &
                                (fx(i,2,km1) - fx(i,1,km1))) * &
                                dtr(i,2,km1) + xd(i,2,km1)
          end if
          !
          fd(i,2,k)           =  fx(i,1,k) + cld(i,km1) * &
                                (fx(i,2,k) - fx(i,1,k))
        end if
      end do ! loop 225
    end do ! loop 250
  end if
  !
  !----------------------------------------------------------------------c
  !     initialization for surface                                       c
  !----------------------------------------------------------------------c
  !
  k = lev - 1
  do i = il1, il2
    embs                      =  em0(i) * bs(i)
    abse0                     =  1.0 - em0(i)
    fu(i,1,lev)               =  embs + abse0 * fd(i,1,lev)
    fy(i,1,lev)               =  embs + abse0 * fx(i,1,lev)
    fy(i,2,lev)               =  embs + abse0 * fx(i,2,lev)
    fu(i,2,lev)               =  fy(i,1,lev) + cld(i,k) * &
                                (fy(i,2,lev) - fy(i,1,lev))
    fw(i,2,lev)               =  fy(i,2,lev)
    !
    !----------------------------------------------------------------------c
    !     determining the upward flux for the first lay above surface      c
    !----------------------------------------------------------------------c
    !
    fu(i,1,k)                 =  fu(i,1,lev) * dtr(i,1,k) + &
                                xu(i,1,k)
    fy(i,1,k)                 =  fy(i,1,lev) * dtr(i,1,k) + &
                                xu(i,1,k)
    !
    if (cld(i,k) < cut) then
      s(i,k)                  =  0.0
      fy(i,2,k)               =  fy(i,2,lev) * dtr(i,1,k) + &
                                xu(i,1,k)
      fu(i,2,k)               =  fy(i,1,k)
      fw(i,2,k)               =  fy(i,2,k)
      taum(i,2,k)             =  0.0
    else
      s(i,k)                  =  scatbk(i,k) * fx(i,2,k) + &
                                scatfw(i,k) * fy(i,2,lev) + &
                                scatsm(i,2,k)
      !
      fy(i,2,k)               =  fy(i,2,lev) * dtr(i,2,k) + &
                                xu(i,2,k) + s(i,k)
      fu(i,2,k)               =  fy(i,1,k) + cld(i,k) * &
                                (fy(i,2,k) - fy(i,1,k))
      fw(i,2,k)               =  fy(i,2,k)
      taum(i,2,k)             =  taum(i,1,k)
    end if
  end do ! loop 300
  !
  !----------------------------------------------------------------------c
  !     add the layers upward from the second layer to maxc              c
  !     scattering effect for upward path is included                    c
  !----------------------------------------------------------------------c
  !
  do k = lev - 2, maxc, - 1
    kp1 = k + 1
    kp2 = k + 2
    do i = il1, il2
      if (k >= nct(i)) then
        fu(i,1,k)             =  fu(i,1,kp1) * dtr(i,1,k) + &
                                xu(i,1,k)
        !
        if (cld(i,k) < cut) then
          fu(i,2,k)           =  fu(i,2,kp1) * dtr(i,1,k) + &
                                xu(i,1,k)
          fy(i,1,k)           =  fu(i,2,k)
          fy(i,2,k)           =  fu(i,2,k)
          fw(i,2,k)           =  fu(i,2,k)
          taum(i,2,k)         =  0.0
        else
          !
          !----------------------------------------------------------------------c
          !     fy(i,2,k) contains unperturbed + backward scattering effect +    c
          !     forward scattering effect + internal scattering effect           c
          !    (li and fu, jas 2000)                                             c
          !----------------------------------------------------------------------c
          !
          if (cld(i,k) <= cld(i,kp1) .or. &
              cld(i,k) - cldm(i,k) < cut) then
            !
            fy(i,1,k)         = ( fy(i,2,kp1) + (1.0 - cld(i,kp1)) / &
                                (max(1.0 - cld(i,k),1.e-10)) * &
                                (fy(i,1,kp1) - fy(i,2,kp1)) ) * &
                                dtr(i,1,k) + xu(i,1,k)
            t(i)              =  fy(i,2,kp1)
          else
            fy(i,1,k)         =  fy(i,1,kp1) * dtr(i,1,k) + &
                                xu(i,1,k)
            t(i)              =  fy(i,1,kp1) + &
                                (cld(i,kp1) - cldm(i,kp1)) / &
                                (cld(i,k) - cldm(i,k)) * &
                                (fy(i,2,kp1) - fy(i,1,kp1))
          end if
          !
          bkins               =  scatbk(i,k) * fx(i,2,k) + &
                                scatsm(i,2,k)
          fy(i,2,k)           =  t(i) * (dtr(i,2,k) + scatfw(i,k)) + &
                                xu(i,2,k) + bkins
          taum(i,2,k)         =  taum(i,2,kp1) + taum(i,1,k)
          s1(i)               =  0.0
          s(i,k)              =  bkins + scatfw(i,k) * fy(i,2,kp1)
          term1(i)            =  0.0
          !
          if (ncu(i,k) > 1) then
            kx = k + ncu(i,k)
            kxm = kx - 1
            !
            sanu              =  anu(i,kxm)
            anutau            =  sanu / (sanu + taum(i,2,k))
            if (sanu <= 0.50) then
              dtrgw           =  sqrt(anutau)
            else if (sanu > 0.50 .and. sanu <= 1.0) then
              x               =  sqrt(anutau)
              dtrgw           =  x + 2.0 * (sanu - 0.50) * &
                                (anutau - x)
            else if (sanu > 1.0 .and. sanu <= 2.0) then
              dtrgw           =  anutau + (sanu - 1.0) * anutau * &
                                (anutau - 1.0)
            else if (sanu > 2.0 .and. sanu <= 3.0) then
              x               =  anutau * anutau
              dtrgw           =  x + (sanu - 2.0) * x * &
                                (anutau - 1.0)
            else if (sanu > 3.0 .and. sanu <= 4.0) then
              x               =  anutau * anutau * anutau
              dtrgw           =  x + (sanu - 3.0) * x * &
                                (anutau - 1.0)
            else if (sanu > 4.0 .and. sanu <= 20.0) then
              dtrgw           =  anutau ** (nint(sanu))
            else
              dtrgw           =  exp( - taum(i,2,k))
            end if
            !
            term1(i)          = (fw(i,2,kx) - bf(i,kx)) * dtrgw
            s1(i)             = (emisw(i,kp1) + s(i,kp1)) * &
                                dtr(i,2,k)
          end if
        end if
      end if
    end do ! loop 400
    !
    !----------------------------------------------------------------------c
    !     determining the terms going into the correlation calculations    c
    !     for subgrid variability for cldm portion.                        c
    !----------------------------------------------------------------------c
    !
    if (ncum(k) > 2) then
      do kk = kp2, k + ncum(k) - 1
        do i = il1, il2
          if (k >= nct(i) .and. cld(i,k) >= cut .and. &
              ncu(i,k) > 2 .and. kk <= k + ncu(i,k) - 1) then
            !
            sanu                =  anu(i,kk)
            anutau              =  sanu / (sanu + &
                                  taum(i,2,k) - taum(i,2,kk))
            if (sanu <= 0.50) then
              dtrgw             =  sqrt(anutau)
            else if (sanu > 0.50 .and. sanu <= 1.0) then
              x                 =  sqrt(anutau)
              dtrgw             =  x + 2.0 * (sanu - 0.50) * &
                                  (anutau - x)
            else if (sanu > 1.0 .and. sanu <= 2.0) then
              dtrgw             =  anutau + (sanu - 1.0) * anutau * &
                                  (anutau - 1.0)
            else if (sanu > 2.0 .and. sanu <= 3.0) then
              x                 =  anutau * anutau
              dtrgw             =  x + (sanu - 2.0) * x * &
                                  (anutau - 1.0)
            else if (sanu > 3.0 .and. sanu <= 4.0) then
              x                 =  anutau * anutau * anutau
              dtrgw             =  x + (sanu - 3.0) * x * &
                                  (anutau - 1.0)
            else if (sanu > 4.0 .and. sanu <= 20.0) then
              dtrgw             =  anutau ** (nint(sanu))
            else
              dtrgw             =  exp( - (taum(i,2,k) - taum(i,2,kk)))
            end if
            !
            s1(i)               =  s1(i) + &
                                  (emisw(i,kk) + s(i,kk)) * dtrgw
          end if
        end do
      end do ! loop 420
    end if
    !
    !----------------------------------------------------------------------c
    !     in cldm region consider the correlation between different layers c
    !----------------------------------------------------------------------c
    !
    do i = il1, il2
      if (k >= nct(i)) then
        if (cld(i,k) >= cut) then
          if (ncu(i,k) == 1) then
            fw(i,2,k)         =  fy(i,2,k)
            fu(i,2,k)         =  fy(i,1,k) + cld(i,k) * (fy(i,2,k) - &
                                fy(i,1,k))
          else
            fw(i,2,k)         =  term1(i) + s1(i) + bf(i,k) + &
                                emisw(i,k) + s(i,k)
            fu(i,2,k)         =  cldm(i,k) * (fw(i,2,k) - &
                                fy(i,2,k)) + fy(i,1,k) + &
                                cld(i,k) * (fy(i,2,k) - fy(i,1,k))
          end if
        end if
      end if
    end do ! loop 430
  end do ! loop 450
  !
  !----------------------------------------------------------------------c
  !     add the layers upward above the highest cloud  to the toa, no    c
  !     scattering                                                       c
  !----------------------------------------------------------------------c
  !
  do k = lev - 1, l1, - 1
    kp1 = k + 1
    !
    do i = il1, il2
      if (kp1 <= nct(i)) then
        fu(i,1,k)             =  fu(i,1,kp1) * dtr(i,1,k) + &
                                xu(i,1,k)
        fu(i,2,k)             =  fu(i,2,kp1) * dtr(i,1,k) + &
                                xu(i,1,k)
      end if
      !
      !----------------------------------------------------------------------c
      !     scattering effect for downward path at the top layer of the      c
      !     highest cloud                                                    c
      !----------------------------------------------------------------------c
      !
      if (k == nct(i)) then
        fw(i,1,k)             =  fx(i,1,k)
        fwins                 =  scatsm(i,1,k) + &
                                scatfw(i,k) * fx(i,2,k)
        fmbk                  =  fx(i,2,k) * dtr(i,2,k) + &
                                xd(i,2,k) + fwins
        fx(i,2,kp1)           =  fmbk + scatbk(i,k) * fy(i,2,kp1)
        taum(i,2,k)           =  taum(i,1,k)
        s(i,k)                =  scatbk(i,k) * fw(i,2,kp1) + fwins
        !
        fw(i,1,kp1)           =  fmbk + scatbk(i,k) * fw(i,2,kp1)
        fd(i,2,kp1)           =  fx(i,1,kp1) + cld(i,k) * &
                                (fx(i,2,kp1) - fx(i,1,kp1))
      end if
    end do ! loop 500
  end do ! loop 550
  !
  !----------------------------------------------------------------------c
  !     scattering effect for downward path in from maxc to the surface  c
  !----------------------------------------------------------------------c
  !
  do k = maxc + 2, lev
    km1 = k - 1
    km2 = k - 2
    km3 = k - 3
    do i = il1, il2
      if (km2 >= nct(i)) then
        if (cld(i,km1) < cut) then
          fd(i,2,k)           =  fd(i,2,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
          fx(i,1,k)           =  fd(i,2,k)
          fx(i,2,k)           =  fd(i,2,k)
          fw(i,1,k)           =  fd(i,2,k)
          taum(i,2,km1)       =  0.0
        else
          if (cld(i,km1) <= cld(i,km2) .or. &
              cld(i,km1) - cldm(i,km1) < cut) then
            !
            fx(i,1,k)         = (fx(i,2,km1) + (1.0 - cld(i,km2)) / &
                                (max(1.0 - cld(i,km1),1.e-10)) * &
                                (fx(i,1,km1) - fx(i,2,km1))) * &
                                dtr(i,1,km1) + xd(i,1,km1)
            t(i)              =  fx(i,2,km1)
          else
            fx(i,1,k)         =  fx(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
            t(i)              =  fx(i,1,km1) + &
                                (cld(i,km2) - cldm(i,km2)) / &
                                (cld(i,km1) - cldm(i,km1)) * &
                                (fx(i,2,km1) -  fx(i,1,km1))
          end if
          !
          fx(i,2,k)           =  t(i) * dtr(i,2,km1) + xd(i,2,km1) + &
                                scatbk(i,km1) * fy(i,2,k) + &
                                scatfw(i,km1) * t(i) + &
                                scatsm(i,1,km1)
          !
          taum(i,2,km1)       =  taum(i,2,km2) + taum(i,1,km1)
          s1(i)               =  0.0
          s(i,km1)            =  scatbk(i,km1) * fw(i,2,k) + &
                                scatfw(i,km1) * fw(i,1,km1) + &
                                scatsm(i,1,km1)
          term1(i)            =  0.0
          !
          if (ncd(i,km1) > 1) then
            kx = k - ncd(i,km1)
            sanu              =  anu(i,kx)
            anutau            =  sanu / (sanu + taum(i,2,km1))
            if (sanu <= 0.50) then
              dtrgw           =  sqrt(anutau)
            else if (sanu > 0.50 .and. sanu <= 1.0) then
              x               =  sqrt(anutau)
              dtrgw           =  x + 2.0 * (sanu - 0.50) * &
                                (anutau - x)
            else if (sanu > 1.0 .and. sanu <= 2.0) then
              dtrgw           =  anutau + (sanu - 1.0) * anutau * &
                                (anutau - 1.0)
            else if (sanu > 2.0 .and. sanu <= 3.0) then
              x               =  anutau * anutau
              dtrgw           =  x + (sanu - 2.0) * x * &
                                (anutau - 1.0)
            else if (sanu > 3.0 .and. sanu <= 4.0) then
              x               =  anutau * anutau * anutau
              dtrgw           =  x + (sanu - 3.0) * x * &
                                (anutau - 1.0)
            else if (sanu > 4.0 .and. sanu <= 20.0) then
              dtrgw           =  anutau ** (nint(sanu))
            else
              dtrgw           =  exp( - taum(i,2,km1))
            end if
            !
            term1(i)          = (fw(i,1,kx) - bf(i,kx)) * dtrgw
            s1(i)             = (s(i,km2) - emisw(i,km2)) * &
                                dtr(i,2,km1)
          end if
        end if
      end if
    end do ! loop 700
    !
    !----------------------------------------------------------------------c
    !     determining the terms going into the correlation calculations    c
    !     for cldm portion.                                                c
    !----------------------------------------------------------------------c
    !
    if (ncdm(km1) > 2) then
      !
      !----------------------------------------------------------------------c
      !     note that in the following loop, "km1" is actually the           c
      !     representative variable, so that k-ncd(i,km1) is actually        c
      !     km1-ncd(i,km1)+1. the simpler form is used only for              c
      !     computational efficiency.                                        c
      !----------------------------------------------------------------------c
      !
      do kk = km3, k - ncdm(km1), - 1
        do i = il1, il2
          if (km2 >= nct(i) .and. cld(i,km1) >= cut .and. &
              ncd(i,km1) > 2 .and. kk >= k - ncd(i,km1)) then
            !
            sanu                =  anu(i,kk)
            anutau              =  sanu / (sanu + &
                                  taum(i,2,km1) - taum(i,2,kk))
            if (sanu <= 0.50) then
              dtrgw             =  sqrt(anutau)
            else if (sanu > 0.50 .and. sanu <= 1.0) then
              x                 =  sqrt(anutau)
              dtrgw             =  x + 2.0 * (sanu - 0.50) * &
                                  (anutau - x)
            else if (sanu > 1.0 .and. sanu <= 2.0) then
              dtrgw             =  anutau + (sanu - 1.0) * anutau * &
                                  (anutau - 1.0)
            else if (sanu > 2.0 .and. sanu <= 3.0) then
              x                 =  anutau * anutau
              dtrgw             =  x + (sanu - 2.0) * x * &
                                  (anutau - 1.0)
            else if (sanu > 3.0 .and. sanu <= 4.0) then
              x                 =  anutau * anutau * anutau
              dtrgw             =  x + (sanu - 3.0) * x * &
                                  (anutau - 1.0)
            else if (sanu > 4.0 .and. sanu <= 20.0) then
              dtrgw             =  anutau ** (nint(sanu))
            else
              dtrgw             =  exp( - (taum(i,2,km1) - taum(i,2,kk)))
            end if
            !
            s1(i)               =  s1(i) - &
                                  (emisw(i,kk) - s(i,kk)) * dtrgw
          end if
        end do
      end do ! loop 720
    end if
    !
    do i = il1, il2
      if (km2 >= nct(i)) then
        if (cld(i,km1) >= cut) then
          if (ncd(i,km1) == 1) then
            fw(i,1,k)         =  fx(i,2,k)
            fd(i,2,k)         =  fx(i,1,k) + cld(i,km1) * &
                                (fx(i,2,k) - fx(i,1,k))
          else
            fw(i,1,k)         =  term1(i) + s1(i) + bf(i,k) - &
                                emisw(i,km1) + s(i,km1)
            fd(i,2,k)         =  cldm(i,km1) * &
                                (fw(i,1,k) - fx(i,2,k)) + &
                                fx(i,1,k) + cld(i,km1) * &
                                (fx(i,2,k) - fx(i,1,k))
          end if
        end if
      end if
    end do ! loop 730
  end do ! loop 750
  !
  return
end
!> \file
!> Calculation of longwave radiative transfer using absorption      
!! approximation with scattering effect included by perturbation
!! method \cite Li2002a \cite Li2002b. The cloud overlap is properly considered with random and 
!! full overlap assumption. low, middle and high cloud blocks are
!! random overlapped and inside each block the maximum overlap is
!! assumed. The cloud subgrid variability is considered by assuming
!! \f$\Gamma\f$ distribution, where \f$\nu\f$ is the parameter deteriming the
!! variability.