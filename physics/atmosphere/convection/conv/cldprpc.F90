!> \file
!> \brief The purpose of this subroutine is to evaluate the vertical profiles
!! of the mass flux, entrainment, detrainment, and associated updraft
!! and downdraft properties for the Zhang-McFarlane (ZM) deep convection
!! parameterization as implemented in CanAM. The output profiles of
!! mass flux, entrainment, and detrainment are all per unit cloud-base
!! mass flux.
!!
!! @author Norm McFarlane, John Scinocca
!
subroutine cldprpc(q,t,p,z,s,mu,eu,du,md,ed,sd,qd,mc,qu,su,zf,qst, &
                   hmn,hsat,alpha,shat,ql, &
                   euo,edo,dzo,duo,mdo,eps0o, &
                   jb,lel,jt,mx,j0,jd,wrk, &
                   ilev,ilg,il1g,il2g,c0fac,msg)
  !
  !     * feb 02/2017 - m.lazare.    loop 378 modified to support bounds
  !     *                            array checking explicitly.
  !     * feb 16/2009 - m.lazare.    new version for gcm15h:
  !     *                            - add accuracy restriction on eps0.
  !     * mar 27/2007 - m.lazare.    previous version cldprpb for gcm15g:
  !     *                            - weight increased from 0.25 to 0.75.
  !     *                            - selective promotion of variables
  !     *                              in real(8) :: mode to work in 32-bit.
  !     *                            - c0fac now passed in from convection
  !     *                              driver, to ensure consistent useage
  !     *                              in cldprp and contra.
  !     *                            - weight changed from 0.50 to 0.25
  !     *                              for tuning purposes.
  !     * jun 15/2006 - m.lazare.    previous version cldprpa for gcm15f:
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     *                            - "ZERO","ONE","FMAX" data constants
  !     *                              added and used in call to
  !     *                              intrinsics.
  !     * may 08/2006 - m.lazare/    previous version cldprpa for gcm15e:
  !     *               k.vonsalzen. - calculation of saturation vapour
  !     *                              pressure modified to work over
  !     *                              complete vertical domain, by
  !     *                              specifying limit as es->p.
  !     *                            - c0 increased from 4.e-3 to 6.e-3
  !     *                              for reduced cloud forcing in tropics.
  !     * dec 12/2005 - m.lazare/    previous version cldprp9 for gcm15d:
  !     *               k.vonsalzen.
  use phys_consts, only : grav, rgas, cpres, eps1, eps2, t1s, hv, rw1, rw2, rw3
  use phys_parm_defs, only : ap_weight, &
                             ap_fmax

  implicit none
  real, intent(in) :: c0fac
  integer, intent(in) :: il1g  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2g  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: msg
  !
  !     * i/o fields:
  !
  real, intent(in)  , dimension(ilg,ilev) :: q !< Large-scale mean water vapor mixing ratio at layer mid-levels \f$[g/g]\f$
  real, intent(in)  , dimension(ilg,ilev) :: t !< Large-scale meant temperature at layer mid-levels \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: p !< Large-scale pressure at layer mid-levels \f$[hPa]\f$
  real, intent(in)  , dimension(ilg,ilev) :: z !< Height above the local column surface of the layer mid-levels \f$[m]\f$
  real, intent(in)  , dimension(ilg,ilev) :: s !< Ratio of the large-scale dry static energy to the specific heat at constant pressure \f$[K]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: mu !< Updraft mass flux per unit base mass flux \f$[1]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: eu !< Updraft entrainment rate per unit base mass flux \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: du !< Updraft detrainment rate per unit base mass flux \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: md !< Downdraft mass flux per unit base mass flux \f$[1]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: ed !< Downdraft entrainment rate per unit base mass flux\f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: sd !< Ratio of the downdraft dry static energy to the specific heat at constant pressure \f$[K]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: qd !< Downdraft water vapor mixing ratio \f$[g/g]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: mc !< Sum of the updraft and downdraft mass flux per unit base mass flux \f$[1]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: qu !< Updraft water vapor mixing ratio \f$[g/g]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: su !< Ratio of the updraft dry static energy to the specific heat at constant pressure \f$[K]\f$
  real, intent(in)  , dimension(ilg,ilev) :: zf !< Height of model interface levels above the local surface \f$[m]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: qst !< Saturation value of the water vapor mixing ratio corresponding to the large-scale temperature \f$[g/g]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: hmn !< Large-scale moist static energy \f$[J/kg]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: hsat !< Large-scale moist static energy at saturation \f$[J/kg]\f$
  real, intent(in)  , dimension(ilg,ilev) :: alpha !< Work array not currently used in subroutine \f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev) :: shat !< S interpolated to model interface levels \f$[K]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: ql !< Updraft liquid water mixing ratio \f$[g/g]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: euo !< Updraft entrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: edo !< Downdraft entrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: dzo   !< Layer depths used in scalar chemical tracer convective effects calculations \f$[m]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: duo !< Updraft detrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg,ilev) :: mdo !< Downdraft mass flux used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$

  real, intent(inout)  , dimension(ilg) :: eps0o !< Maximum updraft entrainment rate for the column \f$[m^{-1}]\f$
  real, intent(inout)  , dimension(ilg) :: wrk !< Downdraft weighting function \f${[}\mu/(1+\mu PCP/EVP)]\f$ (note OUT) \f$[units]\f$

  integer, intent(in),dimension(ilg) :: jb !< Index for the base level of the convective region for the column \f$[1]\f$
  integer, intent(in),dimension(ilg) :: lel !< Level of neutral buoyancy for an undiluted parcel ascending from the base level\f$[1]\f$
  integer, intent(inout),dimension(ilg) :: jt !< Top level of deep convection (initially set to LEL and subsequently re-evaluated (note INOUT) \f$[1]\f$
  integer, intent(in),dimension(ilg) :: mx !< Lowest level for initiation of moist convection \f$[1]\f$
  integer, intent(inout),dimension(ilg) :: j0 !< Level of minimum saturated moist static energy (note OUT) \f$[1]\f$
  integer, intent(inout),dimension(ilg) :: jd !< Level of the base of the updraft detrainment layer (note OUT) \f$[1]\f$

  !
  !     * internal work fields:
  !
  real :: beta
  real :: c0
  real :: ddrat
  real :: epslim
  real :: estref
  real :: estu
  real :: etmp
  real :: fac
  integer :: il
  integer :: j
  integer :: k
  real :: qstu
  real :: ratio
  real :: ttt
  real :: weight
  real :: zeps
  !
  !     *** the following must be promoted for accuracy !! ! *********
  real*8, dimension(ilg,ilev) :: iprm
  real*8, dimension(ilg,ilev) :: i1
  real*8, dimension(ilg,ilev) :: i2
  real*8, dimension(ilg,ilev) :: ihat
  real*8, dimension(ilg,ilev) :: i3
  real*8, dimension(ilg,ilev) :: idag
  real*8, dimension(ilg,ilev) :: i4
  real*8, dimension(ilg,ilev) :: f
  real*8, dimension(ilg,ilev) :: eps
  real*8, dimension(ilg)      :: hmin
  real*8, dimension(ilg)      :: expdif
  real*8, dimension(ilg)      :: expnum
  real*8, dimension(ilg)      :: ftemp
  real*8, dimension(ilg)      :: hmax
  real*8, dimension(ilg)      :: zuef
  real*8  :: fmax

  !     ************************************************************
  !
  real, dimension(ilg,ilev) :: gamma
  real, dimension(ilg,ilev) :: dz
  real, dimension(ilg,ilev) :: hu
  real, dimension(ilg,ilev) :: hd
  real, dimension(ilg,ilev) :: qsthat
  real, dimension(ilg,ilev) :: hsthat
  real, dimension(ilg,ilev) :: gamhat
  real, dimension(ilg,ilev) :: cu
  real, dimension(ilg,ilev) :: qds
  real, dimension(ilg)      :: eps0
  real, dimension(ilg)      :: rmue
  real, dimension(ilg)      :: zdef
  real, dimension(ilg)      :: fact
  real, dimension(ilg)      :: tu
  real, dimension(ilg)      :: rl
  real, dimension(ilg)      :: est
  real, dimension(ilg)      :: totpcp
  real, dimension(ilg)      :: totevp
  real, dimension(ilg)      :: alfa
  real, dimension(ilg)      :: zfd
  real, dimension(ilg)      :: zfb
  real, dimension(ilg)      :: denom
  real, dimension(ilg)      :: hub
  real, dimension(ilg)      :: ireset
  real, dimension(ilg)      :: jlcl


  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real*8, parameter :: zero = 0.
  real  , parameter :: one = 1.
  !
  !     * statement function to calculate saturation vapour pressure
  !     * over water.
  !
  real :: esw
  esw(ttt)    = exp(rw1 + rw2/ttt) * ttt ** rw3
  !
  fmax = ap_fmax
  weight = ap_weight
  !----------------------------------------------------------------------
  zeps = 1.e-33
  epslim = 0.001
  do k = msg + 1,ilev - 1
    do il = il1g,il2g
      dz(il,k) = zf(il,k) - zf(il,k + 1)
    end do
  end do ! loop 10
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      mu(il,k) = 0.
      f(il,k) = 0.
      eps(il,k) = 0.
      eu(il,k) = 0.
      du(il,k) = 0.
      ql(il,k) = 0.
      cu(il,k) = 0.
      qds(il,k) = q(il,k)
      md(il,k) = 0.
      ed(il,k) = 0.
      sd(il,k) = s(il,k)
      qd(il,k) = q(il,k)
      mc(il,k) = 0.
      hd(il,k) = 0.
      i1(il,k) = 0.
      i2(il,k) = 0.
      i3(il,k) = 0.
      i4(il,k) = 0.
      ihat(il,k) = 0.
      idag(il,k) = 0.
      iprm(il,k) = 0.
      qu(il,k) = q(il,k)
      su(il,k) = s(il,k)
      etmp = esw(t(il,k))
      estref = p(il,k) * (1. - epslim)/(1. - epslim * eps2)
      if (etmp < estref) then
        est(il) = etmp
      else
        est(il) = estref
      end if
      if (k >= lel(il)) then
        qst(il,k) = eps1 * est(il)/(p(il,k) - est(il))
      else
        qst(il,k) = q(il,k)
      end if
      gamma(il,k) = qst(il,k) * (1. + qst(il,k)/eps1) * eps1 * hv &
                    /(rgas * t(il,k) ** 2) * hv/cpres
      hmn (il,k) = cpres * t(il,k) + grav * z(il,k) + hv * q  (il,k)
      hsat(il,k) = cpres * t(il,k) + grav * z(il,k) + hv * qst(il,k)
      hu(il,k) = hmn(il,k)
    end do
  end do ! loop 50
  !
  do il = il1g,il2g
    hsthat(il,msg + 1) = hsat(il,msg + 1)
    qsthat(il,msg + 1) = qst(il,msg + 1)
    gamhat(il,msg + 1) = gamma(il,msg + 1)
    totpcp(il)  = 0.
    totevp(il)  = 0.
    dz(il,ilev) = zf(il,ilev)
    rl(il) = hv
    jt(il) = lel(il)
    eps0(il) = 0.
    alfa(il) = 0.
    expdif (il) = 0.
    hub(il) = hu(il,ilev)
    j0(il) = ilev
    jd(il) = ilev
    jlcl(il) = 0.
    hmin(il) = 1.e6
    hmax(il) = hmn(il,mx(il))
    zfb(il) = zf(il,jb(il))
    ireset(il) = 0.
  end do ! loop 75
  !
  do k = msg + 2,ilev
    do il = il1g,il2g
      qsthat(il,k) = qst(il,k)
      gamhat(il,k) = gamma(il,k)
      if (k >= lel(il)) then
        if (qst(il,k - 1) /= qst(il,k)) then
          qsthat(il,k) = log(qst(il,k - 1)/qst(il,k)) * qst(il,k - 1) * qst(il,k) &
                         /(qst(il,k - 1) - qst(il,k))
        end if
        !
        if (gamma(il,k - 1) /= gamma(il,k)) then
          gamhat(il,k) = log(gamma(il,k - 1)/gamma(il,k)) &
                         * gamma(il,k - 1) * gamma(il,k)/(gamma(il,k - 1) - gamma(il,k))
        end if
      end if
      hsthat(il,k) = cpres * shat(il,k) + hv * qsthat(il,k)
    end do
  end do ! loop 100
  !
  ! cc   **********************************************************
  ! cc   find the level of minimum hsat, where detrainment starts.
  ! cc   initialize certain arrays inside the cloud plume.
  ! cc   **********************************************************
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (hsat(il,j) <= hmin(il) .and. (j >= jt(il) .and. j <= jb(il))) then
        hmin(il) = hsat(il,j)
        j0(il)  = j
      end if
      if (j >= jt(il) .and. j <= jb(il)) then
        f(il,j) = 0.
        hu(il,j) = hmax(il)
      end if
    end do
  end do ! loop 150
  !
  !     * define bounds for detrainment level.
  !     * re-initialize "HMIN" for ensuing calculations.
  !
  do il = il1g,il2g
    j0(il) = min(j0(il),jb(il) - 2)
    j0(il) = max(j0(il),jt(il) + 2)
    hmin(il) = 1.e6
  end do ! loop 175
  !
  ! ccc  *********************************************************
  ! ccc  compute taylor series for approximate eps(z) below
  ! ccc  *********************************************************
  !
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if (j < jb(il) .and. j >= jt(il)) then
        i1  (il,j) = i1(il,j + 1) + (hmax(il) - hmn(il,j)) * dz(il,j)
        ihat(il,j) = 0.5 * (i1(il,j + 1) + i1(il,j))
        i2  (il,j) = i2(il,j + 1) + ihat(il,j) * dz(il,j)
        idag(il,j) = 0.5 * (i2(il,j + 1) + i2(il,j))
        i3  (il,j) = i3(il,j + 1) + idag(il,j) * dz(il,j)
        iprm(il,j) = 0.5 * (i3(il,j + 1) + i3(il,j))
        i4  (il,j) = i4(il,j + 1) + iprm(il,j) * dz(il,j)
      end if
    end do
  end do ! loop 275
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if ( (j >= j0(il) .and. j <= jb(il)) .and. &
          hmn(il,j) <= hmin(il)                ) then
        hmin(il) = hmn(il,j)
        expdif (il) = hmax(il) - hmin(il)
      end if
    end do
  end do ! loop 350
  !
  ! ccc  *********************************************************
  ! ccc  compute approximate eps(z) using above taylor series
  ! ccc  *********************************************************
  !
  do j = msg + 2,ilev
    do il = il1g,il2g
      if (j < jt(il) .or. j > jb(il)) then
        i1(il,j) = 0.
        expnum(il) = 0.
      else
        expnum(il) = hmax(il) - ( hsat(il,j - 1) * (zf(il,j) - z(il,j)) &
                     + hsat(il,j) * (z(il,j - 1) - zf(il,j)) )/(z(il,j - 1) - z(il,j))
      end if
      if ( (expdif (il) > 100. .and. expnum(il) > 0.) .and. &
          i1(il,j) > expnum(il) * dz(il,j)) then
        ftemp(il) = expnum(il)/i1(il,j)
        f(il,j) = ftemp(il) &
                  + i2(il,j)/i1(il,j) * ftemp(il) ** 2 &
                  + (2. * i2(il,j) ** 2 - i1(il,j) * i3(il,j))/i1(il,j) ** 2 &
                  * ftemp(il) ** 3 &
                  + ( - 5. * i1(il,j) * i2(il,j) * i3(il,j) + 5. * i2(il,j) ** 3 &
                  + i1(il,j) ** 2 * i4(il,j))/i1(il,j) ** 3 * ftemp(il) ** 4
        f(il,j) = max(f(il,j),zero)
        f(il,j) = min(f(il,j),fmax)
      end if
    end do
  end do ! loop 375
  !
  do il = il1g,il2g
    if (f(il,j0(il)) < 1.e-6 .and. j0(il) < jb(il) .and. &
        f(il,min(j0(il) + 1,ilev)) > f(il,j0(il)) ) then
      j0(il) = j0(il) + 1
    end if
  end do ! loop 378
  !
  do j = msg + 2,ilev
    do il = il1g,il2g
      if (j >= jt(il) .and. j <= j0(il)) then
        f(il,j) = max(f(il,j),f(il,j - 1))
      end if
    end do
  end do ! loop 380
  !
  do j = ilev,msg + 1, - 1
    do il = il1g,il2g
      if (j >= jt(il) .and. j < j0(il)) then
        eps(il,j) = f(il,j)
      else if (j >= j0(il) .and. j <= jb(il)) then
        eps(il,j) = f(il,j0(il))
      end if
    end do
  end do ! loop 425
  !
  ! cc   ****************************************************************
  ! cc   specify the updraft mass flux mu, entrainment eu, detrainment du
  ! cc   and moist static energy hu.
  ! cc   here and below mu, eu,du, md and ed are all normalized by mb
  ! cc   ****************************************************************
  !
  do il = il1g,il2g
    eps0(il) = f(il,j0(il))
    if (eps0(il) < 1.e-6) eps0(il) = 0.
    if (eps0(il) > 0.) then
      mu(il,jb(il)) = 1.
      hub(il)      = hu(il,jb(il))
      eu(il,jb(il)) = eps0(il)/2.
    end if
  end do ! loop 475
  !
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if (eps0(il) > 0. .and. (j >= jt(il) .and. j < jb(il)) ) then
        zuef(il) = zf(il,j) - zfb(il)
        rmue(il) = (1./eps0(il)) * (exp(eps(il,j + 1) * zuef(il)) - 1.)/zuef(il)
        mu(il,j) = (1./eps0(il)) * (exp(eps(il,j) * zuef(il)) - 1.)/zuef(il)
        eu(il,j) = (rmue(il) - mu(il,j + 1))/dz(il,j)
        du(il,j) = (rmue(il) - mu(il,j))/dz(il,j)
      end if
    end do
  end do ! loop 480
  !
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if ( (j >= lel(il) .and. j < jb(il)) .and. &
          ireset(il) == 0. .and. eps0(il) > 0.) then
        if (mu(il,j) < 0.01 .or. hu(il,j + 1) > hub(il)) then
          hu(il,j) = hub(il)
          mu(il,j) = 0.
          jt(il) = j + 1
          ireset(il) = 1.
          !           PRINT *,'JT DUE TO TOO MUCH DETRAINMENT  ===',JT(IL),
          !    1              'IL= ',IL
        else
          hu(il,j) = mu(il,j + 1)/mu(il,j) * hu(il,j + 1) + dz(il,j)/mu(il,j) * &
                     (eu(il,j) * hmn(il,j) - du(il,j) * hsat(il,j))
          if (hu(il,j) <= hsthat(il,j)   .and. &
              hu(il,j + 1) > hsthat(il,j + 1)) then
            jt(il) = j
            ireset(il) = 2.
            !             PRINT *,'JT DUE TO OVERSHOOTING ===',JT(IL),' IL= ',IL
          end if
        end if
      end if
    end do
  end do ! loop 500
  !
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if (j >= lel(il) .and. j <= jt(il) &
          .and. eps0(il) > 0. ) then
        mu(il,j) = 0.
        eu(il,j) = 0.
        du(il,j) = 0.
        hu(il,j) = hub(il)
      end if
      if (j == jt(il) .and. eps0(il) > 0.) then
        du(il,j) = mu(il,j + 1)/dz(il,j)
      end if
    end do
  end do ! loop 600
  !
  ! cc   ******************************************************************
  ! cc   specify downdraft properties (no downdrafts if jd>=jb).
  !     scale down downward mass flux profile so that net flux
  !     (up-down) at cloud base in not negative.
  ! cc   ************************************************************
  !
  do il = il1g,il2g
    if (eps0(il) > 0.) then
      jd(il) = max(j0(il),jt(il) + 1)
      zfd(il) = zf(il,jd(il))
      hd(il,jd(il)) = hmn(il,jd(il) - 1)
      fact(il) = 2. * eps0(il) * (zfd(il) - zfb(il))
      if (fact(il) > 0.) then
        alfa(il) = fact(il)/(exp(fact(il)) - 1.)
      else
        alfa(il) = 0.
      end if
    end if
  end do ! loop 625
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        zdef(il) = zfd(il) - zf(il,j)
        if ((j == jd(il)) .and. fact(il) > 0.) then
          md(il,j) = - 1.
        end if
        if ((j == jb(il)) .and. fact(il) > 0.) then
          md(il,j) = - 1.
        end if
        if ((j < jb(il)) .and. zdef(il) > 0.) then
          md(il,j) = - 1.
        end if
      end if
    end do
  end do ! loop 630

  !
  do j = msg + 2,ilev
    do il = il1g,il2g
      if ((j > jd(il) .and. j <= jb(il)) .and. eps0(il) > 0.) then
        ed(il,j - 1) = (md(il,j - 1) - md(il,j))/dz(il,j - 1)
        hd(il,j) = md(il,j - 1)/md(il,j) * hd(il,j - 1) &
                   - dz(il,j - 1)/md(il,j) * ed(il,j - 1) * hmn(il,j - 1)
      end if
    end do
  end do ! loop 650
  !
  ! cc   *********************************************************
  ! cc   calculate updraft and downdraft effects on q and s.
  ! cc   *********************************************************
  !
  do k = msg + 1,ilev
    do il = il1g,il2g
      if ((k >= jd(il) .and. k <= jb(il)) .and. eps0(il) > 0. &
          .and. jd(il) < jb(il)) then
        !         sd(il,k) = shat(il,k)
        !    1             +              (hd(il,k)-hsthat(il,k))/
        !    2               (cpres    *(1.+gamhat(il,k)))
        qds(il,k) = qsthat(il,k) &
                    + gamhat(il,k) * (hd(il,k) - hsthat(il,k))/ &
                    (hv * (1. + gamhat(il,k)))
      end if
    end do
  end do ! loop 670
  !
  do j = ilev - 1,msg + 2, - 1
    do il = il1g,il2g
      if (eps0(il) > 0. .and. jlcl(il) == 0.) then
        if (j > jt(il) .and. j < jb(il)) then
          su(il,j) = mu(il,j + 1)/mu(il,j) * su(il,j + 1) &
                     + dz(il,j)/mu(il,j) * (eu(il,j) - du(il,j)) * s(il,j)
          qu(il,j) = mu(il,j + 1)/mu(il,j) * qu(il,j + 1) &
                     + dz(il,j)/mu(il,j) * (eu(il,j) * q(il,j) - du(il,j) * qst(il,j))
          tu(il) = su(il,j) - grav/cpres * zf(il,j)
          etmp = esw(tu(il))
          estref = p(il,j) * (1. - epslim)/(1. - epslim * eps2)
          if (etmp < estref) then
            estu = etmp
          else
            estu = estref
          end if
          qstu = eps1 * estu/((p(il,j) + p(il,j - 1))/2. - estu)
          if (qu(il,j) >= qstu) then
            jlcl(il) = real(j)
          end if
        end if
      end if
    end do ! loop 680
  end do ! loop 690
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        if (j == jb(il)) then
          qu(il,j) = q(il,mx(il))
          su(il,j) = (hu(il,j) - hv * qu(il,j))/cpres
        end if
        if (j > jt(il) .and. j <= nint(jlcl(il))) then
          su(il,j) = shat(il,j) &
                     +              (hu(il,j) - hsthat(il,j))/ &
                     (cpres    * (1. + gamhat(il,j)))
          qu(il,j) = qsthat(il,j) &
                     + gamhat(il,j) * (hu(il,j) - hsthat(il,j))/ &
                     (hv * (1. + gamhat(il,j)))
        end if
      end if
    end do
  end do ! loop 700
  !
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if (eps0(il) > 0.) then
        if (j > jt(il) .and. j < jb(il)) then
          cu(il,j) = ((mu(il,j) * su(il,j) - mu(il,j + 1) * su(il,j + 1))/dz(il,j) &
                     - (eu(il,j) - du(il,j)) * s(il,j))/(hv/cpres)
        end if
      end if
    end do
  end do ! loop 750
  !
  beta = 1.
  do j = ilev,msg + 2, - 1
    do il = il1g,il2g
      if (j >= jt(il) .and. j < jb(il) .and. eps0(il) > 0. &
          .and. mu(il,j) >= 0.0) then
        fac = max(t(il,jb(il)) - t(il,j),0.)/ &
              max(t(il,jb(il)) - t1s, one)
        c0 = c0fac * min(fac ** 2,one)
        denom(il) = (mu(il,j) + dz(il,j) * beta * du(il,j) &
                    + dz(il,j) * c0 * (beta * mu(il,j) + (1. - beta) * mu(il,j + 1)) * beta)
        if (denom(il) > 0.) then
          ql(il,j) = 1./denom(il) &
                     * (mu(il,j + 1) * ql(il,j + 1) - dz(il,j) * du(il,j) * (1. - beta) &
                     * ql(il,j + 1) + dz(il,j) * cu(il,j) - dz(il,j) * c0 * (beta * mu(il,j) &
                     + (1. - beta) * mu(il,j + 1)) * (1. - beta) * ql(il,j + 1))
        else
          ql(il,j) = 0.
        end if
        totpcp(il) = totpcp(il) + dz(il,j) * (cu(il,j) &
                     - du(il,j) * (beta * ql(il,j) + (1. - beta) * ql(il,j + 1)))
      end if
    end do
  end do ! loop 800
  !
  ddrat = 0.
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (eps0(il) > 0. .and. j == jd(il)) then
        qd(il,j) = qds(il,j) - ddrat * (qds(il,j) - q(il,j - 1))
        sd(il,j) = (hd(il,j) - hv * qd(il,j) )/cpres
      end if
    end do
  end do ! loop 850
  !
  do j = msg + 1,ilev - 1
    do il = il1g,il2g
      if ((j >= jd(il) .and. j < jb(il)) .and. eps0(il) > 0. &
          .and. jd(il) < jb(il)) then
        qd(il,j + 1) = qds(il,j + 1) - ddrat * (qds(il,jd(il)) - qd(il,jd(il)))
        sd(il,j + 1) = (hd(il,j + 1) - hv * qd(il,j + 1))/cpres
        totevp(il) = totevp(il) - dz(il,j) * ed(il,j) * q(il,j)
      end if
    end do
  end do ! loop 900
  !
  !     * save certain fields to be used in subroutine contra3 for
  !     * sulfur cycle, rather than having to recompute them.
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      dzo(il,j) = dz(il,j)
      euo(il,j) = eu(il,j)
      edo(il,j) = ed(il,j)
      duo(il,j) = du(il,j)
      mdo(il,j) = md(il,j)
    end do
  end do ! loop 923
  !
  do il = il1g,il2g
    eps0o(il) = eps0(il)
    if (eps0(il) > 0.) then
      totevp(il) = totevp(il) + md(il,jd(il)) * q(il,jd(il) - 1) &
                   - md(il,jb(il)) * qd(il,jb(il))
    else
      !
      !         * reset indices to ilev to that mb will be zero for rest of code.
      !
      j0(il) = ilev
      jd(il) = ilev
      jt(il) = ilev
    end if
  end do ! loop 925

  do j = msg + 2,ilev
    do il = il1g,il2g
      if (eps0(il) > 0. .and. totevp(il) > 0.) then
        ratio = totpcp(il)/totevp(il)
        md(il,j) = md(il,j) * weight * ratio/(1. + weight * ratio)
        ed(il,j) = ed(il,j) * weight * ratio/(1. + weight * ratio)
      else
        md(il,j) = 0.
        ed(il,j) = 0.
      end if
    end do
  end do ! loop 950
  !
  !     * save fraction of rain which evaporates in the downdrafts.
  !
  do il = il1g,il2g
    wrk(il) = 0.
    if (eps0(il) > 0. .and. totevp(il) /= 0.) then
      wrk(il) = min(one, weight * totevp(il) &
                /(totevp(il) + weight * totpcp(il)))
    end if
  end do ! loop 960
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (eps0(il) > 0. .and. (j >= jt(il) .and. j <= jb(il))) then
        mc(il,j) = mu(il,j) + md(il,j)
      end if
    end do
  end do ! loop 975
  !
  return
end subroutine cldprpc

!> \file
!! \section sec_theory Theoretical basis
!! \subsection ssec_updrafts Updrafts
!! This routine is based on the deep convection parameterization formulation
!! developed by \cite Zhang1995, henceforth referred to as ZM. The formulation is based on a bulk updraft
!! profile that is comprised of an ensemble of deep convective entraining
!! plumes, all of which have the same cloud-base mass flux. Individual
!! plumes within the ensemble have fixed unique fractional entrainment
!! rates. For a given representative sub-ensemble plume the entrainment
!! rate is determined by specifying the height above cloud-base where
!! the plume buoyancy becomes negative while being positive between this
!! level and the level of of free convection (LFC) for the plume. This
!! level of neutral buoyancy (LNB) is approximated as the level at which
!! the plume temperature is equal to that of the large-scale environment,
!! assumed to be identical to the mean state defined by the model prognostic
!! variables. The detrainment rate of the representative plume is assumed
!! to be zero below the LNB but all of the plume mass is detrained at
!! the LNB.
!! \n
!! \n
!! The bulk mass flux profile consistent with this conceptual framework
!! is given by
!! \n
!! \n
!! \f{equation}{
!! \frac{M_{u}(\lambda_{D}(z),z)}{M_{b}}=\frac{1}{\lambda_{0}(z-z_{b})}\left[exp\lambda_{D}(z)(z-z_{b})-1\right]\tag{1}
!! \f}
!! where \f$z_{b}\f$ is the specified height above the surface of the base
!! of the plume ensemble, \f$\lambda_{D}(z)\f$ is the fractional entrainment
!! profile and \f$M_{b}\f$ is the (independently evaluated) updraft mass
!! flux at the base level. The base updraft mass flux ( \f$M_{b}\f$ ) is
!! evaluated through the closure formulation that is implemented in the
!! subroutine closur10.f.
!! \n
!! \n
!! The fractional entrainment profile function is determined from the
!! following equations
!! \n
!! \f{equation}{
!! h_{b}-\overline{h}_{*}(z)=\lambda_{D}(z)\int_{z_{b}}^{z}[h_{b}-\bar{h}(z')]exp[\lambda_{D}(z)(z'-z))]dz'\tag{2}
!! \f}
!! This follows from application of the following entraining plume moist
!! static energy equation:
!! \n
!! \f{equation}{
!! \frac{\partial h_{p}}{\partial z'}=-\lambda_{D}(z)\left[h_{p}(z')-\bar{h}(z')\right]dz'; z_{b}\leq z'\leq z\tag{3}
!! \f}
!! where the left-hand side of equation (2) is the difference between
!! the boundary values of the plume moist static energy at the base and
!! detrainment levels.
!! \n
!! \n
!! The general definition of the moist static energy for a given temperature
!! (\f$T\f$) and water vapor mixing ratio (\f$r\f$) is \f$h=c_{p}T+L_{v}r+gz\f$
!! where \f$L_{v}\f$ is the latent heat of vaporization and \f$g\f$ is the
!! acceleration due to gravity. Here \f$h_{b}=\bar{h}(z_{b})\f$ where \f$\bar{h}\f$
!! is the large-scale environmental value of the moist static energy,
!! with a corresponding saturation value given by \f$\bar{h}_{*}=c_{p}\bar{T}+L_{v}r_{*}(\bar{T},p)+gz\f$
!! where \f$r_{*}\f$ is the saturation value at the given temperature and
!! pressure. Since it is within the cloudy updraft, the plume is assumed
!! to be saturated with respect to water vapor at the detrainment level.
!! \n
!! \n
!! Equation (2) is implicitly nonlinear in \f$\lambda_{D}\f$ because of
!! the dependence of \f$h_{p}\f$ on that quantity through equation (3).
!! An extended Newton-Raphson scheme, using a three-term Taylor expansion
!! about an initial guess value is used to evaluate this quantity. The
!! method of <em> reversion of series </em> employed to extract an accurate
!! approximation in a single iteration using an initial guess of zero
!! (corresponding to an undiluted plume).The formulation of this procedure
!! is discussed in the appendix of ZM, Limitations on the maximum
!! value of \f$\lambda_{D}\f$ are imposed it two ways: (1) an absolute limit
!! is set through specification of the FMAX variable (units \f$m^{-1})\f$
!! ; (2) the lowest level of detrainment is required to be at or above
!! the level of minimum \f$\bar{h}_{*}\f$ .The above procedure for determining
!! the \f$\lambda_{D}\f$ profile is carried out from this level upward until
!! the LNB for a non-entraining plume is reached.
!! \n
!! \n
!! A further constraint is that \f$\lambda_{D}\f$ must not increase with
!! height. This is consistent with the underlying plume ensemble framework
!! proposed by \cite Arakawa1974, and adapted in the ZM bulk
!! formulation, wherein individual plumes are distinguished by their
!! fractional entrainment rates with large entrainment rates being associated
!! with shallower plumes. However, local non-monotonic variations in
!! the \f$\bar{h}_{*}\f$ profile can in some cases cause the procedure outlined
!! above to produce a value of \f$\lambda_{D}\f$ at a given level that is
!! larger than the value at the level below it. This implies the existence
!! of a narrow layer with locally reduced buoyancy for the plume that
!! would otherwise detrain at the upper level. It is reasonable to assume
!! that this plume would have sufficient upward momentum to penetrate
!! the small region of negative buoyancy without undergoing substantial
!! detrainment. Therefore in these circumstances the value at the lower
!! level is reset to the larger of the two.
!! \n
!! \n
!! Once the \f$\lambda_{D}\f$ profile is evaluated the mass flux profile
!! and corresponding entrainment and detrainment profiles mare evaluated.
!! These are formally defined as stated in equations (5) of ZM. In practice,
!! for the discretized formulation used in the model, the mass flux is
!! defined at layer interfaces while entrainment and detrainment are
!! evaluated at the center points of the layers. This is done, for the
!! layer (\f$j\f$) between interface levels (\f$j,j+1\f$) (indexed from the
!! top downward) by evaluating the mass flux at the upper interface as
!! \f$M_{u}(\lambda_{D}(z_{j}),z_{j})\f$ and also a ``non-detraining''
!! mass flux \f$M_{u}^{ND}=M_{u}(\lambda_{D}(z_{j+1}),z_{j})\f$. The the
!! layer updraft entrainment and detrainment rates \f$\left(E_{j,j+1},D_{j,j+1}\right)\f$
!! are defined as
!! \n
!! \f{equation}{
!! E_{j,j+1}=\frac{M_{u}^{ND}-M_{u}(\lambda_{D}(z_{j+1}),z_{j+1})}{z_{j}-z_{j+1}}\tag{4}
!! \f}
!! \f{equation}{
!! D_{j,j+1}=\frac{M_{u}^{ND}-M_{u}(\lambda_{D}(z_{j}),z_{j})}{z_{j}-z_{j+1}}\tag{5}
!! \f}
!! Since, by construction, \f$\lambda_{D}\f$ does not increase with height,
!! these quantities will be non-negative. The expression for the detrainment
!! rate is a discrete approximation to equation (5a) of ZM.
!! \n
!! \n
!! Using the above determined mass flux, entrainment and detrainment
!! profiles, discretized versions of equations (6) in ZM are solved for
!! the bulk updraft properties. These equations are first combined into
!! a budget equation for the bulk updraft moist static energy (\f$h_{u})\f$
!! which is discretized and used to evaluate this quantity starting from
!! from the specified base level. The top of the convective layer is
!! determined as the highest level (\f$z_{t})\f$ such that \f$h_{u}\geq\bar{h}_{*}\f$
!! for \f$z_{b}\leq z\leq z_{t}\f$ . The bulk mass flux, entrainment and
!! detrainment are set to zero above this level.
!! \n
!! \n
!! The base level is typically below the lifting condensation level (LCL)
!! of the bulk updraft. The bulk updraft is assumed to be unsaturated
!! with respect to water vapor below the LCL. Accordingly the budget
!! equations for the dry static energy (\f$S_{u}=c_{p}T_{u}+gz)\f$ and the
!! water vapor mixing ratio (\f$q_{u})\f$ are solved separately starting
!! from the base level assuming that there is no condensation. Using
!! the corresponding bulk updraft temperature and ambient pressure the
!! saturation value of the water vapor pressure and the corresponding
!! water vapor mixing ratio are evaluated. The LCL is set as the lowest
!! level where this saturation mixing ratio is less that the bulk updraft
!! mixing ratio calculated assuming no condensation. Above this level
!! the bulk updraft is assumed to be saturated with respect to water
!! vapor, i.e. \f$q_{u}=q_{*}(T_{u},p)\f$. In this case, given the bulk
!! updraft moist static energy, pressure and height above the surface,
!! the corresponding temperature is a the solution of the equation
!! \n
!! \f{equation}{
!! c_{p}T_{u}+L_{v}q_{*}(T_{u},p)+gz=h_{u}\tag{6}
!! \f}
!! The left-hand side of this expression can be approximated with a
!! two-term Taylor expansion about \f$\bar{h}_{*}\f$ to give the following
!! approximate expressions for the updraft dry static energy and water
!! vapor mixing ratio:
!! \n
!! \f{equation}{
!! q_{u}\sim q_{*}(\bar{\bar{T},p)+\frac{\gamma}{L_{v}(1+\gamma)}\left(h_{u}-\bar{h}_{*}\right)}\tag{7}
!! \f}
!! \f{equation}{
!! S_{u}=\bar{S}+\frac{(h_{u}-\bar{h}_{*})}{1+\gamma}\tag{8}
!! \f}
!! where \f$\gamma=1+(L_{v}/c_{p})(\partial q_{*}/\partial\bar{T)}\f$ .
!! These approximations are used to evaluate the effective condensation
!! rate for layer (\f$j,j+1\f$) in accord with a discretized representation
!! of equation (6a) of ZM where the updraft dry static energy, water
!! vapor and liquid water mixing ratios are evaluated at layer interfaces
!! while the dry static energy and water vapor mixing ratio for the environment
!! are evaluated at the mid-points of the layers in terms of the large-scale
!! mean (prognostic) variables and interpolated to layer interfaces.
!! \n
!! \n
!! <em> Note that mixed phase and ice-phase effects are ignored in the
!! operational version of the ZM scheme. </em>
!! \n
!! \n
!! The liquid water mixing ratio of the bulk updraft is evaluated at
!! layer interfaces using a discretized version of equation (6c) in ZM.
!! The layer-mean liquid water detrainment is assumed to be a weighted
!! average of the values at the upper and lower interfaces with a specified
!! weight parameter (\f$BETA\leq1\f$) for the upper interface contribution.
!! The form of the precipitation flux term shown in equation 7 of ZM
!! has been retained but the constant conversion factor (\f$C_{0})\f$ has
!! been replaced by a specified vertical profile function that allows
!! for the efficiency of precipitation formation to be reduced below
!! the environmental freezing level and enhanced above it.
!! \n
!! \subsection ssec_downdrafts Downdrafts
!! A saturated, evaporatively driven bulk downdraft is included. The
!! basic formulation for this bulk downdraft is similar to that outlined
!! in ZM but simplified in CanAM implementation by setting the downdraft
!! entrainment to zero. The downdraft is initiated at the base
!! of the lowest detrainment level with the downdraft moist static energy
!! set equal to the large-scale environmental value. Between this level
!! and the base level of the convective layer the mass flux and moist
!! static energy of the downdraft are assumed to be independent of height.
!! The downdraft detrains in the layer immediately below the base level.
!! Since it is assumed to be saturated with respect to water vapor, the
!! downdraft dry static energy and water vapor mixing ratio are evaluated
!! in that layer using similar approximations to those used to evaluate
!! updraft properties where the updraft is saturated. The magnitude of
!! the downdraft mass flux, relative the that of the updraft at the base
!! of the convective layer is determined in terms of the precipitation
!! production in the updraft and the evaporation in the downdraft as
!! \n
!! \f{equation}{
!! \frac{M_{d}}{M_{b}}=-\left(\frac{\mu PCP}{\mu PCP+EVP}\right)\tag{9}
!! \f}
!! where \f$PCP\f$,\f$EVP\f$ are, respectively the precipitation production per
!! unit updraft base mass flux and the evaporation of rainwater in the
!! downdraft per unit downdraft mass flux. The weighting parameter \f$\mu\f$ is
!! specified to be 0.75 in the CanAM implementation. This parameter
!! sets the maximum fraction of the precipitation produced in the updraft
!! that can be evaporated in the downdraft if the downdraft maximum evaporation
!! rate is large relative to it.
!! \n
!! \subsection ssec_down_entrainment Downdraft entrainment
!!
!! Downdraft entrainment effects can be included as in ZM by replacing
!! this code
!> \code
!!      DO 630 J=MSG+1,ILEV
!!      DO 630 IL=IL1G,IL2G
!!        IF (EPS0(IL)>0.)                                        THEN
!!          ZDEF(IL)=ZFD(IL)-ZF(IL,J)
!!          IF ((J==JD(IL)) .AND. FACT(IL)>0.)                  THEN
!!            MD(IL,J)=-1.
!!          END IF
!!          IF ((J==JB(IL)) .AND. FACT(IL)>0.)                  THEN
!!            MD(IL,J)=-1.
!!          END IF
!!          IF ((J<JB(IL)) .AND. ZDEF(IL)>0.)                  THEN
!!            MD(IL,J)=-1.
!!          END IF
!!        END IF
!!  630 CONTINUE
!! \endcode
!! with this code
!> \code
!!      DO 630 J=MSG+1,ILEV
!!      DO 630 IL=IL1G,IL2G
!!          ZDEF(IL)=ZFD(IL)-ZF(IL,J)
!!          IF ((J==JD(IL)) .AND. FACT(IL)>0.) THEN
!!             MD(IL,J)=-ALFA(IL)
!!          END IF
!!
!!          IF ((J==JB(IL)) .AND. FACT(IL)>0.) THEN
!!              MD(IL,J)=-1.
!!          END IF
!!
!!          IF ((J<JB(IL)) .AND. ZDEF(IL)>0. .AND. EPS0(IL)>0.) THEN
!!            MD(IL,J)=-ALFA(IL)/(2.*EPS0(IL))*(EXP(2.*EPS0(IL)*ZDEF(IL))-1.)/ZDEF(IL)
!!          END IF
!!      630 CONTINUE
!! \endcode
