!> \file
!> \brief The purpose of this subroutine is to evaluate the temperature and
!! water vapor mixing ratio of an air parcel give its moist static energy
!! and total water (vapor plus condensate) mixing ratio.
!!
!! @author Norm McFarlane, John Scinocca
!
subroutine tincld3(tc,ilg,ilev,msg,il1,il2,lal,wrk,hmnc,qc,qstc, &
                   zf,pf,rrl,grav,cpres,eps1,eps2,a,b,iskip)
  !-----------------------------------------------------------------------
  !     * oct 24/2006 - m.lazare.    new version for gcm15f:
  !     *                            - implicit none with necessary
  !     *                              promotion to work in 32-bit.
  !     * may 04/2006 - m.lazare/    previous version for gcm15e:
  !     *               k.vonsalzen. - calculation of saturation vapour
  !     *                              pressure modified to work over
  !     *                              complete vertical domain, by
  !     *                              specifying limit as es->p.
  !     *                              this requires passing in eps2.
  !     * feb 06/2004 - m.lazare/    previous version tincld for gcm15d:
  !     *               k.vonsalzen/
  !     *               n.mcfarlane.
  !
  !     * calculates in-cloud temperature and saturation water vapour
  !     * mixing ratio from moist static energy and total water mixing
  !     * ratio. it is required that the water vapour mixing ratio
  !     * from the input is larger than 0.
  !-----------------------------------------------------------------------
  implicit none
  !
  !     * multi-level work fields.
  !

  real*8, intent(inout),  dimension(ilg,ilev)         :: tc !< Parcel temperature \f$[K]\f$
  real*8, intent(inout),  dimension(ilg,ilev)         :: wrk !< Variable description\f$[units]\f$
  real*8, intent(in),  dimension(ilg,ilev)         :: zf !< Local height above ground of model levels \f$[m]\f$
  real*8, intent(inout),  dimension(ilg,ilev)         :: qstc !< Saturation water vapor mixing ratio \f$[kg/kg]\f$
  real*8, intent(in),  dimension(ilg,ilev)         :: pf !< Local ambient atmospheric pressure on model levels \f$[mb]\f$
  real*8, intent(in),  dimension(ilg,ilev)         :: hmnc !< Moist static energy for ambient air \f$[J/g]\f$
  real*8, intent(in),  dimension(ilg,ilev)         :: qc !< Ambient total water mixing ratio \f$[kg/kg]\f$

  !
  !     * single-level work fields.
  !
  integer, intent(in), dimension(ilg)              :: lal !< Lowest model level for saturation conditions to be evaluated \f$[1]\f$
  integer, intent(in), dimension(ilg)              :: iskip !< Switch to control execution of calculations in subroutine \f$[1]\f$

  !
  !     * scalar quantities passed in.
  !
  real, intent(in) :: rrl !< Latent heat of vaporization \f$[J/kg]\f$
  real, intent(in) :: grav   !< Gravity on Earth \f$[m s^{-2}]\f$
  real, intent(in) :: cpres   !< Heat capacity of dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: eps1 !< Ratio of molecular weight of water vapor to the mean molecular weight of dry air \f$[1]\f$
  real, intent(in) :: eps2 !< 1-EPS1 \f$[1]\f$
  real, intent(in) :: a !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$ \f$[units]\f$
  real, intent(in) :: b !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$\f$[units]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: msg !< Index of the starting level for prognostic moisture in the model \f$[1]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  real*8, parameter :: tmin = 150.
  real*8, parameter :: tmax = 400.
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !
  !     * internal scalar quantities.
  !
  real*8  :: awrk
  real*8  :: est
  real*8  :: estref
  real*8  :: qsttmp
  real*8  :: qtemp
  real*8  :: ttemp
  real*8  :: hmnpr
  real*8  :: dhmnpr
  real*8  :: etmp
  real    :: epslim
  integer :: l
  integer :: il
  !
  !-----------------------------------------------------------------------
  !     * initialize cloud base values based on unsaturated
  !     * conditions.
  !
  do l = msg + 2,ilev
    do il = il1,il2
      if (iskip(il) == 0) then
        qstc(il,l) = qc(il,l)
        wrk(il,l) = log(qstc(il,l))
        tc(il,l) = b/( a - log(pf(il,l)/( eps1/qstc(il,l) + 1. )) )
      end if
    end do
  end do ! loop 60
  !
  !     * calculate temperature and saturation mixing ratio using newton-
  !     * RAPHSON'S ITERATIVE METHOD UNDER SATURATED CONDITIONS. THE
  !     * iterations are performed for log(q*) as iteration variable in
  !     * order to ensure positive definiteness and convergence of the
  !     * method.
  !
  epslim = 0.001
  do l = ilev,msg + 2, - 1
    do il = il1,il2
      if (l <= lal(il) .and. iskip(il) == 0) then
        awrk = ( hmnc(il,l) - rrl * qc(il,l) - grav * zf(il,l) )/cpres
        awrk = min(max(awrk,tmin),tmax)
        est = exp(a - b/awrk)
        estref = pf(il,l) * (1. - epslim)/(1. - epslim * eps2)
        if (est >= estref) then
          !
          !              *** no saturation.
          !
          tc(il,l) = awrk
          qstc(il,l) = 1. - epslim
        else
          qsttmp = eps1 * est/(pf(il,l) - est)
          if (qsttmp >= qc(il,l) ) then
            !
            !                 *** no saturation.
            !
            tc(il,l) = awrk
            qstc(il,l) = qsttmp
          else
            !
            !                 *** first iteration.
            !
            qtemp = qstc(il,l)
            ttemp = tc(il,l)
            hmnpr = cpres * ttemp + grav * zf(il,l) + rrl * qtemp &
                    - hmnc(il,l)
            dhmnpr = cpres * eps1 * ttemp * ttemp/( b * (eps1 + qtemp) ) &
                     + rrl * qtemp
            wrk(il,l) = wrk(il,l) - hmnpr/dhmnpr
            !
            !                 *** second iteration.
            !
            qsttmp = eps1 * est/(pf(il,l) - est)
            qtemp = max(exp(wrk(il,l)),qsttmp)
            ttemp = b/( a - log(pf(il,l)/(eps1/qtemp + 1.)) )
            hmnpr = cpres * ttemp + grav * zf(il,l) + rrl * qtemp &
                    - hmnc(il,l)
            dhmnpr = cpres * eps1 * ttemp * ttemp/( b * (eps1 + qtemp) ) &
                     + rrl * qtemp
            wrk(il,l) = wrk(il,l) - hmnpr/dhmnpr
            !
            !                 *** third iteration.
            !
            qtemp = max(exp(wrk(il,l)),qsttmp)
            ttemp = b/( a - log(pf(il,l)/(eps1/qtemp + 1.)) )
            hmnpr = cpres * ttemp + grav * zf(il,l) + rrl * qtemp &
                    - hmnc(il,l)
            dhmnpr = cpres * eps1 * ttemp * ttemp/( b * (eps1 + qtemp) ) &
                     + rrl * qtemp
            wrk(il,l) = wrk(il,l) - hmnpr/dhmnpr
            !
            !                 *** retrieve saturation water mixing ratio and
            !                 *** temperature.
            !
            qstc(il,l) = max(exp(wrk(il,l)),qsttmp)
            if (qstc(il,l) > 0. ) then
              tc(il,l) = b/(a - log(pf(il,l)/(eps1/qstc(il,l) + 1.)))
            else
              tc(il,l) = 0.
            end if
          end if
        end if
        !
        !           *** allow only temperatures between tmin and tmax.
        !
        tc(il,l) = min(max(tc(il,l),tmin),tmax)
        etmp = exp(a - b/tc(il,l))
        if (etmp < estref) then
          est = etmp
        else
          est = estref
        end if
        qstc(il,l) = eps1 * est/(pf(il,l) - est)
      end if
    end do
  end do ! loop 80
  !
  return
end subroutine tincld3
!> \file
!! \section sec_theory Theoretical formulation
!! \n
!! The subroutine makes use of the following accurate empirical formula
!! for the saturation vapor pressure of water (\f$e_{*})\f$ as a function
!! of its temperature (\f$T\f$):
!! \n
!! \f{equation}{
!! e_{*}=exp[-(A-B/T)]\label{eq_1}\tag{1}
!! \f}
!! \n
!! The corresponding formula for the saturation water vapor mixing ratio is
!! \n
!! \f{equation}{
!! r_{*}(T,p)=\epsilon e_{*}/(p-e_{*})\label{eq_2}\tag{2}
!! \f}
!! \n
!! This equation can, alternatively, be used to express the parcel temperature
!! as a function of its water vapor mixing ratio in saturated conditions as
!! \n
!! \f{equation}{
!! T(r_{*},p)=\frac{B}{A-ln(\frac{P}{\epsilon/r_{*}+1})}\label{eq_3}\tag{3}
!! \f}
!! \n
!! Given the input values of the parcel moist static energy (\f$h^{(p)}\f$)
!! at a given height (\f$z\f$) above the surface, and the corresponding
!! total water mixing ratio (\f$r_{c}^{(p)}=r_{v}^{(p)}+r_{l}^{(p)}+r_{i}^{(p)}\f$)
!! the parcel temperature and water vapor mixing ration can be determined
!! from the following relationships:
!! \n
!! \subsection ssrc_unsat_conditions Unsaturated conditions
!! \n
!! \f{equation}{
!! r_{v}^{(p)}=r_{c}^{(p)}\label{eq_4}\tag{4}
!! \f}
!! \n
!! \f{equation}{
!! T^{(p)}=(h^{(p)}-L_{v}r_{v}^{(p)}-gz)/c_{p}\label{eq_5}\tag{5}
!! \f}
!! \n
!! where \f$L_{v}\f$ is the latent heat of vaporization and \f$c_{p}\f$ is
!! the specific heat at constant pressure.}}
!! \n
!! \subsection ssec_sat_conditions Saturated conditions
!! \n
!! In the subroutine these estimates of the parcel water vapor mixing
!! ratio and temperature are evaluated initially. Using this estimated
!! temperature and the ambient atmospheric pressure, a reference saturation
!! value of the mixing ratio is evaluated using equations (1) and (2).
!! If this reference saturation mixing ratio is less that the estimated
!! parcel mixing ration then it is assumed that the parcel is saturated
!! with respect to water vapor. In this case the moist static energy
!! is held at the input value (\f$h^{(p)})\f$ but, since saturation conditions
!! apply,
!! \n
!! \f{equation}{
!! h(T^{(p)},r_{*}^{(p)},p,z)=c_{p}T^{(p)}+L_{v}r_{*}(T^{(p)},p)+gz=h^{(p)}\label{eq_6}\tag{6}
!! \f}
!! \n
!! For given ambient values of the pressure and height above ground,
!! this equation is a non-linear (transcendental) function of the temperature
!! and/or of the water vapor mixing ratio. In these circumstances a classical
!! Newton-Raphson iterative procedure can be used to obtain accurate
!! estimates of the parcel temperature and water vapor mixing ratio that
!! satisfy equations (1), (2) and (6). This involves choosing either
!! the temperature or the water vapor mixing ratio as the iteration variable
!! as they are related through equations (1) and (2) above, from which
!! it follows that , in saturated conditions,
!! \n
!! \f{equation}{
!! ln(p)+ln(r_{*})-ln(\epsilon+r_{*})=A-B/T^{(p)} \label{eq_7}\tag{7}
!! \f}
!! \n
!! the subroutine uses \f$ln(r_{*})\f$ as the iteration variable with the
!! iteration procedure being defined as
!! \n
!! \f{equation}{
!! [ln(r_{*})]^{(i)}=[ln(r_{*})]^{(i-1)}-\frac{[h-h^{(p)}]^{(i-1)}}{[\partial h/\partial ln(r_{*})]^{(i-1)}}\label{eq_8}\tag{8}
!! \f}
!! \n
!! where \f$i\f$is the iteration number. Note that the ambient atmospheric
!! pressure and height are locally fixed variables for iteration purposes.
!! Given the value of \f$r_{*}^{(i)}\f$, the value of the corresponding
!! estimated parcel temperature is obtained using equation (3) above.
!! It is easily shown that
!! \n
!! \f{equation}{
!! \frac{\partial T^{(p)}}{\partial ln(r_{*})}=\frac{(T^{(p)})^{2}}{B}\frac{\epsilon}{\epsilon+r_{*}}\label{eq_9}\tag{9}
!! \f}
!! \n
!! \f{equation}{
!! \frac{\partial h}{\partial ln(r_{*})}=c_{p}\frac{(T^{(p)})^{2}}{B}\left(\frac{\epsilon}{\epsilon+r_{*}}\right)+L_{v}r_{*}\label{eq_10}\tag{10}
!! \f}
!! \n
!! Typically \f$\epsilon>>r_{*}.\f$ and therefore all of the quantities
!! in these equations are strongly positive and well-behaved. The iteration
!! procedure converges sufficiently rapidly that only three iterations
!! are needed to achieve a high degree of accuracy. At the third iteration
!! stage, the final water vapour mixing ratio and temperature are evaluated
!! as
!! \n
!! \f{equation}{
!! r_{v}^{(p)}=r_{*}^{(3)}; T^{(p)}=T(r_{*}^{(3)},p)\label{eq_11}\tag{11}
!! \f}
