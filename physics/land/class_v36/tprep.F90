subroutine tprep(thliqc, thliqg, thicec, thiceg, tbarc,  tbarg, &
                       tbarcs, tbargs, hcpc,   hcpg,   tctopc, tcbotc, &
                       tctopg, tcbotg, hcpscs, hcpsgs, tcsnow, tsnocs, &
                       tsnogs, wsnocs, wsnogs, rhoscs, rhosgs, tcano, &
                       tcans,  cevap,  ievap,  tbar1p, wtable, zero, &
                       evapc,  evapcg, evapg,  evapcs, evpcsg, evapgs, &
                       gsnowc, gsnowg, gzeroc, gzerog, gzrocs, gzrogs, &
                       qmeltc, qmeltg, evap,   gsnow, &
                       tpondc, tpondg, tpndcs, tpndgs, qsensc, qsensg, &
                       qevapc, qevapg, tacco,  qacco,  taccs,  qaccs, &
                       ilmox,  uex,    hblx, &
                       ilmo,   ue,     hbl, &
                       st,     su,     sv,     sq,     srh, &
                       cdh,    cdm,    qsens,  qevap,  qlwavg, &
                       fsgv,   fsgs,   fsgg,   flgv,   flgs,   flgg, &
                       hfsc,   hfss,   hfsg,   hevc,   hevs,   hevg, &
                       hmfc,   hmfn,   qfcf,   qfcl,   evppot, acond, &
                       drag,   thliq,  thice,  tbar,   zpond,  tpond, &
                       thpor,  thlmin, thlret, thfc,   hcps,   tcs, &
                       ta,     rhosno, tsnow,  zsnow,  wsnow,  tcan, &
                       fc,     fcs,    delz,   delzw,  zbotw, &
                       isand,  ilg,    il1,    il2,    jl,     ig, &
                       fveg,   tcsatu, tcsatf, ftemp,  ftempx, fvap, &
                       fvapx,  rib,    ribx)
  !
  !     * feb 09/15 - d.verseghy. new version for gcm18 and class 3.6:
  !     *                         - initialize new surface relative
  !     *                           humidity field srh to zero.
  !     *                         - impose accuracy limit on comparison
  !     *                           between delz and delzw in calculation
  !     *                           of tcbotc and tcbotg (3 places).
  !     * jun 21/13 - m.lazare.   pass in and initialize to zero "GSNOW".
  !     * nov 24/11 - r.harvey.   new snow thermal conductivity from
  !     *                         sturm et al. (1997).
  !     * oct 12/11 - m.lazare.   removed tsurf.
  !     * aug   /08 - j.p.paquin. add calculation for ftemp, fvap and
  !     *                         rib for output in gem (implemented by
  !     *                         l. duarte on oct. 28/08).
  !     * mar 20/08 - d.verseghy. remove tbar3, tctop3, tcbot3.
  !     * dec 12/07 - d.verseghy. major revisions to calculation of
  !     *                         soil thermal conductivity.
  !     * may 18/06 - d.verseghy. adjust calculation of tbar1p for rock
  !     *                         soils; limit calculation of tbar3(i,3)
  !     *                         to upper 4.1 m of soil; correct wtable
  !     *                         to account for presence of ice.
  !     * mar 23/06 - d.verseghy. modify calculation of hcpsno to account
  !     *                         for presence of water in snowpack.
  !     * mar 21/06 - p.bartlett. initialize additional variables to zero.
  !     * oct 04/05 - d.verseghy. new variables tbar3,tctop3,tcbot3.
  !     * apr 08/05 - y.delage. tctop varies gradually with zpond to tcw.
  !     * mar 16/05 - d.verseghy. treat frozen soil water as ice
  !     *                         volume rather than as equivalent
  !     *                         liquid water volume; reverse order
  !     *                         in loop 500.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * aug 05/04 - y.delage/d.verseghy. initialize new diagnostic
  !     *                         variables ilmo, ue and hbl.
  !     * jul 30/02 - d.verseghy. move calculation of vegetation
  !     *                         stomatal resistance into aprep
  !     *                         and canalb; shortened class3
  !     *                         common block.
  !     * jun 17/02 - d.verseghy. new thermal arrays for surface
  !     *                         temperature iteration, with ponded
  !     *                         water rolled into soil upper layer
  !     *                         shortened class4 common block.
  !     * mar 20/02 - d.verseghy. move calculation of background soil
  !     *                         properties into "CLASSB"; updates
  !     *                         to make zpond a prognostic variable.
  !     * feb 27/02 - d.verseghy. recalculate wilting point based on
  !     *                         field capacity.
  !     * jan 18/02 - d.verseghy. introduction of calculation of field
  !     *                         capacity and new bare soil evaporation
  !     *                         parameters.
  !     * apr 11/01 - m.lazare.   shortened "CLASS2" common block.
  !     * nov 01/00 - a.wu/d.verseghy. extend mineral soil calculation
  !     *                              of soil evaporation "BETA" to
  !     *                              organic soils.
  !     * sep 19/00 - a.wu/d.verseghy. change calculation of thermal
  !     *                              conductivity for organic soils,
  !     *                              using method of farouki (1981).
  !     *                              also, calculate stomatal resistance
  !     *                              using vegetation-varying
  !     *                              coefficients for environmental
  !     *                              variables.
  !     * feb 14/00 - d.verseghy. insert calculation of water table depth
  !     *                         for organic soils.
  !     * dec 07/99 - a.wu/d.verseghy.  incorporate calculation of "BETA"
  !     *                               parameter for new soil evaporation
  !     *                               formulation.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         changes related to variable soil depth
  !     *                         (moisture holding capacity) and depth-
  !     *                         varying soil properties.
  !     * jan 24/97 - d.verseghy. class - version 2.6.
  !     *                         set rc and rcs to zero for grid cells
  !     *                         with no vegetation.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * aug 30/95 - d.verseghy. class - version 2.4.
  !     *                         remove subtraction of residual soil
  !     *                         moisture content in calculations of
  !     *                         "PSIZRO" and "PSII".
  !     * aug 18/95 - d.verseghy. revisions to allow for inhomogeneity
  !     *                         between soil layers and fractional
  !     *                         organic matter content.
  !     * dec 16/94 - d.verseghy. class - version 2.3.
  !     *                         initialize three new diagnostic fields.
  !     * nov 12/94 - d.verseghy. set initial temperature of emerging
  !     *                         canopy to ta instead of to zero.
  !     * jan 31/94 - d.verseghy. class - version 2.2.
  !     *                         introduce limiting values into
  !     *                         calculation of "PSIZRO" to avoid
  !     *                         overflows.
  !     * jul 27/93 - d.verseghy/m.lazare. initialize new diagnostic
  !     *                                  fields fsgv,fsgg,flgv,flgg,
  !     *                                  hfsc,hfsg,hmfc.
  !     * may 06/93 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  modifications to canopy
  !     *                                  resistance to add "RCS"
  !     *                                  field for snow-covered
  !     *                                  canopy.
  !     * jul 04/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. preparation and initialization for
  !     *                         land surface energy budget
  !     *                         calculations.
  !
  use hydcon, only : hcpw, hcpice, hcpsnd, rhow, rhoice, tcw, tcice, tcsand, tcglac
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ig !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: tbarc (ilg,ig) !<
  real, intent(inout) :: tbarg (ilg,ig) !<
  real, intent(inout) :: tbarcs(ilg,ig) !<
  real, intent(inout) :: tbargs(ilg,ig) !<
  real, intent(inout) :: thliqc(ilg,ig) !<
  real, intent(inout) :: thliqg(ilg,ig) !<
  real, intent(inout) :: thicec(ilg,ig) !<
  real, intent(inout) :: thiceg(ilg,ig) !<
  real, intent(inout) :: hcpc  (ilg,ig) !<
  real, intent(inout) :: hcpg  (ilg,ig) !<
  real, intent(inout) :: tctopc(ilg,ig) !<
  real, intent(inout) :: tcbotc(ilg,ig) !<
  real, intent(inout) :: tctopg(ilg,ig) !<
  real, intent(inout) :: tcbotg(ilg,ig) !<
  !
  real, intent(inout) :: hcpscs(ilg) !<
  real, intent(inout) :: hcpsgs(ilg) !<
  real, intent(inout) :: tcsnow(ilg) !<
  real, intent(inout) :: tsnogs(ilg) !<
  real, intent(inout) :: tsnocs(ilg) !<
  real, intent(inout) :: wsnocs(ilg) !<
  real, intent(inout) :: wsnogs(ilg) !<
  real, intent(inout) :: rhoscs(ilg) !<
  real, intent(inout) :: rhosgs(ilg) !<
  real, intent(inout) :: tcano (ilg) !<
  real, intent(inout) :: tcans (ilg) !<
  real, intent(inout) :: cevap (ilg) !<
  real, intent(inout) :: tbar1p(ilg) !<
  real, intent(inout) :: wtable(ilg) !<
  real, intent(inout) :: zero  (ilg) !<
  real, intent(inout) :: tpondc(ilg) !<
  real, intent(inout) :: tpondg(ilg) !<
  real, intent(inout) :: tpndcs(ilg) !<
  real, intent(inout) :: tpndgs(ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: ievap !<
  !
  !     * output arrays which are internal work arrays for classt
  !     * and are initialized to zero here.
  !
  real, intent(inout) :: evapc (ilg) !<
  real, intent(inout) :: evapcg(ilg) !<
  real, intent(inout) :: evapg (ilg) !<
  real, intent(inout) :: evapcs(ilg) !<
  real, intent(inout) :: evpcsg(ilg) !<
  real, intent(inout) :: evapgs(ilg) !<
  real, intent(inout) :: gsnowc(ilg) !<
  real, intent(inout) :: gsnowg(ilg) !<
  real, intent(inout) :: gzeroc(ilg) !<
  real, intent(inout) :: gzerog(ilg) !<
  real, intent(inout) :: gzrocs(ilg) !<
  real, intent(inout) :: gzrogs(ilg) !<
  real, intent(inout) :: qmeltc(ilg) !<
  real, intent(inout) :: qmeltg(ilg) !<
  real, intent(inout) :: evap  (ilg) !<
  real, intent(inout) :: gsnow (ilg) !<
  real, intent(inout) :: qsensc(ilg) !<
  real, intent(inout) :: qsensg(ilg) !<
  real, intent(inout) :: qevapc(ilg) !<
  real, intent(inout) :: qevapg(ilg) !<
  real, intent(inout) :: tacco (ilg) !<
  real, intent(inout) :: qacco (ilg) !<
  real, intent(inout) :: taccs (ilg) !<
  real, intent(inout) :: qaccs (ilg) !<
  !
  !     * diagnostic arrays.
  !
  real, intent(inout) :: st    (ilg) !<
  real, intent(inout) :: su    (ilg) !<
  real, intent(inout) :: sv    (ilg) !<
  real, intent(inout) :: sq    (ilg) !<
  real, intent(inout) :: srh   (ilg) !<
  real, intent(inout) :: cdh   (ilg) !<
  real, intent(inout) :: cdm   (ilg) !<
  real, intent(inout) :: qsens (ilg) !<
  real, intent(inout) :: qevap (ilg) !<
  real, intent(inout) :: qlwavg(ilg) !<
  real, intent(inout) :: fsgv  (ilg) !<
  real, intent(inout) :: fsgs  (ilg) !<
  real, intent(inout) :: fsgg  (ilg) !<
  real, intent(inout) :: flgv  (ilg) !<
  real, intent(inout) :: flgs  (ilg) !<
  real, intent(inout) :: flgg  (ilg) !<
  real, intent(inout) :: hfsc  (ilg) !<
  real, intent(inout) :: hfss  (ilg) !<
  real, intent(inout) :: hfsg  (ilg) !<
  real, intent(inout) :: hevc  (ilg) !<
  real, intent(inout) :: hevs  (ilg) !<
  real, intent(inout) :: hevg  (ilg) !<
  real, intent(inout) :: hmfc  (ilg) !<
  real, intent(inout) :: hmfn  (ilg) !<
  real, intent(inout) :: qfcf  (ilg) !<
  real, intent(inout) :: qfcl  (ilg) !<
  real, intent(inout) :: evppot(ilg) !<
  real, intent(inout) :: acond (ilg) !<
  real, intent(inout) :: drag  (ilg) !<
  real, intent(inout) :: ilmo  (ilg) !<
  real, intent(inout) :: ue    (ilg) !<
  real, intent(inout) :: hbl   (ilg) !<
  real, intent(inout) :: ilmox (ilg) !<
  real, intent(inout) :: uex   (ilg) !<
  real, intent(inout) :: hblx  (ilg) !<
  real, intent(inout) :: ftemp (ilg) !<
  real, intent(inout) :: ftempx(ilg) !<
  real, intent(inout) :: fvap  (ilg) !<
  real, intent(inout) :: fvapx (ilg) !<
  real, intent(inout) :: rib   (ilg) !<
  real, intent(inout) :: ribx  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: thliq (ilg,ig) !<
  real, intent(in) :: thice (ilg,ig) !<
  real, intent(in) :: tbar  (ilg,ig) !<
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: tpond (ilg) !<
  !
  real, intent(in) :: ta    (ilg) !<
  real, intent(in) :: rhosno(ilg) !<
  real, intent(in) :: tsnow (ilg) !<
  real, intent(in) :: zsnow (ilg) !<
  real, intent(in) :: wsnow (ilg) !<
  real, intent(in) :: tcan  (ilg) !<
  real, intent(in) :: fc    (ilg) !<
  real, intent(in) :: fcs   (ilg) !<
  !
  !     * soil property arrays.
  !
  real, intent(in) :: thpor(ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: thlret(ilg,ig) !<
  real, intent(in) :: thfc  (ilg,ig) !<
  real, intent(in) :: hcps  (ilg,ig) !<
  real, intent(in) :: tcs   (ilg,ig) !<

  real, intent(in) :: delzw(ilg,ig) !<
  real, intent(in) :: zbotw(ilg,ig) !<
  real, intent(in) :: delz(ig) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * internal work fields for this routine.
  !
  real, intent(inout) :: fveg  (ilg) !<
  real, intent(inout) :: tcsatu(ilg) !<
  real, intent(inout) :: tcsatf(ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  real :: satrat !<
  real :: thlsat !<
  real :: thisat !<
  real :: tcdry !<
  real :: tckapu !<
  real :: tckapf !<
  real :: tcratu !<
  real :: tcratf !<
  real :: tcsolu !<
  real :: tcsolf !<
  real :: tcsoil !<
  real :: tsum1 !<
  real :: tsum2 !<
  real :: zsum !<
  !
  !----------------------------------------------------------------------
  !     * initialize 2-d and 3-d arrays.
  !
  do j=1,ig
    do i=il1,il2
      thliqg(i,j)=thliq(i,j)
      thiceg(i,j)=thice(i,j)
      thliqc(i,j)=thliq(i,j)
      thicec(i,j)=thice(i,j)
      tbarcs(i,j)=0.0
      tbargs(i,j)=0.0
      tbarc (i,j)=0.0
      tbarg (i,j)=0.0
      tctopc(i,j)=0.0
      tcbotc(i,j)=0.0
      tctopg(i,j)=0.0
      tcbotg(i,j)=0.0
    end do
  end do ! loop 50
  !
  !     * initialize 1-d internal work fields and diagnostic arrays.
  !
  do i=il1,il2
    fveg  (i)=fc(i)+fcs(i)
    if (tcan(i)>5.0) then
      tcans (i)=tcan(i)
      tcano (i)=tcan(i)
    else
      tcans (i)=ta(i)
      tcano (i)=ta(i)
    end if
    evapc (i)=0.
    evapcg(i)=0.
    evapg (i)=0.
    evapcs(i)=0.
    evpcsg(i)=0.
    evapgs(i)=0.
    gsnowc(i)=0.
    gsnowg(i)=0.
    gzeroc(i)=0.
    gzerog(i)=0.
    gzrocs(i)=0.
    gzrogs(i)=0.
    qmeltc(i)=0.
    qmeltg(i)=0.
    qsensc(i)=0.
    qsensg(i)=0.
    qevapc(i)=0.
    qevapg(i)=0.
    tpondc(i)=0.
    tpondg(i)=0.
    tpndcs(i)=0.
    tpndgs(i)=0.
    taccs (i)=0.
    qaccs (i)=0.
    tacco (i)=0.
    qacco (i)=0.
    st    (i)=0.
    su    (i)=0.
    sv    (i)=0.
    sq    (i)=0.
    srh   (i)=0.
    cdh   (i)=0.
    cdm   (i)=0.
    qsens (i)=0.
    qevap (i)=0.
    evap  (i)=0.
    qlwavg(i)=0.
    fsgv  (i)=0.
    fsgs  (i)=0.
    fsgg  (i)=0.
    flgv  (i)=0.
    flgs  (i)=0.
    flgg  (i)=0.
    hfsc  (i)=0.
    hfss  (i)=0.
    hfsg  (i)=0.
    hevc  (i)=0.
    hevs  (i)=0.
    hevg  (i)=0.
    hmfc  (i)=0.
    hmfn  (i)=0.
    qfcf  (i)=0.
    qfcl  (i)=0.
    evppot(i)=0.
    acond (i)=0.
    drag  (i)=0.
    ilmo  (i)=0.
    ue    (i)=0.
    hbl   (i)=0.
    ilmox (i)=0.
    uex   (i)=0.
    hblx  (i)=0.
    zero  (i)=0.
    ftemp (i)=0.
    fvap  (i)=0.
    rib   (i)=0.
    gsnow (i)=0.
    ftempx(i)=0.
    fvapx (i)=0.
    ribx  (i)=0.
    wtable(i)=9999.
  end do ! loop 100
  !
  !     * surface evaporation efficiency for bare soil energy balance
  !     * calculations.
  !
  do i=il1,il2
    if (thliqg(i,1)<(thlmin(i,1)+0.001)) then
      ievap(i)=0
      cevap(i)=0.0
    else if (thliqg(i,1)>thfc(i,1)) then
      ievap(i)=1
      cevap(i)=1.0
    else
      ievap(i)=1
      cevap(i)=0.25*(1.0-cos(3.14159*thliqg(i,1)/thfc(i,1)))**2
    end if
  end do ! loop 200
  !
  !     * volumetric heat capacities of soil layers.
  !
  do j=1,ig
    do i=il1,il2
      if (isand(i,1)>-4) then
        hcpg(i,j)=hcpw*thliqg(i,j)+hcpice*thiceg(i,j)+ &
             hcps(i,j)*(1.0-thpor(i,j))
        hcpc(i,j)=hcpw*thliqc(i,j)+hcpice*thicec(i,j)+ &
             hcps(i,j)*(1.0-thpor(i,j))
      else
        hcpc(i,j)=hcpice
        hcpg(i,j)=hcpice
      end if
    end do
  end do ! loop 300
  !
  !     * thermal properties of snow.
  !
  do i=il1,il2
    if (zsnow(i)>0.) then
      hcpscs(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
             (rhow*zsnow(i))
      hcpsgs(i)=hcpscs(i)
      !             tcsnow(i)=2.576e-6*rhosno(i)*rhosno(i)+0.074
      if (rhosno(i)<156.0) then
        tcsnow(i)=0.234e-3*rhosno(i)+0.023
      else
        tcsnow(i)=3.233e-6*rhosno(i)*rhosno(i)-1.01e-3* &
                 rhosno(i)+0.138
      end if
      if (fveg(i)<1.) then
        tsnogs(i)=tsnow(i)
        wsnogs(i)=wsnow(i)
        rhosgs(i)=rhosno(i)
      else
        tsnogs(i)=0.0
        wsnogs(i)=0.0
        rhosgs(i)=0.0
      end if
      if (fveg(i)>0.) then
        tsnocs(i)=tsnow(i)
        wsnocs(i)=wsnow(i)
        rhoscs(i)=rhosno(i)
      else
        tsnocs(i)=0.0
        wsnocs(i)=0.0
        rhoscs(i)=0.0
      end if
    else
      tsnogs(i)=0.0
      wsnogs(i)=0.0
      rhosgs(i)=0.0
      tsnocs(i)=0.0
      wsnocs(i)=0.0
      rhoscs(i)=0.0
      tcsnow(i)=0.0
    end if
  end do ! loop 400
  !
  !     * thermal conductivities of soil layers and depth of water
  !     * table in organic soils.
  !
  do j=ig,1,-1
    do i=il1,il2
      if    (isand(i,1)==-4) then
        tctopg(i,j)=tcglac
        tcbotg(i,j)=tcglac
      else if (isand(i,j)==-3) then
        tctopc(i,j)=tcsand
        tctopg(i,j)=tcsand
        tcbotc(i,j)=tcsand
        tcbotg(i,j)=tcsand
      else if (isand(i,j)==-2) then
        if ((thliqg(i,j)+thiceg(i,j))>(thlret(i,j)+0.0001)) &
            then
          wtable(i)=zbotw(i,j)-delzw(i,j)*min(1.0, &
                       (thliqg(i,j)+thiceg(i,j)-thlret(i,j))/ &
                       (thpor(i,j)-thlret(i,j)))
        end if
        if (thliqg(i,j)>(thlret(i,j)+0.0001)) then
          satrat=min((thlret(i,j)+thiceg(i,j))/ &
                    thpor(i,j), 1.0)
          thlsat=thliqg(i,j)/(thliqg(i,j)+thiceg(i,j))
          thisat=thiceg(i,j)/(thliqg(i,j)+thiceg(i,j))
          tcdry=0.30*exp(-2.0*thpor(i,j))
          tcsatu(i)=tcw*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
          tcsatf(i)=tcice*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
          tcratu=0.6*satrat/(1.0-0.4*satrat)
          tcratf=0.25*satrat/(1.0-0.75*satrat)
          tcsolu=(tcsatu(i)-tcdry)*tcratu+tcdry
          tcsolf=(tcsatf(i)-tcdry)*tcratf+tcdry
          tcsoil=tcsolu*thlsat+tcsolf*thisat
          if (delzw(i,j)>0.0) then
            tctopc(i,j)=tcsoil
            tctopg(i,j)=tcsoil
          else
            tctopc(i,j)=tcsand
            tctopg(i,j)=tcsand
          end if
          if (delzw(i,j)<(delz(j)-0.01)) then
            tcbotc(i,j)=tcsand
            tcbotg(i,j)=tcsand
          else
            tcbotc(i,j)=tcsatu(i)*thlsat+tcsatf(i)*thisat
            tcbotg(i,j)=tcsatu(i)*thlsat+tcsatf(i)*thisat
          end if
          if (j==1) then
            tctopc(i,j)=tctopc(i,j)+(tcw-tctopc(i,j))* &
                             min(zpond(i),1.0e-2)*100.0
            tctopg(i,j)=tctopc(i,j)
          end if
        else
          satrat=min((thliqg(i,j)+thiceg(i,j))/ &
                    thpor(i,j), 1.0)
          thlsat=thliqg(i,j)/(thliqg(i,j)+thiceg(i,j))
          thisat=thiceg(i,j)/(thliqg(i,j)+thiceg(i,j))
          tcdry=0.30*exp(-2.0*thpor(i,j))
          tcsatu(i)=tcw*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
          tcsatf(i)=tcice*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
          tcratu=0.6*satrat/(1.0-0.4*satrat)
          tcratf=0.25*satrat/(1.0-0.75*satrat)
          tcsolu=(tcsatu(i)-tcdry)*tcratu+tcdry
          tcsolf=(tcsatf(i)-tcdry)*tcratf+tcdry
          tcsoil=tcsolu*thlsat+tcsolf*thisat
          if (delzw(i,j)>0.0) then
            tctopc(i,j)=tcsoil
            tctopg(i,j)=tcsoil
          else
            tctopc(i,j)=tcsand
            tctopg(i,j)=tcsand
          end if
          if (delzw(i,j)<(delz(j)-0.01)) then
            tcbotc(i,j)=tcsand
            tcbotg(i,j)=tcsand
          else
            tcbotc(i,j)=tcsoil
            tcbotg(i,j)=tcsoil
          end if
          if (j==1) then
            tctopc(i,j)=tctopc(i,j)+(tcw-tctopc(i,j))* &
                             min(zpond(i),1.0e-2)*100.0
            tctopg(i,j)=tctopc(i,j)
          end if
        end if
      else
        satrat=min((thliqg(i,j)+thiceg(i,j))/ &
                thpor(i,j), 1.0)
        thlsat=thliqg(i,j)/(thliqg(i,j)+thiceg(i,j))
        thisat=thiceg(i,j)/(thliqg(i,j)+thiceg(i,j))
        tcdry=0.75*exp(-2.76*thpor(i,j))
        tcsatu(i)=tcw*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
        tcsatf(i)=tcice*thpor(i,j)+tcs(i,j)*(1.0-thpor(i,j))
        tckapu=(4.0*real(isand(i,j))+1.9*real(100-isand(i,j)))/ &
                100.0
        tckapf=(1.2*real(isand(i,j))+0.85*real(100-isand(i,j)))/ &
                100.0
        tcratu=tckapu*satrat/(1.0+(tckapu-1.0)*satrat)
        tcratf=tckapf*satrat/(1.0+(tckapf-1.0)*satrat)
        tcsolu=(tcsatu(i)-tcdry)*tcratu+tcdry
        tcsolf=(tcsatf(i)-tcdry)*tcratf+tcdry
        tcsoil=tcsolu*thlsat+tcsolf*thisat
        if (delzw(i,j)>0.0) then
          tctopc(i,j)=tcsoil
          tctopg(i,j)=tcsoil
          !                  if (j==1) tctopc(i,j)=tctopc(i,j)*0.1
        else
          tctopc(i,j)=tcsand
          tctopg(i,j)=tcsand
        end if
        if (delzw(i,j)<(delz(j)-0.01)) then
          tcbotc(i,j)=tcsand
          tcbotg(i,j)=tcsand
        else
          tcbotc(i,j)=tcsoil
          tcbotg(i,j)=tcsoil
        end if
        if (j==1) then
          tctopc(i,j)=tctopc(i,j)+(tcw-tctopc(i,j))* &
                         min(zpond(i),1.0e-2)*100.0
          tctopg(i,j)=tctopc(i,j)
        end if
      end if
    end do
  end do ! loop 500
  !
  !     * add ponded water temperature to first soil layer for use
  !     * in ground heat flux calculations.
  !
  do i=il1,il2
    if (zpond(i)>0.) then
      tbar1p(i)=(tpond(i)*hcpw*zpond(i) + &
                   tbar(i,1)*(hcpg(i,1)*delzw(i,1)+ &
                   hcpsnd*(delz(1)-delzw(i,1))))/ &
                   (hcpw*zpond(i)+hcpg(i,1)*delzw(i,1)+ &
                   hcpsnd*(delz(1)-delzw(i,1)))
    else
      tbar1p(i)=tbar(i,1)
    end if
  end do ! loop 600
  !
  return
end subroutine tprep
