subroutine grinfl(iveg,thliq,thice,tbarw,basflw,tbasfl, &
                        runoff,trunof,zfav,lzfav,thlinv,qfg, &
                        wlost,fi,evap,r,tr,tpond,zpond,dt, &
                        zmat,wmove,tmove,thliqx,thicex,tbarwx, &
                        delzx,zbotx,fdt,tfdt,psif,thlinf,grkinf, &
                        thlmax,thtest,zrmdr,fdummy,tdummy,thldum, &
                        thidum,tdumw,trmdr,zf,fmax,tused,rdummy, &
                        zero,wexces,fdtbnd,wadd,tadd,wadj,timpnd, &
                        dzf,dtflow,thlnlz,thlqlz,dzdisp,wdisp,wabs, &
                        thpor,thlret,thlmin,bi,psisat,grksat, &
                        thlrat,thfc,delzw,zbotw,xdrain,delz,isand, &
                        igrn,igrd,ifill,izero,lzf,ninf,ifind,iter, &
                        nend,isimp,igdr, &
                        ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  !     * oct 18/11 - m.lazare.   pass in "IGDR" as an input field
  !     *                         (originating in classb) to
  !     *                         grdran and wend.
  !     * apr 04/11 - d.verseghy. modify test in 150 loop to use delz
  !     *                         instead of xdrain.
  !     * jan 06/09 - d.verseghy. modify delzx and zbotx of bottom layer
  !     *                         additional thliq check in 350 loop
  !     *                         pass additional variables to wend.
  !     * mar 27/08 - d.verseghy. move viscosity adjustment to wprep.
  !     * oct 31/06 - r.soulis.   adjust grksat for viscosity of water
  !     *                         and presence of ice; adjust thpor for
  !     *                         presence of ice.
  !     * mar 22/06 - d.verseghy. unconditionally define variables for
  !     *                         all "IF" statements.
  !     * sep 28/05 - d.verseghy. remove hard coding of ig=3 in 400 loop.
  !     * mar 23/05 - d.verseghy.r.soulis. pass additional variables
  !     *                         to grdran and wend; pass out zfav,
  !     *                         lzfav, thlinv; calculate grktld
  !     *                         internally; revise calculation of
  !     *                         thlinf.
  !     * mar 16/04 - d.verseghy. treat frozen soil water as ice
  !     *                         volume rather than as equivalent
  !     *                         liquid water volume.
  !     * sep 24/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 27/04 - y.delage/d.verseghy. protect sensitive
  !     *                         calculations against roundoff errors.
  !     * jul 26/02 - d.verseghy. shortened class4 common block.
  !     * dec 12/01 - d.verseghy. pass new variable in for calculation
  !     *                         of baseflow.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * apr 17/96 - d.verseghy. class - version 2.5.
  !     *                         bug fix: initialize fdt and tfdt
  !     *                         to zero.
  !     * aug 18/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. update soil layer temperatures and
  !     *                         liquid moisture contents for
  !     *                         infiltrating conditions (i.e.
  !     *                         ponded water or rainfall occurring
  !     *                         within current timestep).
  !
  use times_mod, only : delt
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: iveg !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: igp1 !<
  integer, intent(in) :: igp2 !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: n !<
  !
  !     * input/output fields.
  !
  real, intent(inout) :: thliq (ilg,ig) !<
  real, intent(inout) :: thice (ilg,ig) !<
  real, intent(inout) :: tbarw (ilg,ig) !<
  !
  real, intent(in) :: basflw(ilg) !<
  real, intent(in) :: tbasfl(ilg) !<
  real, intent(in) :: runoff(ilg) !<
  real, intent(in) :: trunof(ilg) !<
  real, intent(in) :: qfg   (ilg) !<
  real, intent(in) :: wlost (ilg) !<
  real, intent(inout) :: zfav  (ilg) !<
  real, intent(inout) :: thlinv(ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: lzfav !<
  !
  !     * input fields.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: evap  (ilg) !<
  real, intent(in) :: r     (ilg) !<
  real, intent(in) :: tr    (ilg) !<
  real, intent(in) :: tpond (ilg) !<
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: dt    (ilg) !<
  !
  !     * work fields (for all called routines as well).
  !
  real, intent(in), dimension(ilg,igp2,igp1) :: zmat !<
  !
  real, intent(inout) :: wmove (ilg,igp2) !<
  real, intent(inout) :: tmove (ilg,igp2) !<
  !
  real, intent(inout) :: thliqx(ilg,igp1) !<
  real, intent(inout) :: thicex(ilg,igp1) !<
  real, intent(inout) :: tbarwx(ilg,igp1) !<
  real, intent(inout) :: delzx (ilg,igp1) !<
  real, intent(inout) :: zbotx (ilg,igp1) !<
  real, intent(inout) :: fdt   (ilg,igp1) !<
  real, intent(inout) :: tfdt  (ilg,igp1) !<
  real, intent(inout) :: psif  (ilg,igp1) !<
  real, intent(inout) :: thlinf(ilg,igp1) !<
  real, intent(inout) :: grkinf(ilg,igp1) !<
  real, intent(in) :: thlmax(ilg,ig) !<
  real, intent(in) :: thtest(ilg,ig) !<
  real, intent(in) :: zrmdr (ilg,igp1) !<
  real, intent(in) :: fdummy(ilg,igp1) !<
  real, intent(in) :: tdummy(ilg,igp1) !<
  real, intent(in) :: thldum(ilg,ig) !<
  real, intent(in) :: thidum(ilg,ig) !<
  real, intent(in) :: tdumw (ilg,ig) !<
  !
  real, intent(inout) :: trmdr (ilg) !<
  real, intent(inout) :: zf    (ilg) !<
  real, intent(in) :: fmax  (ilg) !<
  real, intent(in) :: tused (ilg) !<
  real, intent(inout) :: rdummy(ilg) !<
  real, intent(in) :: zero  (ilg) !<
  real, intent(in) :: wexces(ilg) !<
  real, intent(inout) :: fdtbnd(ilg) !<
  real, intent(in) :: wadd  (ilg) !<
  real, intent(in) :: tadd  (ilg) !<
  real, intent(in) :: wadj  (ilg) !<
  real, intent(in) :: timpnd(ilg) !<
  real, intent(in) :: dzf   (ilg) !<
  real, intent(in) :: dtflow(ilg) !<
  real, intent(in) :: thlnlz(ilg) !<
  real, intent(in) :: thlqlz(ilg) !<
  real, intent(in) :: dzdisp(ilg) !<
  real, intent(in) :: wdisp (ilg) !<
  real, intent(in) :: wabs  (ilg) !<
  !
  !     * soil information arrays.
  !
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: thlret(ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: bi    (ilg,ig) !<
  real, intent(in) :: psisat(ilg,ig) !<
  real, intent(in) :: grksat(ilg,ig) !<
  real, intent(in) :: thlrat(ilg,ig) !<
  real, intent(in) :: thfc  (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: zbotw (ilg,ig) !<
  real, intent(in) :: xdrain(ilg) !<
  real, intent(in) :: delz(ig) !<
  !
  !     * various integer :: arrays.
  !
  integer, intent(in)              :: isand (ilg,ig) !<
  integer, intent(inout)              :: igrn  (ilg) !<
  integer, intent(in)              :: igrd  (ilg) !<
  integer, intent(inout)              :: ifill (ilg) !<
  integer, intent(in)              :: izero (ilg) !<
  integer, intent(inout)              :: lzf   (ilg) !<
  integer, intent(inout)              :: ninf  (ilg) !<
  integer, intent(in)              :: ifind (ilg) !<
  integer, intent(in)              :: iter  (ilg) !<
  integer, intent(in)              :: nend  (ilg) !<
  integer, intent(in)              :: isimp (ilg) !<
  integer, intent(in)              :: igdr  (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  real :: grksatf(ilg,ig) !<
  real :: thporf(ilg,ig) !<
  real :: psiinf !<
  real :: grk !<
  real :: psi !<
  !-----------------------------------------------------------------------
  !     * determine points which satisfy conditions for these calculations
  !     * and store them as having non-zero values for work array "IGRN".
  !
  do i=il1,il2
    if (fi(i)>0. .and. &
        isand(i,1)>-4 .and. dt(i)>0. .and. &
        (r(i)>0. .or. zpond(i)>0.)) then
      igrn(i)=1
      rdummy(i)=0.
    else
      igrn(i)=0
    end if
  end do ! loop 50
  !
  !     * adjust grksat for viscosity of water and presence of ice
  !     * adjust thpor for presence of ice.
  !     * initialization; determination of soil hydraulic conductivities
  !     * and soil moisture suction across wetting front.
  !
  do j=1,ig
    do i=il1,il2
      if (igrn(i)>0) then
        thliqx(i,j)=thliq(i,j)
        thicex(i,j)=thice(i,j)
        tbarwx(i,j)=tbarw(i,j)
        delzx(i,j)=delzw(i,j)
        zbotx(i,j)=zbotw(i,j)
        fdt (i,j)=0.0
        tfdt(i,j)=0.0
        if (isand(i,j)>-3) then
          grksatf(i,j)=grksat(i,j)*(1.0-max(0.0,min(1.0, &
                 thice(i,j)/thpor(i,j))))**2
          thporf(i,j)=max((thpor(i,j)-thice(i,j)-0.00001), &
                 thliq(i,j),thlmin(i,j))
          thlinf(i,j)=max(thliq(i,j),thlmin(i,j), &
                         thlrat(i,j)*(thpor(i,j)- &
                         thice(i,j)-0.00001))
          grkinf(i,j)=grksatf(i,j)*(thlinf(i,j)/thporf(i,j)) &
                         **(2.*bi(i,j)+3.)
        else
          grksatf(i,j)=0.0
          thporf(i,j)=0.0
          thlinf(i,j)=0.0
          grkinf(i,j)=0.0
        end if
      end if
    end do
  end do ! loop 100
  !
  do i=il1,il2
    if (igrn(i)>0) then
      if (delzw(i,ig)<delz(ig)) then
        thliqx(i,ig+1)=0.0
        thicex(i,ig+1)=0.0
        tbarwx(i,ig+1)=0.0
        delzx(i,ig+1)=0.0
        thlinf(i,ig+1)=0.0
        grkinf(i,ig+1)=0.0
      else
        thliqx(i,ig+1)=thliqx(i,ig)
        thicex(i,ig+1)=thicex(i,ig)
        tbarwx(i,ig+1)=tbarwx(i,ig)
        delzx(i,ig+1)=999999.
        thlinf(i,ig+1)=thlinf(i,ig)
        grkinf(i,ig+1)=grkinf(i,ig)*xdrain(i)
      end if
      zbotx (i,ig+1)=zbotx(i,ig)+delzx(i,ig+1)
      fdt   (i,ig+1)=0.0
      tfdt  (i,ig+1)=0.0
    end if
  end do ! loop 150
  !
  do j=1,ig
    do i=il1,il2
      if (igrn(i)>0) then
        if (thpor(i,j)>0.0001) then
          psiinf=max(psisat(i,j)*(thlinf(i,j)/thporf(i,j))** &
                         (-bi(i,j)),psisat(i,j))
          grk=min(grksatf(i,j)*(thliq(i,j)/thporf(i,j))** &
                      (2.*bi(i,j)+3.),grksatf(i,j))
          psi=max(psisat(i,j)*(thliq(i,j)/thporf(i,j))** &
                      (-bi(i,j)),psisat(i,j))
        else
          psiinf=psisat(i,j)
          grk=grksatf(i,j)
          psi=psisat(i,j)
        end if
        if (thlinf(i,j)>thliq(i,j)) then
          psif (i,j)=max(bi(i,j)*(grkinf(i,j)*psiinf-grk*psi)/ &
                     (grkinf(i,j)*(bi(i,j)+3.)), 0.0)
        else
          psif (i,j)=0.0
        end if
      end if
    end do
  end do ! loop 200
  !
  do i=il1,il2
    if (igrn(i)>0) then
      psif (i,ig+1)=psif (i,ig)
      trmdr(i)=delt
    else
      trmdr(i)=0.
    end if
  end do ! loop 250
  !
  do j=1,igp2
    do i=il1,il2
      if (igrn(i)>0) then
        wmove(i,j)=0.0
        tmove(i,j)=0.0
      end if
    end do
  end do ! loop 300
  !
  !     * determine starting position of wetting front; initialization
  !     * for saturated infiltration.
  !
  do i=il1,il2
    if (igrn(i)>0) then
      ifill(i)=1
      zf(i)=0.0
      lzf(i)=1
      if (zpond(i)>0. .or. grkinf(i,1)<1.0e-12) then
        ninf(i)=2
        tmove(i,2)=tbarwx(i,1)
        ifill(i)=0
      end if
      do j=1,ig
        if (thliq(i,j)>=(thlinf(i,j)-1.0e-6) .and. &
            thliq(i,j)>0.0001 .and. lzf(i)==j) then
          zf(i)=zbotw(i,j)
          lzf(i)=j+1
          ninf(i)=j+2
          wmove(i,j+1)=thliq(i,j)*delzw(i,j)
          tmove(i,j+1)=tbarwx(i,j)
          tmove(i,j+2)=tbarwx(i,j+1)
          ifill(i)=0
        end if
      end do ! loop 350
    else
      ifill(i)=0
      lzf(i)=0
      ninf(i)=0
    end if
  end do ! loop 400
  !
  !     * if saturated infiltration conditions are not present at once
  !     * (ifill=1), call "WFILL" to do processing for period of
  !     * unsaturated infiltration.
  !
  call wfill(wmove,tmove,lzf,ninf,zf,trmdr,r,tr, &
                 psif,grkinf,thlinf,thliqx,tbarwx, &
                 delzx,zbotx,dzf,timpnd,wadj,wadd, &
                 ifill,ifind,ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  !     * call "WFLOW" to do processing for period of saturated
  !     * infiltration.
  !
  call wflow(wmove,tmove,lzf,ninf,trmdr,tpond,zpond, &
                 r,tr,evap,psif,grkinf,thlinf,thliqx,tbarwx, &
                 delzx,zbotx,fmax,zf,dzf,dtflow,thlnlz, &
                 thlqlz,dzdisp,wdisp,wabs,iter,nend,isimp, &
                 igrn,ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  !     * recalculate temperatures and liquid moisture contents of
  !     * soil layers following infiltration.
  !
  call wend(thliqx,thicex,tbarwx,zpond,tpond, &
                basflw,tbasfl,runoff,trunof,fi, &
                wmove,tmove,lzf,ninf,trmdr,thlinf,delzx, &
                zmat,zrmdr,fdtbnd,wadd,tadd,fdt,tfdt, &
                thlmax,thtest,thldum,thidum,tdumw, &
                tused,rdummy,zero,wexces,xdrain, &
                thpor,thlret,thlmin,bi,psisat,grksat, &
                thfc,delzw,isand,igrn,igrd,igdr,izero, &
                iveg,ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  do j=1,ig
    do i=il1,il2
      if (igrn(i)>0) then
        thliq(i,j)=thliqx(i,j)
        thice(i,j)=thicex(i,j)
        tbarw(i,j)=tbarwx(i,j)
      end if
    end do
  end do ! loop 800
  !
  do i=il1,il2
    if (igrn(i)>0 .and. lzf(i)<ig+1) then
      zfav(i)=(zf(i)+max(zbotw(i,lzf(i))-delzw(i,lzf(i)),0.0))/ &
                 2.0
      lzfav(i)=lzf(i)
      thlinv(i)=thlinf(i,lzf(i))
    else
      zfav(i)=0.0
      lzfav(i)=0
      thlinv(i)=0.0
    end if
  end do ! loop 850
  !
  !     * if time remains in the current model step after infiltration
  !     * has ceased (trmdr>0), call "GRDRAN" to calculate water flows
  !     * between layers for the remainder of the time step.
  !
  call grdran(iveg,thliq,thice,tbarw,fdummy,tdummy,basflw, &
                  tbasfl,runoff,trunof,qfg,wlost,fi,evap,zero,zero, &
                  trmdr,wexces,thlmax,thtest,thpor,thlret,thlmin, &
                  bi,psisat,grksat,thfc,delzw,xdrain,isand,izero, &
                  izero,igrd,igdr, &
                  ig,igp1,igp2,ilg,il1,il2,jl,n)
  !
  return
end subroutine grinfl
