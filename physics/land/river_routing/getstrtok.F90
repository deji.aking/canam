!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getstrtok(input_string, ntok, tok)

  !     * purpose: evaluates the number of valid tokens in a character string
  !     *          and returns their values.  the structure of a command string
  !     *          is:  token_id  tok1 ... tokn # comment.  tokens are preceded
  !     *          by a token identifier (hardcoded and upper case) and separated
  !     *          by spaces.  arbitrary comments are allowed following the #
  !     *          character.
  !
  !     * notes:   the maximum number of allowable tokens is 32
  !
  !     * modification:
  !
  !     * nov 07/2015 - m.lazare. coupler code modified for use in agcm:
  !     *                         - no cpp diretives.
  !     *
  !     * 10/15/98 by scott tinis
  !     *          file created.

  implicit none

  integer      :: i

  character*(*), intent(in)    :: input_string   !< INPUT STRING FOR PARSING\f$[units]\f$
  character*32, intent(inout)  :: tok(32)            !< ARRAY OF 32 CHAR TOKENS\f$[units]\f$
  integer, intent(inout)       :: ntok !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer, dimension(32) :: istart ! pointer to start of ith token
  integer, dimension(32) :: iend ! pointer to end of ith token
  integer      :: ip ! running pointer

  integer      :: lenstr ! function


  if (lenstr(input_string) == 0) then
    ! blank line; nothing to parse
    ntok = 0
  else

    ip = 1
    ntok = 0

    do i = 1, 32
      ! strip off the first 32 tokens

      istart(i) = 0
      iend(i) = 0

  10  if (ip == len( input_string)) then
        if (input_string(ip:ip) /= ' ') then
          ! reached end of last token by default
          if (input_string(ip:ip) == '#') then
            ! comment is last character; back up 1 char
            iend(i) = ip - 1
          else
            iend(i) = ip
          end if
          ntok = ntok + 1
        end if
        go to 100
      end if

      if (input_string(ip:ip) == '#') then
        ! reached the comment
        if (istart(i) /= 0 .and. iend(i) == 0) then
          ! check to see if last token closed off before exit
          iend(i) = ip - 1
          ntok = ntok + 1
        end if
        go to 100
      end if

      if (input_string(ip:ip) == ' ') then
        if (istart(i) > 0) then
          ! we have found the end of this token
          iend(i) = ip - 1
          ntok = ntok + 1
          ip = ip + 1
          go to 50
        else
          ! we are still looking for the beginning
          ip = ip + 1
          go to 10
        end if
      else
        if (istart(i) == 0) then
          ! we have found the beginning of this token
          istart(i) = ip
        end if
        ! else we are in the middle of the current token
        ip = ip + 1
        go to 10
      end if

  50 end do


  100 do i = 1, ntok
      ! set the token strings
      tok(i) = input_string(istart(i):iend(i))
    end do
  end if

end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
