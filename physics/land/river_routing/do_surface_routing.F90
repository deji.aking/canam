!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine do_surface_routing(going_in,coming_out,previous_depth, &
                              cur_slope,cur_distance,previous_going_in,cur_depth, &
                              vel,cur_width,dtcoup)

  !=======================================================================
  !     * aug 27/2016 - m.lazare. coupler code modified for use in agcm:
  !     *                         - no cpp diretives.
  !     *                         - pass in dtcoup and use instead of
  !     *                           hardcoded 86400.
  !     *                         - replace "NO_OF_STEPS_IN_A_DAY" by
  !     *                           "NSTEPS".
  !=======================================================================

  implicit none

  real, intent(in) :: going_in !< Variable description\f$[units]\f$
  real, intent(inout) :: coming_out !< Variable description\f$[units]\f$
  real, intent(in) :: previous_depth !< Variable description\f$[units]\f$
  real, intent(inout) :: cur_slope !< Variable description\f$[units]\f$
  real, intent(inout) :: cur_distance !< Variable description\f$[units]\f$
  real, intent(in) :: previous_going_in !< Variable description\f$[units]\f$
  real, intent(inout) :: cur_depth !< Variable description\f$[units]\f$
  real, intent(inout) :: vel !< Variable description\f$[units]\f$
  real, intent(in) :: cur_width !< Variable description\f$[units]\f$
  real, intent(in) :: dtcoup !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: width
  real :: manningsn
  real :: time_step
  real :: multiplier
  real, dimension(55) :: inflow
  real, dimension(55) :: depth
  real :: d1
  real :: d2
  real :: rhs
  real :: rhs2

  integer :: nsteps
  integer :: n


  width = cur_width
  if (width < 10.0)width = 10.0

  manningsn = 0.040
  cur_slope = cur_slope

  !     take into account the meandering of the river channel

  cur_distance = cur_distance * 1.40

  !     for stable solution of the forward diff. explicit eqn.
  !     deltat/(distance.width) must be less than about 0.0004 (i found this)
  !     and therefore we can use this to determine the time steps we
  !     will use over a period of a day

  time_step = 0.0004 * cur_distance * width
  nsteps = nint(dtcoup/time_step) + 1

  !     however, just to be on the safer side lets fix the nsteps
  !     to a minimum of 10

  if (nsteps < 10)nsteps = 10

  !     but do not let the nsteps increase beyond 50

  if (nsteps > 50)nsteps = 50

  time_step = dtcoup/real(nsteps)
  multiplier = time_step/(cur_distance * width)

  !     interpolate inflow for all time steps between going_in and
  !     previous_going_in. if we have 5 time steps in a coupling frequency, that
  !     means we need to interpolate 4 values in between previous_going_in and
  !     going_in


  inflow(1) = previous_going_in
  inflow(nsteps + 1) = going_in

  do n = 2,nsteps
    inflow(n) = ((going_in - previous_going_in) * ((n - 1.)/ &
                nsteps)) + previous_going_in
  end do


  !     now we go thru all the time steps and find the depth
  !     of flow at nsteps+1 time step, which is the
  !     depth we will use to find outflow

  depth(1) = previous_depth

  do n = 2,(nsteps + 1)
    d1 = depth(n - 1)
    if (d1 < 1.e-20) d1 = 0.
    rhs2 = ((width ** (5./3.)) * (d1 ** (5./3.)) * &
           (cur_slope ** (0.5)))/(manningsn * ((width + 2 * d1) ** (2./3.)))
    rhs = multiplier * (inflow(n - 1) - rhs2)
    depth(n) = depth(n - 1) + rhs
    if (depth(n) < 0.0) then
      write( * , * )' - VE FLOW DEPTH'
      call           xit('DO_SURFACE_ROUTING', - 1)
    end if
  end do

  !     now we use the final depth to find outflow

  d2 = depth(nsteps + 1)
  cur_depth = d2

  vel = (1/manningsn) * (cur_slope ** 0.5) * &
        (((width * d2)/(width + 2 * d2)) ** (2./3.))

  coming_out = vel * (width * d2)

  !     d2 should not be -ve, depth cannot be -ve, if that happens
  !     then exit

  if (d2 < 0.0) call                  xit('DO_SURFACE_ROUTING', - 2)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
