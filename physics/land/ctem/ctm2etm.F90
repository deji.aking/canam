subroutine ctm2etm(cyear,cday,csec, &
                         eyear,emon,eday,ehour,emin,esec)

  !     * august 19, 2002  - w.g. lee
  !
  !     * ctm2etm converts compressed time (year,day,sec) to
  !     * expanded time (year,month,day,hour,minute,second)
  !
  !     * if input values are non-integer, subroutine aborts.
  !
  !     * input data are re-arrange so that:
  !     *
  !     * day is in the range:  1,2,3...   365
  !     * sec is in the range:  1,2,3... 86400
  !     * note: this means that the inputs are modified for
  !     *       the routine that calls ctm2etm !

  !     ------------------------------------------------------------------

  implicit none

  integer  :: m !<

  real, intent(in)     :: cyear !<
  real, intent(in)     :: cday !<
  real, intent(in)     :: csec !<
  real, intent(inout)     :: eyear !<
  real, intent(inout)     :: emon !<
  real, intent(inout)     :: eday !<
  real, intent(inout)     :: ehour !<
  real, intent(inout)     :: emin !<
  real, intent(inout)     :: esec !<
  real     :: resid !<
  real     :: day(12) !<

  data     day  /31.,28.,31.,30.,31.,30.,31.,31.,30.,31.,30.,31./

  !     ------------------------------------------------------------------

  !     first clean up input data.

  call timeck (cyear, cday, csec, 0)

  !     from input seconds, determine output hours/minutes/seconds.

  ehour = int (csec/3600.0)
  resid = csec - ehour * 3600.0
  emin  = int (resid/60.0)
  esec  = resid - emin * 60.0

  !     from input days, determine output month, day

  emon = 1.0
  eday = cday

  do m = 1,11
    if (eday>day(m)) then
      eday = eday - day(m)
      emon = emon + 1.0
    else
      exit
    end if
  end do ! loop 20

  eyear = cyear

  return
end
