subroutine    bio2str(gleafmas, bleafmas, stemmass, rootmass, &
                            icc,      ilg,      il1,      il2, &
                            ig,       ic,  fcancmx,    zbotw, &
                            delzw, nol2pfts,    l2max, soildpth, &
  !    4--------------- inputs above this line, outputs below --------
                            ailcg,    ailcb,     ailc,    zolnc, &
                            rmatc, rmatctem,     slai,  bmasveg, &
                            cmasvegc,  veghght, rootdpth,   alvisc, &
                            alnirc,     paic,    slaic)
  !
  !     ----------------------------------------------------------------
  !
  !           canadian terrestrial ecosystem model (ctem) v1.1
  !        biomass to structural attributes conversion subroutine
  !
  !     sk: may  2018 - revise tolerances for 32-bit version.
  !     22  nov 2012  - calling this version 1.1 since a fair bit of ctem
  !     v. arora        subroutines were changed for compatibility with class
  !                     version 3.6 including the capability to run ctem in
  !                     mosaic/tile version along with class.
  !
  !     24  sep 2012  - add in checks to prevent calculation of non-present
  !     j. melton       pfts. cleaned up initialization
  !
  !     18  may  2012 - fix bug for soils with the depth close to the
  !     j. melton       boundary between layers, was resulting in roots
  !                     being placed incorrectly.
  !
  !     28  nov. 2011 - make changes for coupling with class 3.5
  !     y. peng
  !
  !     31  aug. 2009 - make changes for coupling with class 3.4. include
  !     v. arora        new output variable called plant area index
  !                     (paic) and storage leaf area index (slaic)
  !
  !     14  mar. 2003 - this subroutine converts leaf, stem, and root biomass
  !     v. arora        into lai, vegetation height, and fraction of roots
  !                     in each soil layer. storage lai is also calculated.
  !
  !                     note that while ctem keeps track of 9 pfts, class 2.7
  !                     keeps track of 4 pfts, so all these vegetation
  !                     structural attributes are calculated for 9 pfts
  !                     sepatarely but then lumped into 4 pfts for use in
  !                     energy & water balance calculations by class
  !
  !                     also, this subroutine does not estimate zolnc(i,5)
  !                     the log of roughness length over the bare fraction
  !                     of the grid cell. only roughness lengths over the
  !                     vegetated fraction are updated
  !
  !              ctem 1.0                 class 2.7
  !         -----------------            -------------
  !        1. needle leaf evg
  !        2. needle leaf dcd           1. needle leaf
  !        3. broad leaf evg
  !        4. broad leaf cold dcd       2. broad leaf
  !        5. broad leaf dry dcd
  !        6. c3 crop
  !        7. c4 crop                   3. crop
  !        8. c3 grass
  !        9. c4 grass                  4. grass
  !
  !     ----------------------------------------------------------------
  !     inputs
  !
  !     gleafmas  - green or live leaf mass in kg c/m2, for the 9 pfts
  !     bleafmas  - brown or dead leaf mass in kg c/m2, for the 9 pfts
  !     stemmass  - stem biomass in kg c/m2, for the 9 pfts
  !     rootmass  - root biomass in kg c/m2, for the 9 pfts
  !     FCANCMX   - MAX. FRACTIONAL COVERAGES OF CTEM's 9 PFTs. THIS IS
  !                 different from fcanc and fcancs (which may vary with
  !                 SNOW DEPTH). FCANCMX DOESN'T CHANGE, UNLESS OF COURSE
  !                 its changed by land use change or dynamic vegetation.
  !     delzw     - thicknesses of the 3 soil layers
  !     zbotw     - bottom of soil layers
  !     icc       - no of pfts for use by ctem, currently 9
  !     ic        - no of pfts for use by class, currently 4
  !     ig        - no. of soil layers, 3
  !     ilg       - no. of grid cells in latitude circle
  !     il1, il2  - il1=1, il2=ilg
  !     nol2pfts  - number of level 2 pfts
  !     l2max     - maximum number of level 2 pfts
  !     soildpth  - soil depth (m)
  !
  !     outputs
  !
  !     AILCG     - GREEN LAI FOR CTEM's 9 PFTs
  !     AILCB     - BROWN LAI FOR CTEM's 9 PFTs. FOR NOW WE ASSUME ONLY
  !                 grasses can have brown leaves.
  !     ailc      - lai to be used by class
  !     zolnc     - log of roughness length to be used by class
  !     rmatctem  - fraction of live roots in each soil layer for each of
  !                 CTEM's 9 PFTs
  !     rmatc     - fraction of live roots in each soil layer for each of the
  !                 CLASS' 4 PFTs
  !     slai      - storage or imaginary lai for phenology purposes
  !     bmasveg   - total (gleaf + stem + root) biomass for each ctem pft, kg c/m2
  !     cmasvegc  - total canopy mass for each of the 4 class pfts. recall that
  !                 class requires canopy mass as an input, and this is now
  !                 provided by ctem. kg/m2.
  !     veghght   - vegetation height (meters)
  !     rootdpth  - 99% soil rooting depth (meters)
  !                 both veghght & rootdpth can be used as diagnostics to see
  !                 how vegetation grows above and below ground, respectively.
  !     alvisc    - albedo for 4 class pfts simulated by ctem, visible
  !     alnirc    - albedo for 4 class pfts simulated by ctem, near ir
  !     PAIC      - PLANT AREA INDEX FOR CLASS' 4 PFTs. THIS IS THE SUM OF
  !                 leaf area index and stem area index.
  !     slaic     - storage lai. this will be used as min. lai that class
  !                 SEES SO THAT IT DOESN'T BLOW UP IN ITS STOMATAL
  !                 conductance calculations.
  !
  !     ----------------------------------------------------------------
  !
  implicit none

  integer, intent(in) :: ic !<
  integer, intent(in) :: icc !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ig !<
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer :: m !<
  integer :: n !<
  integer :: kk !<
  integer :: k1c !<
  integer :: k2c !<
  integer, intent(in) :: nol2pfts(ic) !<
  integer, intent(in) :: l2max !<
  integer :: icount !<
  integer :: sort(icc) !<
  integer :: kend !<

  logical :: deeproots !<

  parameter (kk=12) ! PRODUCT OF L2MAX AND CLASS' PFTs

  real, intent(in)  :: gleafmas(ilg,icc) !<
  real, intent(in)  :: bleafmas(ilg,icc) !<
  real, intent(in)  :: stemmass(ilg,icc) !<
  real, intent(in)  :: rootmass(ilg,icc) !<
  real, intent(inout)  :: ailcg(ilg,icc) !<
  real, intent(inout)  :: ailcb(ilg,icc) !<
  real, intent(inout)  :: ailc(ilg,ic) !<
  real, intent(inout)  :: zolnc(ilg,ic) !<
  real, intent(inout)  :: paic(ilg, ic) !<
  real, intent(inout)  :: rmatc(ilg,ic,ig) !<
  real, intent(in)  :: fcancmx(ilg,icc) !<
  real, intent(in)  :: delzw(ilg,ig) !<
  real, intent(in)  :: zbotw(ilg,ig) !<
  real, intent(inout)  :: rmatctem(ilg,icc,ig) !<
  real, intent(inout)  :: slai(ilg,icc) !<
  real, intent(inout)  :: bmasveg(ilg,icc) !<
  real, intent(inout)  :: cmasvegc(ilg,ic) !<
  real  :: sai(ilg,icc) !<
  real  :: saic(ilg,ic) !<
  real  :: sfcancmx(ilg,ic) !<
  real, intent(inout)  :: alvisc(ilg,ic) !<
  real, intent(inout)  :: alnirc(ilg,ic) !<
  real  :: pai(ilg,icc) !<
  real, intent(inout)  :: slaic(ilg,ic) !<

  real       :: lfespany(kk) !<
  real       :: sla(icc) !<
  real, intent(inout)       :: veghght(ilg,icc) !<
  real       :: lnrghlth(ilg,icc) !<
  real       :: averough(ilg,ic) !<
  real       :: abar(kk) !<
  real       :: avertmas(kk) !<
  real       :: alpha(kk) !<
  real       :: b(icc) !<
  real, intent(inout)       :: rootdpth(ilg,icc) !<
  real       :: usealpha(ilg,icc) !<
  real       :: a(ilg,icc) !<
  real       :: useb(ilg,icc) !<
  real       :: zroot !<
  real, intent(in)       :: soildpth(ilg) !<
  real       :: prcnslai(kk) !<
  real       :: eta(kk) !<
  real       :: kappa(kk) !<
  real       :: minslai(kk) !<
  real       :: specsla(kk) !<
  real       :: kn(kk) !<
  real       :: mxrtdpth(kk) !<
  real       :: albvis(kk) !<
  real       :: albnir(kk) !<
  real       :: etmp(ilg,icc,ig) !<
  real       :: totala(ilg,icc) !<

  real             :: fcoeff !<
  real             :: zero !<
  real             :: fracbofg !<
  real           :: rmat_sum !<
  real           :: tolrnce !<
  !
  !
  common /ctem1/ eta, kappa, kn
  common /ctem2/ lfespany, fracbofg, specsla
  !     ---------------------------------------------------------------
  !     parameters used. also note the structure of parameter vectors
  !     which clearly shows the class pfts (along rows) and ctem sub-pfts
  !     (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     parameter determining average root profile
  data abar/4.70, 5.86, 0.00, &
           3.87, 3.46, 3.97, &
           3.97, 3.97, 0.00, &
           5.86, 4.92, 0.00/
  !
  !     AVERAGE ROOT BIOMASS (KG C/M2) FOR CTEM's 8 PFTs USED FOR
  !     estimating rooting profile
  data avertmas/1.85, 1.45, 0.00, &
               2.45, 2.10, 2.10, &
               0.10, 0.10, 0.00, &
               0.70, 0.70, 0.00/
  !
  !     alpha, parameter determining how the roots grow
  data alpha/0.80, 0.80, 0.00, &
            0.80, 0.80, 0.80, &
            0.80, 0.80, 0.00, &
            0.80, 0.80, 0.00/
  !
  !     prcnslai - storage/imaginary lai is this percentage of maximum
  !     leaf area index that a given root+stem biomass can supoort
  data prcnslai/7.5, 7.5, 0.0, &
               7.5, 7.5, 7.5, &
               7.5, 7.5, 0.0, &
               2.5, 2.5, 0.0/
  !
  !     minslai - minimum storage lai. this is what the model uses as
  !     lai when growing vegetation for scratch. consider these as model
  !     seeds.
  data minslai/0.3, 0.3, 0.0, &
              0.3, 0.3, 0.3, &
              0.2, 0.2, 0.0, &
              0.2, 0.2, 0.0/
  !
  !      maximum rooting depth. this is used so that the rooting depths
  !      SIMULATED BY CTEM's VARIABLE ROOTING DEPTH PARAMETERZATION ARE
  !      constrained to realistic values
  !
  data mxrtdpth/3.00, 3.00, 0.00, &
                5.00, 5.00, 3.00, &
                2.00, 2.00, 0.00, &
                1.00, 1.00, 0.00/
  !
  !     visible and near ir albedos of the 9 ctem pfts
  !
  data albvis/3.00, 3.00, 0.00, &
              3.00, 5.00, 5.00, &
              5.50, 5.50, 0.00, &
              5.00, 6.00, 0.00/
  !
  data albnir/19.0, 19.0, 0.00, &
              23.0, 29.0, 29.0, &
              34.0, 34.0, 0.00, &
              30.0, 34.0, 0.00/
  !
  !     zero
  data zero/1.0e-12/,tolrnce/1.0e-06/
  !

  !     CLASS' ORIGINAL ROOT PARAMETERIZATION HAS DEEPER ROOTS THAN CTEM'S
  !     default values based on literature. in the coupled model this leads
  !     to lower evapotranspiration (et) values. an option is provided here to
  !     deepen roots, but this will also increase photosynthesis and vegetation
  !     biomass slightly, due to more access to soil water. so while use of
  !     deeper roots is desirable in the coupled global model, one may decide
  !     TO USE CTEM'S DEFAULT PARAMETERIZARION FOR STAND ALONE SIMULATIONS, AND
  !     set deeproots to .false.
  !
  data deeproots/.false./
  !     ---------------------------------------------------------------
  !
  if (icc/=9)                            call xit('BIO2STR',-1)
  if (ic/=4)                             call xit('BIO2STR',-2)
  !
  !     initialization
  !
  do j = 1,icc
    do k = 1,ig
      do i = il1,il2
        rmatctem(i,j,k)=0.0
        etmp(i,j,k)    =0.0
      end do ! loop 50
    end do ! loop 40
  end do ! loop 30
  !
  do j = 1,ic
    do k = 1,ig
      do i = il1,il2
        rmatc(i,j,k)=0.0
      end do ! loop 51
    end do ! loop 41
  end do ! loop 31
  !
  icount=0
  do j = 1, ic
    do m = 1, nol2pfts(j)
      n = (j-1)*l2max + m
      icount = icount + 1
      sort(icount)=n
    end do ! loop 53
  end do ! loop 52
  !
  do j = 1,ic
    do i =  il1, il2
      ailc(i,j)=0.0
      saic(i,j)=0.0
      paic(i,j)=0.0
      slaic(i,j)=0.0
      zolnc(i,j)=0.0
      averough(i,j)=0.0
      alvisc(i,j)=0.0
      alnirc(i,j)=0.0
      cmasvegc(i,j)=0.0
      sfcancmx(i,j)=0.0    ! sum of fcancmxs
    end do ! loop 70
  end do ! loop 60
  !
  do j = 1,icc
    sla(j)=0.0
    do i =  il1, il2
      usealpha(i,j)=alpha(sort(j))
      useb(i,j)=0.0
      ailcg(i,j)=0.0
      ailcb(i,j)=0.0
      veghght(i,j)=0.0
      lnrghlth(i,j)=0.0
      a(i,j)=0.0
      slai(i,j)=0.0
      sai(i,j)=0.0
      bmasveg(i,j)=0.0
      pai(i,j)=0.0
    end do ! loop 90
  end do ! loop 80
  !
  !
  !     ------ 1. conversion of leaf biomass into leaf area index -------
  !
  !     find specific leaf area (sla, m2/kg) using leaf life span
  !
  icount=0
  do j = 1, ic
    do m = 1, nol2pfts(j)
      n = (j-1)*l2max + m
      icount = icount + 1
      sla(icount) = 25.0*(lfespany(n)**(-0.50))
      if (specsla(n)>zero) sla(icount)=specsla(n)
    end do ! loop 101
  end do ! loop 100
  !
  !     convert leaf biomass into lai. brown leaves could have less
  !     lai than the green leaves for the same leaf mass. for now we
  !     assume sla of brown leaves is fracbofg times that of green
  !     leaves.
  !
  !     also find stem area index as a function of stem biomass
  !
  do j = 1,icc
    do i = il1,il2
      if (fcancmx(i,j)>0.0) then
        ailcg(i,j)=sla(j)*gleafmas(i,j)
        ailcb(i,j)=sla(j)*bleafmas(i,j)*fracbofg
        sai(i,j)=0.55*(1.0-exp(-0.175*stemmass(i,j))) ! stem area index
        !         plant area index is sum of green and brown leaf area indices
        !         and stem area index
        pai(i,j)=ailcg(i,j)+ailcb(i,j)+sai(i,j)

        !         make class see some minimum pai, otherwise it runs into numerical
        !         problems
        pai(i,j)=max(0.3,pai(i,j))
      end if
    end do ! loop 160
  end do ! loop 150
  !
  !
  !     -------------------  2. calculate storage lai  --------------------
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        slai(i,j)=((stemmass(i,j)+rootmass(i,j))/eta(sort(j))) &
      **(1./kappa(sort(j)))
        slai(i,j)=(prcnslai(sort(j))/100.0)*sla(j)*slai(i,j)
        !
        !         need a minimum slai to be able to grow from scratch. consider
        !         this as model seeds.
        slai(i,j)=max(slai(i,j),minslai(sort(j)))
      end if
    end do ! loop 171
  end do ! loop 170
  !
  !     get fcancmx weighted leaf area index for use by class
  !       needle leaf evg + dcd = total needle leaf
  !       broad leaf evg + dcd cld + dcd dry = total broad leaf
  !       crop c3 + c4 = total crop
  !       grass c3 + c4 = total grass
  !     also add brown lai. note that although green + brown
  !     lai is to be used by class for energy and water balance
  !     calculations, stomatal conductance estimated by the
  !     photosynthesis subroutine is only based on green lai.
  !     that is although both green+brown leaves intercept
  !     water and light, only the green portion photosynthesizes.
  !     ALSO LUMP STEM AND PLANT AREA INDICES FOR CLASS' 4 PFTs
  !
  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        sfcancmx(i,j)=sfcancmx(i,j)+fcancmx(i,m)
        ailc(i,j)=ailc(i,j)+(fcancmx(i,m)*(ailcg(i,m)+ailcb(i,m)))
        saic(i,j)=saic(i,j)+(fcancmx(i,m)*sai(i,m))
        paic(i,j)=paic(i,j)+(fcancmx(i,m)*pai(i,m))
        slaic(i,j)=slaic(i,j)+(fcancmx(i,m)*slai(i,m))
      end do ! loop 220
    end do ! loop 210
  end do ! loop 200
  !
  do j = 1, ic
    do i = il1, il2
      !
      if (sfcancmx(i,j)>zero) then
        ailc(i,j)=ailc(i,j)/sfcancmx(i,j)
        saic(i,j)=saic(i,j)/sfcancmx(i,j)
        paic(i,j)=paic(i,j)/sfcancmx(i,j)
        slaic(i,j)=slaic(i,j)/sfcancmx(i,j)
      else
        ailc(i,j)=0.0
        saic(i,j)=0.0
        paic(i,j)=0.0
        slaic(i,j)=0.0
      end if
      !         for crops and grasses set the minimum lai to a small number, other
      !         wise class will never run tsolvc and thus phtsyn and ctem will not
      !         be able to grow crops or grasses.
      if (j==3.or.j==4) ailc(i,j)=max(ailc(i,j),0.1)
      !
    end do ! loop 240
  end do ! loop 230
  !
  !     ------ 3. conversion of stem biomass into roughness length -------
  !
  !     class uses log of roughness length (zoln) as an input parameter. when
  !     vegetation grows and dies as per ctem, then zoln is provided by ctem.
  !
  !     1. convert stem biomass into vegetation height for trees and crops,
  !        and convert leaf biomass into vegetation height for grass
  !     2. convert vegetation height into roughness length & take its log
  !     3. LUMP THIS FOR CTEM's 9 PFTs INTO CLASS' 4 PFTs
  !
  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        !
        if (j<=2) then                            ! trees
          veghght(i,m)=10.0*stemmass(i,m)**0.385
          veghght(i,m)=min(veghght(i,m),45.0)
        else if (j==3) then                       ! crops
          veghght(i,m)=1.0*(stemmass(i,m)+gleafmas(i,m))**0.385
        else if (j==4) then                       ! grasses
          veghght(i,m)=3.5*(gleafmas(i,m)+fracbofg*bleafmas(i,m))**0.50
        end if
        lnrghlth(i,m)= log(0.10 * max(veghght(i,m),0.10))
        !
      end do ! loop 270
    end do ! loop 260
  end do ! loop 250
  !

  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        averough(i,j)=averough(i,j)+(fcancmx(i,m)*lnrghlth(i,m))
      end do ! loop 320
    end do ! loop 310
  end do ! loop 300
  !
  do j = 1, ic
    do i = il1, il2
      !
      if (sfcancmx(i,j)>zero) then
        averough(i,j)=averough(i,j)/sfcancmx(i,j)
      else
        averough(i,j)=-4.605
      end if
      zolnc(i,j)=averough(i,j)
      !
    end do ! loop 340
  end do ! loop 330
  !
  !
  !     ------ 4. estimating fraction of roots in each soil layer for -----
  !     ------      CTEM's EACH VEGETATION TYPE, USING ROOT BIOMASS   -----
  !
  !
  !     estimate parameter b of variable root profile parameterization
  !
  icount=0
  do j = 1, ic
    do m = 1, nol2pfts(j)
      n = (j-1)*l2max + m
      icount = icount + 1
      !         use decreased abar for all pfts to deepen roots to account
      !         for hydraulic redistrubution
      if (deeproots) then
        b(icount) = (abar(n)-1.5) * (avertmas(n)**alpha(n))
      else
        b(icount) = abar(n) * (avertmas(n)**alpha(n))
      end if
    end do ! loop 360
  end do ! loop 350
  !
  !     use b to estimate 99% rooting depth
  !
  k1c=0
  do j = 1,ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        !
        useb(i,m)=b(m)
        usealpha(i,m)=alpha(sort(m))
        rootdpth(i,m) = (4.605*(rootmass(i,m)**alpha(sort(m))))/b(m)
        !
        !           if estimated rooting depth is greater than soil depth, or
        !           the maximum rooting depth then adjust rooting depth and
        !           parameter alpha
        !
        !           also find "A" (parameter determining root profile). this is
        !           the "A" which depends on time varying root biomass
        !
        !
        if (rootdpth(i,m)>min(soildpth(i),zbotw(i,ig), &
            mxrtdpth(sort(m)))) then
          rootdpth(i,m) = min(soildpth(i),zbotw(i,ig), &
                              mxrtdpth(sort(m)))
          if (rootdpth(i,m)<=zero) then
            a(i,m)=100.0
          else
            a(i,m)=4.605/rootdpth(i,m)
          end if
        else
          if (rootmass(i,m)<=zero) then
            a(i,m)=100.0
          else
            a(i,m)=useb(i,m)/(rootmass(i,m)**usealpha(i,m))
          end if
        end if
        !
      end do ! loop 390
    end do ! loop 380
  end do ! loop 370
  !
  do j = 1,icc
    do i = il1, il2

      kend=9999  ! initialize with a dummy value
      !
      !          using parameter "A" we can find fraction of roots in each soil layer
      !          just like class
      !
      zroot=max(0.0,rootdpth(i,j))

      totala(i,j) = 1.0-exp(-a(i,j)*zroot)

      if (zroot<=zbotw(i,1)) then
        !           if rootdepth is shallower than the bottom of the first layer
        rmatctem(i,j,1)=1.0
        do k=2,ig
          rmatctem(i,j,k)=0.0
        end do ! loop 414
        kend=1
      else
        !
        do k=2,ig
          if (zroot<=zbotw(i,k).and.zroot>zbotw(i,k-1)) then
            !             if rootdepth is shallower than the bottom of current layer and
            !             is deeper than bottom of the previous top layer
            kend=k ! kend = soil layer number in which the roots end
          end if
        end do ! loop 415
        !
        if (kend == 9999) then
          write(6,2100) i,j,k,kend
2100      format(' AT (I) = (',i3,'), PFT=',i2,', DEPTH=',i2,' KEND &
  is not assigned. kend  = ',I5)
          call xit('BIO2STR',-3)
        end if

        etmp(i,j,1)=exp(-a(i,j)*zbotw(i,1))
        rmatctem(i,j,1)=(1.0-etmp(i,j,1))/totala(i,j)
        if (kend == 2) then
          !             if rootdepth is shallower than the bottom of 2nd layer
          etmp(i,j,kend)=exp(-a(i,j)*zroot)
          rmatctem(i,j,kend)=(etmp(i,j,kend-1)-etmp(i,j,kend)) &
                           /totala(i,j)
        else if (kend > 2) then
          !             if rootdepth is shallower than the bottom of 3rd layer
          !             or even the deeper layer (ig>3)

          do k=2,kend-1
            etmp(i,j,k)=exp(-a(i,j)*zbotw(i,k))
            rmatctem(i,j,k)=(etmp(i,j,k-1)-etmp(i,j,k))/totala(i,j)
          end do ! loop 416

          etmp(i,j,kend)=exp(-a(i,j)*zroot)
          rmatctem(i,j,kend)=(etmp(i,j,kend-1)-etmp(i,j,kend)) &
                           /totala(i,j)
        end if
      end if
      !
    end do ! loop 410
  end do ! loop 400
  !
  !    make sure all fractions (of roots in each layer) add to one.
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        rmat_sum = 0.0
        do k = 1, ig
          rmat_sum = rmat_sum + rmatctem(i,j,k)
        end do ! loop 413
        !
        if (abs(rmat_sum-1.0)>tolrnce) then
          write(6,2300) i,j,rmat_sum
2300      format(' AT (I) = (',i3,'), PFT=',i2,' FRACTIONS OF ROOTS &
 not adding to one. sum  = ',F12.7)
          call xit('BIO2STR',-4)
        end if
      end if
    end do ! loop 412
  end do ! loop 411
  !
  !     lump rmatctem(i,9,ig)  into rmatc(i,4,ig) for use by class
  !
  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        !
        do k = 1, ig
          rmatc(i,j,k)=rmatc(i,j,k)+(fcancmx(i,m)*rmatctem(i,m,k))
        end do ! loop 441
        !
      end do ! loop 440
    end do ! loop 430
  end do ! loop 420
  !
  do j = 1, ic
    do i = il1, il2
      !
      if (sfcancmx(i,j)>zero) then
        do k = 1, ig
          rmatc(i,j,k)=rmatc(i,j,k)/sfcancmx(i,j)
        end do ! loop 461
      else
        rmatc(i,j,1)=1.0
        do k = 2, ig
          rmatc(i,j,k)=0.0
        end do ! loop 462
      end if
      !
    end do ! loop 460
  end do ! loop 450
  !
  !
  !     --- 5. calculate total vegetation biomass for each ctem pft, and --
  !     ---------------- canopy mass for each class pft ------------------
  !
  do j = 1, icc
    do i = il1, il2
      if (fcancmx(i,j)>0.0) then
        bmasveg(i,j)=gleafmas(i,j)+stemmass(i,j)+rootmass(i,j)
      end if
    end do ! loop 560
  end do ! loop 550
  !
  !     since class uses canopy mass and not total vegetation biomass as an
  !     input, we find canopy mass as a sum of stem and leaf mass, for each
  !     class pft, i.e. only above ground biomass.
  !
  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        cmasvegc(i,j)= cmasvegc(i,j) + &
                           (fcancmx(i,m)*(bleafmas(i,m)+gleafmas(i,m)+stemmass(i,m)))
      end do ! loop 620
    end do ! loop 610
  end do ! loop 600
  !
  do j = 1, ic
    do i = il1, il2
      !
      if (sfcancmx(i,j)>zero) then
        cmasvegc(i,j)=cmasvegc(i,j)/sfcancmx(i,j)
        cmasvegc(i,j)=cmasvegc(i,j)*(1.0/0.50) ! assuming biomass is 50% c
      else
        cmasvegc(i,j)=0.0
      end if
      !
      !         if there is no vegetation canopy mass will be zero. this should
      !         essentially mean more bare ground, but since we are not changing
      !         fractional coverages at present, we pass a minimum canopy mass
      !         TO CLASS SO THAT IT DOESN'T RUN INTO NUMERICAL PROBLEMS.
      !
      cmasvegc(i,j)=max(cmasvegc(i,j),3.0)
      !
    end do ! loop 640
  end do ! loop 630
  !
  !     --- 6. CALCULATE ALBEDO FOR CLASS' 4 PFTs BASED ON SPECIFIED ----
  !     ------ albedos of ctem 9 pfts and their fractional coveraes -----
  !
  k1c=0
  do j = 1, ic
    if (j==1) then
      k1c = k1c + 1
    else
      k1c = k1c + nol2pfts(j-1)
    end if
    k2c = k1c + nol2pfts(j) - 1
    do m = k1c, k2c
      do i = il1, il2
        alvisc(i,j)= alvisc(i,j) + (fcancmx(i,m)*albvis(sort(m)))
        alnirc(i,j)= alnirc(i,j) + (fcancmx(i,m)*albnir(sort(m)))
      end do ! loop 720
    end do ! loop 710
  end do ! loop 700
  !
  do j = 1, ic
    do i = il1, il2

      if (sfcancmx(i,j)>zero) then
        alvisc(i,j)=(alvisc(i,j)/sfcancmx(i,j))/100.0
        alnirc(i,j)=(alnirc(i,j)/sfcancmx(i,j))/100.0
      else
        alvisc(i,j)=0.0
        alnirc(i,j)=0.0
      end if

    end do ! loop 740
  end do ! loop 730

  return
end

