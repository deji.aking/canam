subroutine ctm2sec(cyear,cday,csec,sec)

  !     * sept 27, 2002  - w.g. lee
  !
  !     * ctm2etm converts compact time (year,day,sec) to
  !     * total number of seconds.
  !
  !     * if input values are non-integer, subroutine aborts.
  !
  !     ------------------------------------------------------------------

  implicit none

  integer  :: m !<

  real, intent(in)     :: cyear !<
  real, intent(in)     :: cday !<
  real, intent(in)     :: csec !<
  real*8, intent(inout)     :: sec !<

  call timeck(cyear,cday,csec,1)

  sec = real(csec,kind=8) + &
            (real(cday,kind=8)-1.+(real(cyear,kind=8)-1.)*365.)*86400.

  return
end
