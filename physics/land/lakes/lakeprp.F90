!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine lakeprp (sno,sic,dens, &
                    gc,snow,hlat, &
                    delt,ilg,iis,iif)
  !.......................................................................
  !     * sep 06/16 - m.lazare.    new version for gcm19+:
  !     *                          - based on oifpst10 but much simplified
  !     *                            for lake stuff not done in "FLAKE".
  !     *                            snowfall contribution moved from
  !     *                            oifprp10.
  !----------------------------------------------------------------------
  !     * dictionary of variables
  !
  !  delt  - model time step (s)
  !  dens  - snow density (kg m-3)
  !  evapr - ground evaporation rate (kg m-2 s-1)
  !  gc    - ground cover type (1.=pack ice, 0.=open water, -1.=land)
  !  hlat  - latent heat flux (w m-2)
  !  hs    - latent heat of sublimation (j kg-1)
  !  sic   - sea ice amount (kg m-2)
  !  sno   - snow amount (kg m-2)
  !  snow  - snowfall rate (kg/m2-s)
  !
  use phys_consts, only : snomax, hs
  implicit none
  real, intent(in) :: delt  !< Variable description\f$[units]\f$
  integer, intent(in) :: iif
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: iis
  !
  !     * output fields:
  !
  real, intent(inout)  , dimension(ilg)  :: sno !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg)  :: sic !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg)  :: dens !< Variable description\f$[units]\f$
  !
  !     * input fields:
  !
  real, intent(in)  , dimension(ilg)  :: gc !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg)  :: snow !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg)  :: hlat !< Variable description\f$[units]\f$
  !
  real :: dele
  real :: evapr
  integer :: i
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !-------------------------------------------------------------------
  do i = iis,iif
    if (gc(i) > 0.5) then
      !
      !         * snow accumulation over ice.
      !
      if (snow(i) > 0.) then
        sno(i) = sno(i) + snow(i) * delt
      end if
      !
      !         * sublimation/deposition.
      !
      evapr = hlat(i)/hs
      dele = evapr * delt
      if (sno(i) > dele) then
        sno(i) = sno(i) - dele
      else
        sic(i) = sic(i) - (dele - sno(i))
        sno(i) = 0.
      end if
      sic(i) = max(sic(i),0.)
      !
      !         * snow density.
      !
      if (sno(i) > snomax) then
        dens(i) = 0.54 * sno(i)/log(1. + 0.54 * sno(i)/275.)
      else
        dens(i) = 275.
      end if
    end if
  end do ! loop 100
  !-----------------------------------------------------------------------
  return
end subroutine lakeprp
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

