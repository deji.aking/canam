# CanAM radiative transfer single column model

To use the CanAM radiative transfer code in a "stand-alone" configuration a driver has
been create that reads columns of input and writes out radiative transfer results.

The radiative transfer calculations are performed using calls to the radiative 
transfer and related routines in the CanAM source.  I.e., the radiative transfer
is performed using the exact same code as in CanESM5.

## Content and function of directories

 - build : Makefiles and script to compile the executable
 - cfg : Hold files defining the run information (extension run_info) and the configuration of the radiative transfer calculations (extension cfg)
 - input : Directory to hold input files
 - output : Directory to hold output files
 - run : Directory in which the code can be run
 - src : Source code for the driver
 - tools : Additonal code and scripts used to perform specialized functions.  E.g., manipulating output into a particular format

## How to use

1. Edit the configuration and run information files as needed.
 - The "cfg" files can usually left as is (experience is needed to set some parameters properly)
 - The "run_info" file sets the location of the configuration, input and output files.

2. Compile the code using "make" under the directory "build".
 - To compile it may be necessary to edit "Makefile" and edit (or create) a "mkmf" file for your system.
 - Compile the code using the command "make".
 - If the compilation is successful, the resulting executable will be called "scm_rt".

3. Run the radiative transfer calculations
 - This can be done from the "run" using the command "../build/scm_rt ../cfg/runinfo_filename"
