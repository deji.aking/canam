! Macros used to define a number of array operations

#undef MKARR1DN
#undef MKARR2DN
#undef MKARR3DN
#undef MKARR4DN
#undef MKARR5DN
#undef MKSARR2DN
#undef MKSARR3DN
#undef MKINTARR
#undef MKINTARR3D

#undef IF_BLOCK
#undef IF_BLOCK_END

#if defined(PHYARRDCL)

#  define IF_BLOCK(OPTION)
#  define IF_BLOCK_END(OPTION)

#  if defined(_DYNAMIC_ALLOCATION) || defined(_POINTER_ASSIGNMENT)
#    define MKARR1DN(PNAME,PTAR,NI) real, dimension(:), ALLOCABLE_ :: PNAME
#    define MKARR2DN(PNAME,PTAR,NI,NK) real, dimension(:,:), ALLOCABLE_ :: PNAME
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) real, dimension(:,:,:), ALLOCABLE_ :: PNAME
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) real, dimension(:,:,:,:), ALLOCABLE_ :: PNAME
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) real, dimension(:,:,:,:,:), ALLOCABLE_ :: PNAME
#    define MKINTARR(PNAME, PTAR,NI,NK) integer, dimension(:,:), ALLOCABLE_ :: PNAME
#    define MKINTARR3D(PNAME, PTAR,NI,NK,NN) integer, dimension(:,:,:), ALLOCABLE_ :: PNAME

#    define MKSARR2DN(PNAME,PTAR,NI,NK) real, dimension(:,:), ALLOCABLE_ :: PNAME
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) real, dimension(:,:,:), ALLOCABLE_ :: PNAME

#  else
#    define MKARR1DN(PNAME,PTAR,NI) real, dimension(NI) :: PNAME
#    define MKARR2DN(PNAME,PTAR,NI,NK) real, dimension(NI,NK) :: PNAME
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) real, dimension(NI,NK,NN) :: PNAME
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) real, dimension(NI,NK,NN,NM) :: PNAME
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) real, dimension(NI,NK,NN,NM,NP) :: PNAME
#    define MKINTARR(PNAME, PTAR,NI,NK) integer, dimension(NI,NK) :: PNAME
#    define MKINTARR3D(PNAME, PTAR,NI,NK,NN) integer, dimension(NI,NK,NN) :: PNAME

#    define MKSARR2DN(PNAME,PTAR,NI,NK) real, dimension(NI,NK) :: PNAME
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) real, dimension(NI,NK,NN) :: PNAME

#  endif

#else

#  define IF_BLOCK(OPTION) if ( OPTION ) then
#  define IF_BLOCK_END(OPTION) endif

#  if defined(POINT_)

#    define MKARR1DN(PNAME,PTAR,NI) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2)
#    define MKARR2DN(PNAME,PTAR,NI,NK) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:,:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:,:,:)
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:,:,:,:)
#    define MKINTARR(PNAME,PTAR,NI,NK) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:)
#    define MKINTARR3D(PNAME,PTAR,NI,NK,NN) nullify(self%PNAME); self%PNAME => PTAR(ioff1:ioff2,:,:)

#    define MKSARR2DN(PNAME,PTAR,NI,NK) allocate(self%PNAME(NI,NK)) ; self%PNAME(il1:il2,1:NK) = SPREAD(PTAR(ioffz+il1z,:),1,nil)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) allocate(self%PNAME(NI,NK,NN)) ; self%PNAME(il1:il2,1:NK,1:NN) = SPREAD(PTAR(ioffz+il1z,:,:),1,nil)

#  elif defined(ASSIGN_)
#    define MKARR1DN(PNAME,PTAR,NI) self%PNAME(il1:il2) = PTAR(ioff+il1:ioff+il2)
#    define MKARR2DN(PNAME,PTAR,NI,NK) self%PNAME(il1:il2,1:NK) = PTAR(ioff+il1:ioff+il2,:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) self%PNAME(il1:il2,1:NK,1:NN) = PTAR(ioff+il1:ioff+il2,:,:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) self%PNAME(il1:il2,1:NK,1:NN,1:NM) = PTAR(ioff+il1:ioff+il2,:,:,:)
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) self%PNAME(il1:il2,1:NK,1:NN,1:NM,1:NP) = PTAR(ioff+il1:ioff+il2,:,:,:,:)
#    define MKSARR2DN(PNAME,PTAR,NI,NK) self%PNAME(il1:il2,1:NK) = SPREAD(PTAR(ioffz+il1z,:),1,nil)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) self%PNAME(il1:il2,1:NK,1:NN) = SPREAD(PTAR(ioffz+il1z,:,:),1,nil)
#    define MKINTARR(PNAME,PTAR,NI,NK) self%PNAME(il1:il2,1:NK) = PTAR(ioff+il1:ioff+il2,:)
#    define MKINTARR3D(PNAME,PTAR,NI,NK,NN) self%PNAME(il1:il2,1:NK,1:NN) = PTAR(ioff+il1:ioff+il2,:,:)

#  elif defined(PACK_)
#    define MKARR1DN(PNAME,PTAR,NI) PTAR(ioff+il1:ioff+il2) = self%PNAME(il1:il2)
#    define MKARR2DN(PNAME,PTAR,NI,NK) PTAR(ioff+il1:ioff+il2,:) = self%PNAME(il1:il2,1:NK)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) PTAR(ioff+il1:ioff+il2,:,:) = self%PNAME(il1:il2,1:NK,1:NN)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) PTAR(ioff+il1:ioff+il2,:,:,:) = self%PNAME(il1:il2,1:NK,1:NN,1:NM)
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) PTAR(ioff+il1:ioff+il2,:,:,:,:) = self%PNAME(il1:il2,1:NK,1:NN,1:NM,1:NP)
#    define MKSARR2DN(PNAME,PTAR,NI,NK) PTAR(ioffz+il1z,:) = self%PNAME(il1,1:NK)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) PTAR(ioffz+il1z,:,:) = self%PNAME(il1,1:NK,1:NN)
#    define MKINTARR(PNAME,PTAR,NI,NK) PTAR(ioff+il1:ioff+il2,:) = self%PNAME(il1:il2,1:NK)
#    define MKINTARR3D(PNAME,PTAR,NI,NK,NN) PTAR(ioff+il1:ioff+il2,:,:) = self%PNAME(il1:il2,1:NK,1:NN)

#  elif defined(ALLOC_)
#    define MKARR1DN(PNAME,PTAR,NI) allocate(self%PNAME(NI))
#    define MKARR2DN(PNAME,PTAR,NI,NK) allocate(self%PNAME(NI,NK))
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) allocate(self%PNAME(NI,NK,NN))
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) allocate(self%PNAME(NI,NK,NN,NM))
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) allocate(self%PNAME(NI,NK,NN,NM,NP))
#    define MKSARR2DN(PNAME,PTAR,NI,NK) allocate(self%PNAME(NI,NK))
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) allocate(self%PNAME(NI,NK,NN))
#    define MKINTARR(PNAME,PTAR,NI,NK) allocate(self%PNAME(NI,NK))
#    define MKINTARR3D(PNAME,PTAR,NI,NK,NN) allocate(self%PNAME(NI,NK,NN))

#  elif defined(DEALLOC_)
#    define MKARR1DN(PNAME,PTAR,NI) deallocate(self%PNAME)
#    define MKARR2DN(PNAME,PTAR,NI,NK) deallocate(self%PNAME)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN) deallocate(self%PNAME)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM) deallocate(self%PNAME)
#    define MKARR5DN(PNAME,PTAR,NI,NK,NN,NM,NP) deallocate(self%PNAME)
#    define MKSARR2DN(PNAME,PTAR,NI,NK) deallocate(self%PNAME)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN) deallocate(self%PNAME)
#    define MKINTARR(PNAME,PTAR,NI,NK) deallocate(self%PNAME)
#    define MKINTARR3D(PNAME,PTAR,NI,NK,NN) deallocate(self%PNAME)
#  endif

#endif
