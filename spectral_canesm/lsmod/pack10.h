#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * oct 27/2020 - c. mcl.     - add nonoro gwd fields (ftne, ftnw, ftny,
!				    tdnx, tdny, rhog)
!     * apr 20/2019 - s.kharin    - fix bug ioffz->ioff in *_ext_gcm_sa_* fields
!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtvipas/xtviros for sampled burdens.
!     * aug 14/2018 - m.lazare.     remove maskpak,gtpal.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}.
!     * nov 01/2017 - j. cole.     - update for cmip6 stratospheric aerosols
!     * aug 09/2017 - m.lazare.    - add fnpat/fnrot.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     * aug 07/2017 - m.lazare.    - initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstrol,cltrol} removed.
!     *                            - add (for nemo/cice support):
!     *                              hsearol,rainsrol,snowsrol
!     *                              obegrol,obwgrol,begorol,bwgorol,
!     *                              begirol,hflirol,
!     *                              hsearol,rainsrol,snowsrol.
!     *                            - remove: ftilpak/ftilrow.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class):
!     *                              thlwpat/thlwrot.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvpat/algdvrot, algdnpat/algdnrot,
!     *                              algwvpat/algwvrot, algwnpat/algwnrot,
!     *                              socipat/socirot.
!     *                            - add (for fractional land/water/ice):
!     *                              salbpat/salbrot, csalpat/csalrot,
!     *                              emispak/emisrow, emispat/emisrot,
!     *                              wrkapal/wrkarol, wrkbpal/wrkbrol,
!     *                              snopako/snorowo.
!     *                            - add (for pla):
!     *                              psvvpak/psvvrow, pdevpak/pdevrow,
!     *                              pdivpak/pdivrow, prevpak/prevrow,
!     *                              privpak/privrow.
!     *                            - bugfixes for isccp fields and additions
!     *                              for calipso and modis.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - flakpak,flndpak,gtpat,licnpak added.
!     *                            - duplicate calls for {edso,edso,esvc,esve}
!     *                              removed.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "pat"/"rot" arrays.
!     * jul 10/2013 - m.lazare/    previous version pack9 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "im" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well: {fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     *                            - "xtraconv" cpp option moved to before
!     *                              "xtrachem", to be consistent with others.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 18/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr but only for
!     *                              kount.gt.kstart.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - add fare, thlq, thic. change selected
!     *                            - pak variables to packed tile versions
!     *                              (pat). add selected pat variables in
!     *                              addition to their pak counterparts.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 03/2012 - m.lazare/    previous version pack8 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "xtradust" and
!     *                              "aodpth".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "flg", "fdlc" and "flam".
!     *                            - "flgrol_r" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "fdlrol_r-sigma*t**4".
!     *                            - include "iseed" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version pack7i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("fsol").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "coupler_ctem".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "histemi"
!     *                              converted to cpp:
!     *                              "transient_aerosol_emissions" with
!     *                              further choice of cpp directives
!     *                              "histemi" for historical or
!     *                              "emists" for future emissions.
!     *                              the "histemi" fields are the
!     *                              same as previously, except that
!     *                              ??? has been removed. for "emists",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("b" for black
!     *                              carbon, "o" for organic carbon and
!     *                              "s" for sulfur), while for each of
!     *                              these, there are "air" for aircraft,
!     *                              "sfc" for surface, "stk" for stack
!     *                              and "fir" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - don't pack class input fields
!     *                              (fcan,alic,alvc,cmas,lnzo,root)
!     *                              since are invariant.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 19/2009 - m.lazare.    previous version pack7h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%df explvol". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version pack7g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%df radforce").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "xtrachem"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - code related to s/l removed.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2006 - j.cole/      previous version pack7f for gcm15f:
!     *               m.lazare.    - add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    new version for gcm15f:
!     *                            - two extra new fields ("dmc" and "smc")
!     *                              under control of "%if def,xtraconv".
!     * may 07/2006 - m.lazare.    previous version for pack7e for gcm15e.
!========================================================================
!     * general physics arrays.
!
do l=1,ilev
  do i=il1,il2
    almcpak(ioff+i,l)  = phys_arrays%almcrow(i,l)
    almxpak(ioff+i,l)  = phys_arrays%almxrow(i,l)
    cicpak (ioff+i,l)  = phys_arrays%cicrow (i,l)
    clcvpak(ioff+i,l)  = phys_arrays%clcvrow(i,l)
    cldpak (ioff+i,l)  = phys_arrays%cldrow (i,l)
    clwpak (ioff+i,l)  = phys_arrays%clwrow (i,l)
    cvarpak(ioff+i,l)  = phys_arrays%cvarrow(i,l)
    cvmcpak(ioff+i,l)  = phys_arrays%cvmcrow(i,l)
    cvsgpak(ioff+i,l)  = phys_arrays%cvsgrow(i,l)
    ftoxpak(ioff+i,l)  = phys_arrays%ftoxrow(i,l)
    ftoypak(ioff+i,l)  = phys_arrays%ftoyrow(i,l)
    ftnepak(ioff+i,l)  = phys_arrays%ftnerow(i,l)
    ftnwpak(ioff+i,l)  = phys_arrays%ftnwrow(i,l)
    ftnypak(ioff+i,l)  = phys_arrays%ftnyrow(i,l)
    tdnxpak(ioff+i,l)  = phys_arrays%tdnxrow(i,l)
    tdnypak(ioff+i,l)  = phys_arrays%tdnyrow(i,l)
    rhogpak(ioff+i,l)  = phys_arrays%rhogrow(i,l)
    hrlpak (ioff+i,l)  = phys_arrays%hrlrow (i,l)
    hrspak (ioff+i,l)  = phys_arrays%hrsrow (i,l)
    hrlcpak (ioff+i,l) = phys_arrays%hrlcrow (i,l)
    hrscpak (ioff+i,l) = phys_arrays%hrscrow (i,l)
    ometpak(ioff+i,l)  = phys_arrays%ometrow(i,l)
    qwf0pal(ioff+i,l)  = phys_arrays%qwf0rol(i,l)
    qwfmpal(ioff+i,l)  = phys_arrays%qwfmrol(i,l)
    rhpak  (ioff+i,l)  = phys_arrays%rhrow  (i,l)
    scdnpak(ioff+i,l)  = phys_arrays%scdnrow(i,l)
    sclfpak(ioff+i,l)  = phys_arrays%sclfrow(i,l)
    slwcpak(ioff+i,l)  = phys_arrays%slwcrow(i,l)
    tacnpak(ioff+i,l)  = phys_arrays%tacnrow(i,l)
    tdoxpak(ioff+i,l)  = phys_arrays%tdoxrow(i,l)
    tdoypak(ioff+i,l)  = phys_arrays%tdoyrow(i,l)
    zdetpak(ioff+i,l)  = phys_arrays%zdetrow(i,l)
  enddo
enddo

do n=1,ntrac
  do l=1,ilev
    do i=il1,il2
      sfrcpal (ioff+i,l,n) = phys_arrays%sfrcrol (i,l,n)
    enddo
  enddo
enddo
do l=1,ilev
  do i=il1,il2
    altipak(ioff+i,l)   = phys_arrays%altirow(i,l)
  enddo
enddo

do l=1,levoz
  do i=il1,il2
    ozpak  (ioff+i,l) = phys_arrays%ozrow  (i,l)
    ozpal  (ioff+i,l) = phys_arrays%ozrol  (i,l)
  enddo
enddo

do l=1,levozc
  do i=il1,il2
    o3cpak (ioff+i,l) = phys_arrays%o3crow (i,l)
    o3cpal (ioff+i,l) = phys_arrays%o3crol (i,l)
  enddo
enddo


csalpal(ioff+il1:ioff+il2,1:nbs)     =phys_arrays%csalrol(il1:il2,1:nbs)
csalpat(ioff+il1:ioff+il2,1:im,1:nbs)=phys_arrays%csalrot(il1:il2,1:im,1:nbs)
salbpal(ioff+il1:ioff+il2,1:nbs)     =phys_arrays%salbrol(il1:il2,1:nbs)
salbpat(ioff+il1:ioff+il2,1:im,1:nbs)=phys_arrays%salbrot(il1:il2,1:im,1:nbs)
csdpal(ioff+il1:ioff+il2) = phys_arrays%csdrol(il1:il2)
csfpal(ioff+il1:ioff+il2) = phys_arrays%csfrol(il1:il2)

do i=il1,il2
  alphpak(ioff+i) = phys_arrays%alphrow(i)
  begipal(ioff+i) = phys_arrays%begirol(i)
  begkpal(ioff+i) = phys_arrays%begkrol(i)
  beglpal(ioff+i) = phys_arrays%beglrol(i)
  begopal(ioff+i) = phys_arrays%begorol(i)
  begpal (ioff+i) = phys_arrays%begrol (i)
  bwgipal(ioff+i) = phys_arrays%bwgirol(i)
  bwgkpal(ioff+i) = phys_arrays%bwgkrol(i)
  bwglpal(ioff+i) = phys_arrays%bwglrol(i)
  bwgopal(ioff+i) = phys_arrays%bwgorol(i)
  bwgpal (ioff+i) = phys_arrays%bwgrol (i)
  cbmfpal(ioff+i) = phys_arrays%cbmfrol(i)
  chfxpak(ioff+i) = phys_arrays%chfxrow(i)
  cictpak(ioff+i) = phys_arrays%cictrow(i)
  clbpal (ioff+i) = phys_arrays%clbrol (i)
  cldtpak(ioff+i) = phys_arrays%cldtrow(i)
  cldopak(ioff+i) = phys_arrays%cldorow(i)
  cldopal(ioff+i) = phys_arrays%cldorol(i)
  cldapak(ioff+i) = phys_arrays%cldarow(i)
  cldapal(ioff+i) = phys_arrays%cldarol(i)
  clwtpak(ioff+i) = phys_arrays%clwtrow(i)
  cqfxpak(ioff+i) = phys_arrays%cqfxrow(i)
  csbpal (ioff+i) = phys_arrays%csbrol (i)
  cszpal (ioff+i) = phys_arrays%cszrol (i)
  deltpak(ioff+i) = phys_arrays%deltrow(i)
  dmsopak(ioff+i) = phys_arrays%dmsorow(i)
  dmsopal(ioff+i) = phys_arrays%dmsorol(i)
  edmspak(ioff+i) = phys_arrays%edmsrow(i)
  edmspal(ioff+i) = phys_arrays%edmsrol(i)
  emispak(ioff+i) = phys_arrays%emisrow(i)
  envpak (ioff+i) = phys_arrays%envrow (i)
  fdlpak (ioff+i) = phys_arrays%fdlrow (i)
  fdlcpak(ioff+i) = phys_arrays%fdlcrow(i)
  flampal(ioff+i) = phys_arrays%flamrol(i)
  flampak(ioff+i) = phys_arrays%flamrow(i)
  flanpak(ioff+i) = phys_arrays%flanrow(i)
  flanpal(ioff+i) = phys_arrays%flanrol(i)
  flgcpak(ioff+i) = phys_arrays%flgcrow(i)
  flgpak (ioff+i) = phys_arrays%flgrow (i)
  fsampak(ioff+i) = phys_arrays%fsamrow(i)
  fsampal(ioff+i) = phys_arrays%fsamrol(i)
  fsanpak(ioff+i) = phys_arrays%fsanrow(i)
  fsanpal(ioff+i) = phys_arrays%fsanrol(i)
  fsdcpak(ioff+i) = phys_arrays%fsdcrow(i)
  fsdpak (ioff+i) = phys_arrays%fsdrow (i)
  fsdpal (ioff+i) = phys_arrays%fsdrol (i)
  fsfpal (ioff+i) = phys_arrays%fsfrol (i)
  fsgcpak(ioff+i) = phys_arrays%fsgcrow(i)
  fsgpak (ioff+i) = phys_arrays%fsgrow (i)
  fsgipal(ioff+i) = phys_arrays%fsgirol(i)
  fsgopal(ioff+i) = phys_arrays%fsgorol(i)
  fsipal (ioff+i) = phys_arrays%fsirol (i)
  fslopak(ioff+i) = phys_arrays%fslorow(i)
  fslopal(ioff+i) = phys_arrays%fslorol(i)
  fsopak (ioff+i) = phys_arrays%fsorow (i)
  fsrcpak(ioff+i) = phys_arrays%fsrcrow(i)
  fsrpak (ioff+i) = phys_arrays%fsrrow (i)
  fsscpak(ioff+i) = phys_arrays%fsscrow(i)
  fsspak (ioff+i) = phys_arrays%fssrow (i)
  fsvpak (ioff+i) = phys_arrays%fsvrow (i)
  fsvpal (ioff+i) = phys_arrays%fsvrol (i)
  gampak (ioff+i) = phys_arrays%gamrow (i)
  gicnpak(ioff+i) = phys_arrays%gicnrow(i)
  gtapak (ioff+i) = phys_arrays%gtarow (i)
  gtpak  (ioff+i) = phys_arrays%gtrow  (i)
  hflpak (ioff+i) = phys_arrays%hflrow (i)
  hfspak (ioff+i) = phys_arrays%hfsrow (i)
  hseapal(ioff+i) = phys_arrays%hsearol(i)
  ofsgpal(ioff+i) = phys_arrays%ofsgrol(i)
  olrpak (ioff+i) = phys_arrays%olrrow (i)
  olrcpak(ioff+i) = phys_arrays%olrcrow(i)
  parpal (ioff+i) = phys_arrays%parrol (i)
  pblhpak(ioff+i) = phys_arrays%pblhrow(i)
  pbltpak(ioff+i) = phys_arrays%pbltrow(i)
  pchfpak(ioff+i) = phys_arrays%pchfrow(i)
  pcpcpak(ioff+i) = phys_arrays%pcpcrow(i)
  pcppak (ioff+i) = phys_arrays%pcprow (i)
  pcpspak(ioff+i) = phys_arrays%pcpsrow(i)
  plhfpak(ioff+i) = phys_arrays%plhfrow(i)
  pmslpal(ioff+i) = phys_arrays%pmslrol(i)
  psapak (ioff+i) = phys_arrays%psarow (i)
  pshfpak(ioff+i) = phys_arrays%pshfrow(i)
  psipak (ioff+i) = phys_arrays%psirow (i)
  pwatpak(ioff+i) = phys_arrays%pwatrow(i)
  pwatpam(ioff+i) = phys_arrays%pwatrom(i)
  qfspak (ioff+i) = phys_arrays%qfsrow (i)
  qfslpal(ioff+i) = phys_arrays%qfslrol(i)
  qfsopal(ioff+i) = phys_arrays%qfsorol(i)
  qfxpak (ioff+i) = phys_arrays%qfxrow (i)
  qtpfpam(ioff+i) = phys_arrays%qtpfrom(i)
  qtphpam(ioff+i) = phys_arrays%qtphrom(i)
  qtptpam(ioff+i) = phys_arrays%qtptrom(i)
  rainspal(ioff+i)= phys_arrays%rainsrol(i)
  sicnpak(ioff+i) = phys_arrays%sicnrow(i)
  sicpak (ioff+i) = phys_arrays%sicrow (i)
  sigxpak(ioff+i) = phys_arrays%sigxrow(i)
  slimpal(ioff+i) = phys_arrays%slimrol(i)
  slimplw(ioff+i) = phys_arrays%slimrlw(i)
  slimpsh(ioff+i) = phys_arrays%slimrsh(i)
  slimplh(ioff+i) = phys_arrays%slimrlh(i)
  snopak (ioff+i) = phys_arrays%snorow (i)
  snowspal(ioff+i)= phys_arrays%snowsrol(i)
  sqpak  (ioff+i) = phys_arrays%sqrow  (i)
  srhpak (ioff+i) = phys_arrays%srhrow (i)
  srhnpak(ioff+i) = phys_arrays%srhnrow(i)
  srhxpak(ioff+i) = phys_arrays%srhxrow(i)
  stmnpak(ioff+i) = phys_arrays%stmnrow(i)
  stmxpak(ioff+i) = phys_arrays%stmxrow(i)
  stpak  (ioff+i) = phys_arrays%strow  (i)
  supak  (ioff+i) = phys_arrays%surow  (i)
  svpak  (ioff+i) = phys_arrays%svrow  (i)
  swapak (ioff+i) = phys_arrays%swarow (i)
  swapal (ioff+i) = phys_arrays%swarol (i)
  swxpak (ioff+i) = phys_arrays%swxrow (i)
  swxupak(ioff+i) = phys_arrays%swxurow(i)
  swxvpak(ioff+i) = phys_arrays%swxvrow(i)
  tcvpak (ioff+i) = phys_arrays%tcvrow (i)
  tfxpak (ioff+i) = phys_arrays%tfxrow (i)
  ufspak (ioff+i) = phys_arrays%ufsrow (i)
  ufspal (ioff+i) = phys_arrays%ufsrol (i)
  ufsipal(ioff+i) = phys_arrays%ufsirol(i)
  ufsopal(ioff+i) = phys_arrays%ufsorol(i)
  vfspak (ioff+i) = phys_arrays%vfsrow (i)
  vfspal (ioff+i) = phys_arrays%vfsrol (i)
  vfsipal(ioff+i) = phys_arrays%vfsirol(i)
  vfsopal(ioff+i) = phys_arrays%vfsorol(i)
enddo
snopat(ioff+il1:ioff+il2,:) = phys_arrays%snorot(il1:il2,:)
gtpat (ioff+il1:ioff+il2,:) = phys_arrays%gtrot (il1:il2,:)
gtpax (ioff+il1:ioff+il2,:) = phys_arrays%gtrox (il1:il2,:)
emispat(ioff+il1:ioff+il2,:) = phys_arrays%emisrot(il1:il2,:)
xlmpat(ioff+il1:ioff+il2,:,:)=phys_arrays%xlmrot(il1:il2,:,:)

do l = 1, nbs
   do i = il1, il2
      fsdbpal (ioff+i,l)  = phys_arrays%fsdbrol (i,l)
      fsfbpal (ioff+i,l)  = phys_arrays%fsfbrol (i,l)
      csdbpal (ioff+i,l)  = phys_arrays%csdbrol (i,l)
      csfbpal (ioff+i,l)  = phys_arrays%csfbrol (i,l)
      fssbpal (ioff+i,l)  = phys_arrays%fssbrol (i,l)
      fsscbpal (ioff+i,l) = phys_arrays%fsscbrol (i,l)
      wrkapal (ioff+i,l)  = phys_arrays%wrkarol (i,l)
      wrkbpal (ioff+i,l)  = phys_arrays%wrkbrol (i,l)
   end do
end do
clbpat (ioff+il1:ioff+il2,:)=phys_arrays%clbrot (il1:il2,:)
csbpat (ioff+il1:ioff+il2,:)=phys_arrays%csbrot (il1:il2,:)
csdpat (ioff+il1:ioff+il2,:)=phys_arrays%csdrot (il1:il2,:)
csfpat (ioff+il1:ioff+il2,:)=phys_arrays%csfrot (il1:il2,:)
fdlpat (ioff+il1:ioff+il2,:)=phys_arrays%fdlrot (il1:il2,:)
fdlcpat(ioff+il1:ioff+il2,:)=phys_arrays%fdlcrot(il1:il2,:)
flgpat (ioff+il1:ioff+il2,:)=phys_arrays%flgrot (il1:il2,:)
fsdpat (ioff+il1:ioff+il2,:)=phys_arrays%fsdrot (il1:il2,:)
fsfpat (ioff+il1:ioff+il2,:)=phys_arrays%fsfrot (il1:il2,:)
fsgpat (ioff+il1:ioff+il2,:)=phys_arrays%fsgrot (il1:il2,:)
fsipat (ioff+il1:ioff+il2,:)=phys_arrays%fsirot (il1:il2,:)
fsvpat (ioff+il1:ioff+il2,:)=phys_arrays%fsvrot (il1:il2,:)
parpat (ioff+il1:ioff+il2,:)=phys_arrays%parrot (il1:il2,:)

do l = 1, nbs
  do m = 1, im
    do i = il1, il2
      fsdbpat (ioff+i,m,l)  = phys_arrays%fsdbrot (i,m,l)
      fsfbpat (ioff+i,m,l)  = phys_arrays%fsfbrot (i,m,l)
      csdbpat (ioff+i,m,l)  = phys_arrays%csdbrot (i,m,l)
      csfbpat (ioff+i,m,l)  = phys_arrays%csfbrot (i,m,l)
      fssbpat (ioff+i,m,l)  = phys_arrays%fssbrot (i,m,l)
      fsscbpat(ioff+i,m,l)  = phys_arrays%fsscbrot(i,m,l)
    end do
  end do
end do

! * land surface arrays.

do l=1,ignd
  do i=il1,il2
    dzgpak (ioff+i,l) = phys_arrays%dzgrow (i,l)
    hfcgpak(ioff+i,l) = phys_arrays%hfcgrow(i,l)
    hmfgpak(ioff+i,l) = phys_arrays%hmfgrow(i,l)
    porgpak(ioff+i,l) = phys_arrays%porgrow(i,l)
    fcappak(ioff+i,l) = phys_arrays%fcaprow(i,l)
    qfvgpak(ioff+i,l) = phys_arrays%qfvgrow(i,l)
    tgpak  (ioff+i,l) = phys_arrays%tgrow  (i,l)
    wgfpak (ioff+i,l) = phys_arrays%wgfrow (i,l)
    wglpak (ioff+i,l) = phys_arrays%wglrow (i,l)
  enddo
enddo
gflxpak(ioff+il1:ioff+il2,:)  =phys_arrays%gflxrow(il1:il2,:)
tgpat  (ioff+il1:ioff+il2,:,:)=phys_arrays%tgrot  (il1:il2,:,:)
thicpat(ioff+il1:ioff+il2,:,:)=phys_arrays%thicrot(il1:il2,:,:)
thlqpat(ioff+il1:ioff+il2,:,:)=phys_arrays%thlqrot(il1:il2,:,:)


do i=il1,il2
  anpak  (ioff+i) = phys_arrays%anrow  (i)
  drpak  (ioff+i) = phys_arrays%drrow  (i)
  flggpak(ioff+i) = phys_arrays%flggrow(i)
  flgnpak(ioff+i) = phys_arrays%flgnrow(i)
  flgvpak(ioff+i) = phys_arrays%flgvrow(i)
  fnlapak(ioff+i) = phys_arrays%fnlarow(i)
  fnpak  (ioff+i) = phys_arrays%fnrow  (i)
  fsggpak(ioff+i) = phys_arrays%fsggrow(i)
  fsgnpak(ioff+i) = phys_arrays%fsgnrow(i)
  fsgvpak(ioff+i) = phys_arrays%fsgvrow(i)
  fvgpak (ioff+i) = phys_arrays%fvgrow (i)
  fvnpak (ioff+i) = phys_arrays%fvnrow (i)
  gsnopak(ioff+i) = phys_arrays%gsnorow(i)
  hfcnpak(ioff+i) = phys_arrays%hfcnrow(i)
  hfcvpak(ioff+i) = phys_arrays%hfcvrow(i)
  hflgpak(ioff+i) = phys_arrays%hflgrow(i)
  hflnpak(ioff+i) = phys_arrays%hflnrow(i)
  hflvpak(ioff+i) = phys_arrays%hflvrow(i)
  hfsgpak(ioff+i) = phys_arrays%hfsgrow(i)
  hfsnpak(ioff+i) = phys_arrays%hfsnrow(i)
  hfsvpak(ioff+i) = phys_arrays%hfsvrow(i)
  hmfnpak(ioff+i) = phys_arrays%hmfnrow(i)
  hmfvpak(ioff+i) = phys_arrays%hmfvrow(i)
  mvpak  (ioff+i) = phys_arrays%mvrow  (i)
  pcpnpak(ioff+i) = phys_arrays%pcpnrow(i)
  pigpak (ioff+i) = phys_arrays%pigrow (i)
  pinpak (ioff+i) = phys_arrays%pinrow (i)
  pivfpak(ioff+i) = phys_arrays%pivfrow(i)
  pivlpak(ioff+i) = phys_arrays%pivlrow(i)
  qfgpak (ioff+i) = phys_arrays%qfgrow (i)
  qfnpak (ioff+i) = phys_arrays%qfnrow (i)
  qfvfpak(ioff+i) = phys_arrays%qfvfrow(i)
  qfvlpak(ioff+i) = phys_arrays%qfvlrow(i)
  rhonpak(ioff+i) = phys_arrays%rhonrow(i)
  rofnpak(ioff+i) = phys_arrays%rofnrow(i)
  rofopak(ioff+i) = phys_arrays%roforow(i)
  rofopal(ioff+i) = phys_arrays%roforol(i)
  rofpak (ioff+i) = phys_arrays%rofrow (i)
  rofpal (ioff+i) = phys_arrays%rofrol (i)
  rofvpak(ioff+i) = phys_arrays%rofvrow(i)
  rovgpak(ioff+i) = phys_arrays%rovgrow(i)
  skygpak(ioff+i) = phys_arrays%skygrow(i)
  skynpak(ioff+i) = phys_arrays%skynrow(i)
  smltpak(ioff+i) = phys_arrays%smltrow(i)
  tbaspak(ioff+i) = phys_arrays%tbasrow(i)
  tnpak  (ioff+i) = phys_arrays%tnrow  (i)
  ttpak  (ioff+i) = phys_arrays%ttrow  (i)
  tvpak  (ioff+i) = phys_arrays%tvrow  (i)
  wtrgpak(ioff+i) = phys_arrays%wtrgrow(i)
  wtrnpak(ioff+i) = phys_arrays%wtrnrow(i)
  wtrvpak(ioff+i) = phys_arrays%wtrvrow(i)
  wvfpak (ioff+i) = phys_arrays%wvfrow (i)
  wvlpak (ioff+i) = phys_arrays%wvlrow (i)
  znpak  (ioff+i) = phys_arrays%znrow  (i)
  gapak  (ioff+i) = phys_arrays%garow  (i)
  hblpak (ioff+i) = phys_arrays%hblrow (i)
  ilmopak(ioff+i) = phys_arrays%ilmorow(i)
  petpak (ioff+i) = phys_arrays%petrow (i)
  rofbpak(ioff+i) = phys_arrays%rofbrow(i)
  rofspak(ioff+i) = phys_arrays%rofsrow(i)
  uepak  (ioff+i) = phys_arrays%uerow  (i)
  wsnopak(ioff+i) = phys_arrays%wsnorow(i)
  wtabpak(ioff+i) = phys_arrays%wtabrow(i)
  zpndpak(ioff+i) = phys_arrays%zpndrow(i)
  refpak (ioff+i) = phys_arrays%refrow (i)
  bcsnpak(ioff+i) = phys_arrays%bcsnrow(i)
  depbpak(ioff+i) = phys_arrays%depbrow(i)
enddo

! * accumulated 3h variables

cldt3hpak(ioff+il1:ioff+il2)=phys_arrays%cldt3hrow(il1:il2)
hfl3hpak (ioff+il1:ioff+il2)=phys_arrays%hfl3hrow (il1:il2)
hfs3hpak (ioff+il1:ioff+il2)=phys_arrays%hfs3hrow (il1:il2)
rof3hpal (ioff+il1:ioff+il2)=phys_arrays%rof3hrol (il1:il2)
wgl3hpak (ioff+il1:ioff+il2)=phys_arrays%wgl3hrow (il1:il2)
wgf3hpak (ioff+il1:ioff+il2)=phys_arrays%wgf3hrow (il1:il2)
pcp3hpak (ioff+il1:ioff+il2)=phys_arrays%pcp3hrow (il1:il2)
pcpc3hpak(ioff+il1:ioff+il2)=phys_arrays%pcpc3hrow(il1:il2)
pcpl3hpak(ioff+il1:ioff+il2)=phys_arrays%pcpl3hrow(il1:il2)
pcps3hpak(ioff+il1:ioff+il2)=phys_arrays%pcps3hrow(il1:il2)
pcpn3hpak(ioff+il1:ioff+il2)=phys_arrays%pcpn3hrow(il1:il2)
fdl3hpak (ioff+il1:ioff+il2)=phys_arrays%fdl3hrow (il1:il2)
fdlc3hpak(ioff+il1:ioff+il2)=phys_arrays%fdlc3hrow(il1:il2)
flg3hpak (ioff+il1:ioff+il2)=phys_arrays%flg3hrow (il1:il2)
fss3hpak (ioff+il1:ioff+il2)=phys_arrays%fss3hrow (il1:il2)
fssc3hpak(ioff+il1:ioff+il2)=phys_arrays%fssc3hrow(il1:il2)
fsd3hpak (ioff+il1:ioff+il2)=phys_arrays%fsd3hrow (il1:il2)
fsg3hpak (ioff+il1:ioff+il2)=phys_arrays%fsg3hrow (il1:il2)
fsgc3hpak(ioff+il1:ioff+il2)=phys_arrays%fsgc3hrow(il1:il2)
sq3hpak  (ioff+il1:ioff+il2)=phys_arrays%sq3hrow  (il1:il2)
st3hpak  (ioff+il1:ioff+il2)=phys_arrays%st3hrow  (il1:il2)
su3hpak  (ioff+il1:ioff+il2)=phys_arrays%su3hrow  (il1:il2)
sv3hpak  (ioff+il1:ioff+il2)=phys_arrays%sv3hrow  (il1:il2)

olr3hpak(ioff+il1:ioff+il2)  = phys_arrays%olr3hrow(il1:il2)
fsr3hpak(ioff+il1:ioff+il2)  = phys_arrays%fsr3hrow(il1:il2)
olrc3hpak(ioff+il1:ioff+il2) = phys_arrays%olrc3hrow(il1:il2)
fsrc3hpak(ioff+il1:ioff+il2) = phys_arrays%fsrc3hrow(il1:il2)
fso3hpak(ioff+il1:ioff+il2)  = phys_arrays%fso3hrow(il1:il2)
pwat3hpak(ioff+il1:ioff+il2) = phys_arrays%pwat3hrow(il1:il2)
clwt3hpak(ioff+il1:ioff+il2) = phys_arrays%clwt3hrow(il1:il2)
cict3hpak(ioff+il1:ioff+il2) = phys_arrays%cict3hrow(il1:il2)
pmsl3hpak(ioff+il1:ioff+il2) = phys_arrays%pmsl3hrow(il1:il2)

!
!  * instantaneous variables for sampling
!
do i=il1,il2
   swpal(ioff+i)      = phys_arrays%swrol(i)
   srhpal(ioff+i)     = phys_arrays%srhrol(i)
   pcppal(ioff+i)     = phys_arrays%pcprol(i)
   pcpnpal(ioff+i)    = phys_arrays%pcpnrol(i)
   pcpcpal(ioff+i)    = phys_arrays%pcpcrol(i)
   qfsipal(ioff+i)    = phys_arrays%qfsirol(i)
   qfnpal(ioff+i)     = phys_arrays%qfnrol(i)
   qfvfpal(ioff+i)    = phys_arrays%qfvfrol(i)
   ufsinstpal(ioff+i) = phys_arrays%ufsinstrol(i)
   vfsinstpal(ioff+i) = phys_arrays%vfsinstrol(i)
   hflipal(ioff+i)    = phys_arrays%hflirol(i)
   hfsipal(ioff+i)    = phys_arrays%hfsirol(i)
   fdlpal(ioff+i)     = phys_arrays%fdlrol(i)
   flgpal(ioff+i)     = phys_arrays%flgrol(i)
   fsspal(ioff+i)     = phys_arrays%fssrol(i)
   fsgpal(ioff+i)     = phys_arrays%fsgrol(i)
   fsscpal(ioff+i)    = phys_arrays%fsscrol(i)
   fsgcpal(ioff+i)    = phys_arrays%fsgcrol(i)
   fdlcpal(ioff+i)    = phys_arrays%fdlcrol(i)
   fsopal(ioff+i)     = phys_arrays%fsorol(i)
   fsrpal(ioff+i)     = phys_arrays%fsrrol(i)
   olrpal(ioff+i)     = phys_arrays%olrrol(i)
   olrcpal(ioff+i)    = phys_arrays%olrcrol(i)
   fsrcpal(ioff+i)    = phys_arrays%fsrcrol(i)
   pwatipal(ioff+i)   = phys_arrays%pwatirol(i)
   cldtpal(ioff+i)    = phys_arrays%cldtrol(i)
   clwtpal(ioff+i)    = phys_arrays%clwtrol(i)
   cictpal(ioff+i)    = phys_arrays%cictrol(i)
   baltpal(ioff+i)    = phys_arrays%baltrol(i)
   pmslipal(ioff+i)   = phys_arrays%pmslirol(i)
   cdcbpal(ioff+i)    = phys_arrays%cdcbrol(i)
   cscbpal(ioff+i)    = phys_arrays%cscbrol(i)
   gtpal(ioff+i)      = phys_arrays%gtrol(i)
   tcdpal(ioff+i)     = phys_arrays%tcdrol(i)
   bcdpal(ioff+i)     = phys_arrays%bcdrol(i)
   pspal(ioff+i)      = phys_arrays%psrol(i)
   stpal(ioff+i)      = phys_arrays%strol(i)
   supal(ioff+i)      = phys_arrays%surol(i)
   svpal(ioff+i)      = phys_arrays%svrol(i)
end do ! i

do l = 1, ilev
   do i = il1, il2
     dmcpal(ioff+i,l)  = phys_arrays%dmcrol(i,l)
     tapal(ioff+i,l)   = phys_arrays%tarol(i,l)
     uapal(ioff+i,l)   = phys_arrays%uarol(i,l)
     vapal(ioff+i,l)   = phys_arrays%varol(i,l)
     qapal(ioff+i,l)   = phys_arrays%qarol(i,l)
     rhpal(ioff+i,l)   = phys_arrays%rhrol(i,l)
     zgpal(ioff+i,l)   = phys_arrays%zgrol(i,l)
     rkhpal(ioff+i,l)  = phys_arrays%rkhrol(i,l)
     rkmpal(ioff+i,l)  = phys_arrays%rkmrol(i,l)
     rkqpal(ioff+i,l)  = phys_arrays%rkqrol(i,l)

     ttppal(ioff+i,l)  = phys_arrays%ttprol(i,l)
     ttpppal(ioff+i,l) = phys_arrays%ttpprol(i,l)
     ttpvpal(ioff+i,l) = phys_arrays%ttpvrol(i,l)
     ttplpal(ioff+i,l) = phys_arrays%ttplrol(i,l)
     ttpspal(ioff+i,l) = phys_arrays%ttpsrol(i,l)
     ttlcpal(ioff+i,l) = phys_arrays%ttlcrol(i,l)
     ttscpal(ioff+i,l) = phys_arrays%ttscrol(i,l)
     ttpcpal(ioff+i,l) = phys_arrays%ttpcrol(i,l)
     ttpmpal(ioff+i,l) = phys_arrays%ttpmrol(i,l)

     qtppal(ioff+i,l)  = phys_arrays%qtprol(i,l)
     qtpppal(ioff+i,l) = phys_arrays%qtpprol(i,l)
     qtpvpal(ioff+i,l) = phys_arrays%qtpvrol(i,l)
     qtpcpal(ioff+i,l) = phys_arrays%qtpcrol(i,l)
     qtpmpal(ioff+i,l) = phys_arrays%qtpmrol(i,l)
  end do ! i
end do ! l

anpat(ioff+il1:ioff+il2,:) = phys_arrays%anrot(il1:il2,:)
drnpat(ioff+il1:ioff+il2,:) = phys_arrays%drnrot(il1:il2,:)
fnpat(ioff+il1:ioff+il2,:) = phys_arrays%fnrot(il1:il2,:)
mvpat(ioff+il1:ioff+il2,:) = phys_arrays%mvrot(il1:il2,:)
qavpat(ioff+il1:ioff+il2,:) = phys_arrays%qavrot(il1:il2,:)
rhonpat(ioff+il1:ioff+il2,:) = phys_arrays%rhonrot(il1:il2,:)
tavpat(ioff+il1:ioff+il2,:) = phys_arrays%tavrot(il1:il2,:)
tbaspat(ioff+il1:ioff+il2,:) = phys_arrays%tbasrot(il1:il2,:)
tnpat(ioff+il1:ioff+il2,:) = phys_arrays%tnrot(il1:il2,:)
tpndpat(ioff+il1:ioff+il2,:) = phys_arrays%tpndrot(il1:il2,:)
ttpat(ioff+il1:ioff+il2,:) = phys_arrays%ttrot(il1:il2,:)
tvpat(ioff+il1:ioff+il2,:) = phys_arrays%tvrot(il1:il2,:)
wsnopat(ioff+il1:ioff+il2,:) = phys_arrays%wsnorot(il1:il2,:)
wvfpat(ioff+il1:ioff+il2,:) = phys_arrays%wvfrot(il1:il2,:)
wvlpat(ioff+il1:ioff+il2,:) = phys_arrays%wvlrot(il1:il2,:)
zpndpat(ioff+il1:ioff+il2,:) = phys_arrays%zpndrot(il1:il2,:)
refpat (ioff+il1:ioff+il2,:) = phys_arrays%refrot (il1:il2,:)
bcsnpat(ioff+il1:ioff+il2,:) = phys_arrays%bcsnrot(il1:il2,:)

farepat(ioff+il1:ioff+il2,:) = phys_arrays%farerot(il1:il2,:)
tsfspat(ioff+il1:ioff+il2,:,:)=phys_arrays%tsfsrot(il1:il2,:,:)

if(kount.eq.kstart) then
  dlzwpat(ioff+il1:ioff+il2,:,:)=phys_arrays%dlzwrot(il1:il2,:,:)
  porgpat(ioff+il1:ioff+il2,:,:)=phys_arrays%porgrot(il1:il2,:,:)
  thfcpat(ioff+il1:ioff+il2,:,:)=phys_arrays%thfcrot(il1:il2,:,:)
  thlwpat(ioff+il1:ioff+il2,:,:)=phys_arrays%thlwrot(il1:il2,:,:)
  thrpat (ioff+il1:ioff+il2,:,:)=phys_arrays%thrrot (il1:il2,:,:)
  thmpat (ioff+il1:ioff+il2,:,:)=phys_arrays%thmrot (il1:il2,:,:)
  bipat  (ioff+il1:ioff+il2,:,:)=phys_arrays%birot  (il1:il2,:,:)
  psispat(ioff+il1:ioff+il2,:,:)=phys_arrays%psisrot(il1:il2,:,:)
  grkspat(ioff+il1:ioff+il2,:,:)=phys_arrays%grksrot(il1:il2,:,:)
  thrapat(ioff+il1:ioff+il2,:,:)=phys_arrays%thrarot(il1:il2,:,:)
  hcpspat(ioff+il1:ioff+il2,:,:)=phys_arrays%hcpsrot(il1:il2,:,:)
  tcspat (ioff+il1:ioff+il2,:,:)=phys_arrays%tcsrot (il1:il2,:,:)
  psiwpat(ioff+il1:ioff+il2,:,:)=phys_arrays%psiwrot(il1:il2,:,:)
  zbtwpat(ioff+il1:ioff+il2,:,:)=phys_arrays%zbtwrot(il1:il2,:,:)
  isndpat(ioff+il1:ioff+il2,:,:)=phys_arrays%isndrot(il1:il2,:,:)

  algdvpat(ioff+il1:ioff+il2,:) =phys_arrays%algdvrot(il1:il2,:)
  algdnpat(ioff+il1:ioff+il2,:) =phys_arrays%algdnrot(il1:il2,:)
  algwvpat(ioff+il1:ioff+il2,:) =phys_arrays%algwvrot(il1:il2,:)
  algwnpat(ioff+il1:ioff+il2,:) =phys_arrays%algwnrot(il1:il2,:)
  igdrpat(ioff+il1:ioff+il2,:)  =phys_arrays%igdrrot(il1:il2,:)
endif

! * tke fields.

do l=1,ilev
  do i=il1,il2
    tkempak (ioff+i,l) = phys_arrays%tkemrow (i,l)
    xlmpak  (ioff+i,l) = phys_arrays%xlmrow  (i,l)
    svarpak (ioff+i,l) = phys_arrays%svarrow (i,l)
  enddo
enddo
#if defined (agcm_ctem)

      rmatcpat  (ioff+il1:ioff+il2,:,:,:) = phys_arrays%rmatcrot  (il1:il2,:,:,:)
      rtctmpat  (ioff+il1:ioff+il2,:,:,:) = phys_arrays%rtctmrot  (il1:il2,:,:,:)

      cfcanpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%cfcanrot  (il1:il2,:,:)
      zolncpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%zolncrot  (il1:il2,:,:)
      ailcpat   (ioff+il1:ioff+il2,:,:) = phys_arrays%ailcrot   (il1:il2,:,:)
      cmascpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%cmascrot  (il1:il2,:,:)
      calvcpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%calvcrot  (il1:il2,:,:)
      calicpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%calicrot  (il1:il2,:,:)
      paicpat   (ioff+il1:ioff+il2,:,:) = phys_arrays%paicrot   (il1:il2,:,:)
      slaicpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%slaicrot  (il1:il2,:,:)
      fcancpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%fcancrot  (il1:il2,:,:)
      todfcpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%todfcrot  (il1:il2,:,:)
      ailcgpat  (ioff+il1:ioff+il2,:,:) = phys_arrays%ailcgrot  (il1:il2,:,:)
      slaipat   (ioff+il1:ioff+il2,:,:) = phys_arrays%slairot   (il1:il2,:,:)
      fsnowptl  (ioff+il1:ioff+il2,:)   = phys_arrays%fsnowrtl  (il1:il2,:)
      tcanoptl  (ioff+il1:ioff+il2,:)   = phys_arrays%tcanortl  (il1:il2,:)
      tcansptl  (ioff+il1:ioff+il2,:)   = phys_arrays%tcansrtl  (il1:il2,:)
      taptl     (ioff+il1:ioff+il2,:)   = phys_arrays%tartl     (il1:il2,:)
      cfluxcspat(ioff+il1:ioff+il2,:)   = phys_arrays%cfluxcsrot(il1:il2,:)
      cfluxcgpat(ioff+il1:ioff+il2,:)   = phys_arrays%cfluxcgrot(il1:il2,:)

      co2cg1pat (ioff+il1:ioff+il2,:,:) = phys_arrays%co2cg1rot (il1:il2,:,:)
      co2cg2pat (ioff+il1:ioff+il2,:,:) = phys_arrays%co2cg2rot (il1:il2,:,:)
      co2cs1pat (ioff+il1:ioff+il2,:,:) = phys_arrays%co2cs1rot (il1:il2,:,:)
      co2cs2pat (ioff+il1:ioff+il2,:,:) = phys_arrays%co2cs2rot (il1:il2,:,:)
      ancgptl   (ioff+il1:ioff+il2,:,:) = phys_arrays%ancgrtl   (il1:il2,:,:)
      ancsptl   (ioff+il1:ioff+il2,:,:) = phys_arrays%ancsrtl   (il1:il2,:,:)
      rmlcgptl  (ioff+il1:ioff+il2,:,:) = phys_arrays%rmlcgrtl  (il1:il2,:,:)
      rmlcsptl  (ioff+il1:ioff+il2,:,:) = phys_arrays%rmlcsrtl  (il1:il2,:,:)

      tbarptl   (ioff+il1:ioff+il2,:,:) = phys_arrays%tbarrtl   (il1:il2,:,:)
      tbarcptl  (ioff+il1:ioff+il2,:,:) = phys_arrays%tbarcrtl  (il1:il2,:,:)
      tbarcsptl (ioff+il1:ioff+il2,:,:) = phys_arrays%tbarcsrtl (il1:il2,:,:)
      tbargptl  (ioff+il1:ioff+il2,:,:) = phys_arrays%tbargrtl  (il1:il2,:,:)
      tbargsptl (ioff+il1:ioff+il2,:,:) = phys_arrays%tbargsrtl (il1:il2,:,:)
      thliqcptl (ioff+il1:ioff+il2,:,:) = phys_arrays%thliqcrtl (il1:il2,:,:)
      thliqgptl (ioff+il1:ioff+il2,:,:) = phys_arrays%thliqgrtl (il1:il2,:,:)
      thicecptl (ioff+il1:ioff+il2,:,:) = phys_arrays%thicecrtl (il1:il2,:,:)
!
!     * time varying.
!
      soilcpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%soilcrot  (il1:il2,:,:)
      litrcpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%litrcrot  (il1:il2,:,:)
      rootcpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%rootcrot  (il1:il2,:,:)
      stemcpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%stemcrot  (il1:il2,:,:)
      gleafcpat (ioff+il1:ioff+il2,:,:)   = phys_arrays%gleafcrot (il1:il2,:,:)
      bleafcpat (ioff+il1:ioff+il2,:,:)   = phys_arrays%bleafcrot (il1:il2,:,:)
      fallhpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%fallhrot  (il1:il2,:,:)
      posphpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%posphrot  (il1:il2,:,:)
      leafspat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%leafsrot  (il1:il2,:,:)
      growtpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%growtrot  (il1:il2,:,:)
      lastrpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%lastrrot  (il1:il2,:,:)
      lastspat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%lastsrot  (il1:il2,:,:)
      thisylpat (ioff+il1:ioff+il2,:,:)   = phys_arrays%thisylrot (il1:il2,:,:)
      stemhpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%stemhrot  (il1:il2,:,:)
      roothpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%roothrot  (il1:il2,:,:)
      tempcpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%tempcrot  (il1:il2,:,:)
      ailcbpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%ailcbrot  (il1:il2,:,:)
      bmasvpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%bmasvrot  (il1:il2,:,:)
      veghpat   (ioff+il1:ioff+il2,:,:)   = phys_arrays%veghrot   (il1:il2,:,:)
      rootdpat  (ioff+il1:ioff+il2,:,:)   = phys_arrays%rootdrot  (il1:il2,:,:)
!
      prefpat   (ioff+il1:ioff+il2,:,:)   = phys_arrays%prefrot   (il1:il2,:,:)
      newfpat   (ioff+il1:ioff+il2,:,:)   = phys_arrays%newfrot   (il1:il2,:,:)
!
      cvegpat   (ioff+il1:ioff+il2,:)     = phys_arrays%cvegrot   (il1:il2,:)
      cdebpat   (ioff+il1:ioff+il2,:)     = phys_arrays%cdebrot   (il1:il2,:)
      chumpat   (ioff+il1:ioff+il2,:)     = phys_arrays%chumrot   (il1:il2,:)
      fcolpat   (ioff+il1:ioff+il2,:)     = phys_arrays%fcolrot   (il1:il2,:)
!
!     * ctem diagnostic output fields.
!
      cvegpak(ioff+il1:ioff+il2) = phys_arrays%cvegrow(il1:il2)
      cdebpak(ioff+il1:ioff+il2) = phys_arrays%cdebrow(il1:il2)
      chumpak(ioff+il1:ioff+il2) = phys_arrays%chumrow(il1:il2)
      claipak(ioff+il1:ioff+il2) = phys_arrays%clairow(il1:il2)
      cfnppak(ioff+il1:ioff+il2) = phys_arrays%cfnprow(il1:il2)
      cfnepak(ioff+il1:ioff+il2) = phys_arrays%cfnerow(il1:il2)
      cfrvpak(ioff+il1:ioff+il2) = phys_arrays%cfrvrow(il1:il2)
      cfgppak(ioff+il1:ioff+il2) = phys_arrays%cfgprow(il1:il2)
      cfnbpak(ioff+il1:ioff+il2) = phys_arrays%cfnbrow(il1:il2)
      cffvpak(ioff+il1:ioff+il2) = phys_arrays%cffvrow(il1:il2)
      cffdpak(ioff+il1:ioff+il2) = phys_arrays%cffdrow(il1:il2)
      cflvpak(ioff+il1:ioff+il2) = phys_arrays%cflvrow(il1:il2)
      cfldpak(ioff+il1:ioff+il2) = phys_arrays%cfldrow(il1:il2)
      cflhpak(ioff+il1:ioff+il2) = phys_arrays%cflhrow(il1:il2)
      cbrnpak(ioff+il1:ioff+il2) = phys_arrays%cbrnrow(il1:il2)
      cfrhpak(ioff+il1:ioff+il2) = phys_arrays%cfrhrow(il1:il2)
      cfhtpak(ioff+il1:ioff+il2) = phys_arrays%cfhtrow(il1:il2)
      cflfpak(ioff+il1:ioff+il2) = phys_arrays%cflfrow(il1:il2)
      cfrdpak(ioff+il1:ioff+il2) = phys_arrays%cfrdrow(il1:il2)
      cfrgpak(ioff+il1:ioff+il2) = phys_arrays%cfrgrow(il1:il2)
      cfrmpak(ioff+il1:ioff+il2) = phys_arrays%cfrmrow(il1:il2)
      cvglpak(ioff+il1:ioff+il2) = phys_arrays%cvglrow(il1:il2)
      cvgspak(ioff+il1:ioff+il2) = phys_arrays%cvgsrow(il1:il2)
      cvgrpak(ioff+il1:ioff+il2) = phys_arrays%cvgrrow(il1:il2)
      cfnlpak(ioff+il1:ioff+il2) = phys_arrays%cfnlrow(il1:il2)
      cfnspak(ioff+il1:ioff+il2) = phys_arrays%cfnsrow(il1:il2)
      cfnrpak(ioff+il1:ioff+il2) = phys_arrays%cfnrrow(il1:il2)
      ch4hpak(ioff+il1:ioff+il2) = phys_arrays%ch4hrow(il1:il2)
      ch4npak(ioff+il1:ioff+il2) = phys_arrays%ch4nrow(il1:il2)
      wfrapak(ioff+il1:ioff+il2) = phys_arrays%wfrarow(il1:il2)
      cw1dpak(ioff+il1:ioff+il2) = phys_arrays%cw1drow(il1:il2)
      cw2dpak(ioff+il1:ioff+il2) = phys_arrays%cw2drow(il1:il2)
      fcolpak(ioff+il1:ioff+il2) = phys_arrays%fcolrow(il1:il2)
      curfpak(ioff+il1:ioff+il2,:) = phys_arrays%curfrow(il1:il2,:)

!     additional ctem related cmip6 output
      brfrpak(ioff+il1:ioff+il2) = phys_arrays%brfrrow(il1:il2)   ! baresoilfrac
      c3crpak(ioff+il1:ioff+il2) = phys_arrays%c3crrow(il1:il2)   ! cropfracc3
      c4crpak(ioff+il1:ioff+il2) = phys_arrays%c4crrow(il1:il2)   ! cropfracc4
      crpfpak(ioff+il1:ioff+il2) = phys_arrays%crpfrow(il1:il2)   ! cropfrac
      c3grpak(ioff+il1:ioff+il2) = phys_arrays%c3grrow(il1:il2)   ! grassfracc3
      c4grpak(ioff+il1:ioff+il2) = phys_arrays%c4grrow(il1:il2)   ! grassfracc4
      grsfpak(ioff+il1:ioff+il2) = phys_arrays%grsfrow(il1:il2)   ! grassfrac
      bdtfpak(ioff+il1:ioff+il2) = phys_arrays%bdtfrow(il1:il2)   ! treefracbdldcd
      betfpak(ioff+il1:ioff+il2) = phys_arrays%betfrow(il1:il2)   ! treefracbdlevg
      ndtfpak(ioff+il1:ioff+il2) = phys_arrays%ndtfrow(il1:il2)   ! treefracndldcd
      netfpak(ioff+il1:ioff+il2) = phys_arrays%netfrow(il1:il2)   ! treefracndlevg
      treepak(ioff+il1:ioff+il2) = phys_arrays%treerow(il1:il2)   ! treefrac
      vegfpak(ioff+il1:ioff+il2) = phys_arrays%vegfrow(il1:il2)   ! vegfrac
      c3pfpak(ioff+il1:ioff+il2) = phys_arrays%c3pfrow(il1:il2)   ! c3pftfrac
      c4pfpak(ioff+il1:ioff+il2) = phys_arrays%c4pfrow(il1:il2)   ! c4pftfrac

      clndpak(ioff+il1:ioff+il2) = phys_arrays%clndrow(il1:il2)   ! cland

!
! instantaneous output (used for subdaily output)
!
      cfgppal(ioff+il1:ioff+il2) = phys_arrays%cfgprol(il1:il2)
      cfrvpal(ioff+il1:ioff+il2) = phys_arrays%cfrvrol(il1:il2)
      cfrhpal(ioff+il1:ioff+il2) = phys_arrays%cfrhrol(il1:il2)
      cfrdpal(ioff+il1:ioff+il2) = phys_arrays%cfrdrol(il1:il2)

#endif
#if defined explvol
!
!     * explosive volcano fields.
!
      vtaupak(ioff+il1:ioff+il2) = phys_arrays%vtaurow(il1:il2)
      vtaupal(ioff+il1:ioff+il2) = phys_arrays%vtaurol(il1:il2)
      troppak(ioff+il1:ioff+il2) = phys_arrays%troprow(il1:il2)
      troppal(ioff+il1:ioff+il2) = phys_arrays%troprol(il1:il2)

      do ib = 1, nbs
         do l = 1, levsa
            sw_ext_sa_pak(ioffz+il1z,l,ib) = phys_arrays%sw_ext_sa_row(il1,l,ib)
            sw_ext_sa_pal(ioffz+il1z,l,ib) = phys_arrays%sw_ext_sa_rol(il1,l,ib)
            sw_ssa_sa_pak(ioffz+il1z,l,ib) = phys_arrays%sw_ssa_sa_row(il1,l,ib)
            sw_ssa_sa_pal(ioffz+il1z,l,ib) = phys_arrays%sw_ssa_sa_rol(il1,l,ib)
            sw_g_sa_pak(ioffz+il1z,l,ib)   = phys_arrays%sw_g_sa_row(il1,l,ib)
            sw_g_sa_pal(ioffz+il1z,l,ib)   = phys_arrays%sw_g_sa_rol(il1,l,ib)
         end do ! l
      end do ! ib

      do ib = 1, nbl
         do l = 1, levsa
	         lw_ext_sa_pak(ioffz+il1z,l,ib) = phys_arrays%lw_ext_sa_row(il1,l,ib)
	         lw_ext_sa_pal(ioffz+il1z,l,ib) = phys_arrays%lw_ext_sa_rol(il1,l,ib)
	         lw_ssa_sa_pak(ioffz+il1z,l,ib) = phys_arrays%lw_ssa_sa_row(il1,l,ib)
	         lw_ssa_sa_pal(ioffz+il1z,l,ib) = phys_arrays%lw_ssa_sa_rol(il1,l,ib)
	         lw_g_sa_pak(ioffz+il1z,l,ib)   = phys_arrays%lw_g_sa_row(il1,l,ib)
	         lw_g_sa_pal(ioffz+il1z,l,ib)   = phys_arrays%lw_g_sa_rol(il1,l,ib)
         end do ! l
      end do ! ib

      do l = 1, levsa
        w055_ext_sa_pak(ioffz+il1z,l) = phys_arrays%w055_ext_sa_row(il1,l)
        w110_ext_sa_pak(ioffz+il1z,l) = phys_arrays%w110_ext_sa_row(il1,l)
        w055_ext_sa_pal(ioffz+il1z,l) = phys_arrays%w055_ext_sa_rol(il1,l)
        w110_ext_sa_pal(ioffz+il1z,l) = phys_arrays%w110_ext_sa_rol(il1,l)
        pressure_sa_pak(ioffz+il1z,l) = phys_arrays%pressure_sa_row(il1,l)
        pressure_sa_pal(ioffz+il1z,l) = phys_arrays%pressure_sa_rol(il1,l)
      end do ! l

      do i = il1, il2
         w055_vtau_sa_pak(ioff+i) = phys_arrays%w055_vtau_sa_row(i)
         w055_vtau_sa_pal(ioff+i) = phys_arrays%w055_vtau_sa_rol(i)
         w110_vtau_sa_pak(ioff+i) = phys_arrays%w110_vtau_sa_row(i)
         w110_vtau_sa_pal(ioff+i) = phys_arrays%w110_vtau_sa_rol(i)
      end do ! i

      do l = 1, ilev
         do i = il1, il2
            w055_ext_gcm_sa_pak(ioff+i,l) = phys_arrays%w055_ext_gcm_sa_row(i,l)
            w055_ext_gcm_sa_pal(ioff+i,l) = phys_arrays%w055_ext_gcm_sa_rol(i,l)
            w110_ext_gcm_sa_pak(ioff+i,l) = phys_arrays%w110_ext_gcm_sa_row(i,l)
            w110_ext_gcm_sa_pal(ioff+i,l) = phys_arrays%w110_ext_gcm_sa_rol(i,l)
         end do ! i
      end do ! l

#endif
!
!     * lakes fields.
!
do i=il1,il2
  ldmxpak(ioff+i) = phys_arrays%ldmxrow(i)
  lrimpak(ioff+i) = phys_arrays%lrimrow(i)
  lrinpak(ioff+i) = phys_arrays%lrinrow(i)
  luimpak(ioff+i) = phys_arrays%luimrow(i)
  luinpak(ioff+i) = phys_arrays%luinrow(i)
  lzicpak(ioff+i) = phys_arrays%lzicrow(i)
#if defined (cslm)
        delupak(ioff+i) = phys_arrays%delurow(i)
        dtmppak(ioff+i) = phys_arrays%dtmprow(i)
        expwpak(ioff+i) = phys_arrays%expwrow(i)
        gredpak(ioff+i) = phys_arrays%gredrow(i)
        rhompak(ioff+i) = phys_arrays%rhomrow(i)
        t0lkpak(ioff+i) = phys_arrays%t0lkrow(i)
        tkelpak(ioff+i) = phys_arrays%tkelrow(i)
        flglpak(ioff+i) = phys_arrays%flglrow(i)
        fnlpak (ioff+i) = phys_arrays%fnlrow (i)
        fsglpak(ioff+i) = phys_arrays%fsglrow(i)
        hfclpak(ioff+i) = phys_arrays%hfclrow(i)
        hfllpak(ioff+i) = phys_arrays%hfllrow(i)
        hfslpak(ioff+i) = phys_arrays%hfslrow(i)
        hmflpak(ioff+i) = phys_arrays%hmflrow(i)
        pilpak (ioff+i) = phys_arrays%pilrow (i)
        qflpak (ioff+i) = phys_arrays%qflrow (i)
!
        do l=1,nlklm
          tlakpak(ioff+i,l) = phys_arrays%tlakrow(i,l)
        end do
#endif
#if defined (flake)
        lshppak(ioff+i) = phys_arrays%lshprow(i)
        ltavpak(ioff+i) = phys_arrays%ltavrow(i)
        lticpak(ioff+i) = phys_arrays%lticrow(i)
        ltmxpak(ioff+i) = phys_arrays%ltmxrow(i)
        ltsnpak(ioff+i) = phys_arrays%ltsnrow(i)
        ltwbpak(ioff+i) = phys_arrays%ltwbrow(i)
        lzsnpak(ioff+i) = phys_arrays%lzsnrow(i)
#endif
end do
!
!     * emission fields.
!
do i=il1,il2
  eostpak(ioff+i) = phys_arrays%eostrow(i)
  eostpal(ioff+i) = phys_arrays%eostrol(i)
  !
  escvpak(ioff+i) = phys_arrays%escvrow(i)
  ehcvpak(ioff+i) = phys_arrays%ehcvrow(i)
  esevpak(ioff+i) = phys_arrays%esevrow(i)
  ehevpak(ioff+i) = phys_arrays%ehevrow(i)
end do
#if defined transient_aerosol_emissions
      do l=1,levwf
      do i=il1,il2
        fbbcpak(ioff+i,l) = phys_arrays%fbbcrow(i,l)
        fbbcpal(ioff+i,l) = phys_arrays%fbbcrol(i,l)
      end do
      end do
      do l=1,levair
      do i=il1,il2
        fairpak(ioff+i,l) = phys_arrays%fairrow(i,l)
        fairpal(ioff+i,l) = phys_arrays%fairrol(i,l)
      end do
      end do
#if defined emists
      do i=il1,il2
        sairpak(ioff+i) = phys_arrays%sairrow(i)
        ssfcpak(ioff+i) = phys_arrays%ssfcrow(i)
        sbiopak(ioff+i) = phys_arrays%sbiorow(i)
        sshipak(ioff+i) = phys_arrays%sshirow(i)
        sstkpak(ioff+i) = phys_arrays%sstkrow(i)
        sfirpak(ioff+i) = phys_arrays%sfirrow(i)
        sairpal(ioff+i) = phys_arrays%sairrol(i)
        ssfcpal(ioff+i) = phys_arrays%ssfcrol(i)
        sbiopal(ioff+i) = phys_arrays%sbiorol(i)
        sshipal(ioff+i) = phys_arrays%sshirol(i)
        sstkpal(ioff+i) = phys_arrays%sstkrol(i)
        sfirpal(ioff+i) = phys_arrays%sfirrol(i)
        oairpak(ioff+i) = phys_arrays%oairrow(i)
        osfcpak(ioff+i) = phys_arrays%osfcrow(i)
        obiopak(ioff+i) = phys_arrays%obiorow(i)
        oshipak(ioff+i) = phys_arrays%oshirow(i)
        ostkpak(ioff+i) = phys_arrays%ostkrow(i)
        ofirpak(ioff+i) = phys_arrays%ofirrow(i)
        oairpal(ioff+i) = phys_arrays%oairrol(i)
        osfcpal(ioff+i) = phys_arrays%osfcrol(i)
        obiopal(ioff+i) = phys_arrays%obiorol(i)
        oshipal(ioff+i) = phys_arrays%oshirol(i)
        ostkpal(ioff+i) = phys_arrays%ostkrol(i)
        ofirpal(ioff+i) = phys_arrays%ofirrol(i)
        bairpak(ioff+i) = phys_arrays%bairrow(i)
        bsfcpak(ioff+i) = phys_arrays%bsfcrow(i)
        bbiopak(ioff+i) = phys_arrays%bbiorow(i)
        bshipak(ioff+i) = phys_arrays%bshirow(i)
        bstkpak(ioff+i) = phys_arrays%bstkrow(i)
        bfirpak(ioff+i) = phys_arrays%bfirrow(i)
        bairpal(ioff+i) = phys_arrays%bairrol(i)
        bsfcpal(ioff+i) = phys_arrays%bsfcrol(i)
        bbiopal(ioff+i) = phys_arrays%bbiorol(i)
        bshipal(ioff+i) = phys_arrays%bshirol(i)
        bstkpal(ioff+i) = phys_arrays%bstkrol(i)
        bfirpal(ioff+i) = phys_arrays%bfirrol(i)
      end do
#endif
#endif
#if (defined(pla) && defined(pfrc))
      do i=il1,il2
        bcdpfpak(ioff+i) = phys_arrays%bcdpfrow(i)
        bcdpfpal(ioff+i) = phys_arrays%bcdpfrol(i)
      end do
      do l=1,ilev
      do i=il1,il2
        amldfpak(ioff+i,l) = phys_arrays%amldfrow(i,l)
        amldfpal(ioff+i,l) = phys_arrays%amldfrol(i,l)
        reamfpak(ioff+i,l) = phys_arrays%reamfrow(i,l)
        reamfpal(ioff+i,l) = phys_arrays%reamfrol(i,l)
        veamfpak(ioff+i,l) = phys_arrays%veamfrow(i,l)
        veamfpal(ioff+i,l) = phys_arrays%veamfrol(i,l)
        fr1fpak (ioff+i,l) = phys_arrays%fr1frow (i,l)
        fr1fpal (ioff+i,l) = phys_arrays%fr1frol (i,l)
        fr2fpak (ioff+i,l) = phys_arrays%fr2frow (i,l)
        fr2fpal (ioff+i,l) = phys_arrays%fr2frol (i,l)
        ssldfpak(ioff+i,l) = phys_arrays%ssldfrow(i,l)
        ssldfpal(ioff+i,l) = phys_arrays%ssldfrol(i,l)
        ressfpak(ioff+i,l) = phys_arrays%ressfrow(i,l)
        ressfpal(ioff+i,l) = phys_arrays%ressfrol(i,l)
        vessfpak(ioff+i,l) = phys_arrays%vessfrow(i,l)
        vessfpal(ioff+i,l) = phys_arrays%vessfrol(i,l)
        dsldfpak(ioff+i,l) = phys_arrays%dsldfrow(i,l)
        dsldfpal(ioff+i,l) = phys_arrays%dsldfrol(i,l)
        redsfpak(ioff+i,l) = phys_arrays%redsfrow(i,l)
        redsfpal(ioff+i,l) = phys_arrays%redsfrol(i,l)
        vedsfpak(ioff+i,l) = phys_arrays%vedsfrow(i,l)
        vedsfpal(ioff+i,l) = phys_arrays%vedsfrol(i,l)
        bcldfpak(ioff+i,l) = phys_arrays%bcldfrow(i,l)
        bcldfpal(ioff+i,l) = phys_arrays%bcldfrol(i,l)
        rebcfpak(ioff+i,l) = phys_arrays%rebcfrow(i,l)
        rebcfpal(ioff+i,l) = phys_arrays%rebcfrol(i,l)
        vebcfpak(ioff+i,l) = phys_arrays%vebcfrow(i,l)
        vebcfpal(ioff+i,l) = phys_arrays%vebcfrol(i,l)
        ocldfpak(ioff+i,l) = phys_arrays%ocldfrow(i,l)
        ocldfpal(ioff+i,l) = phys_arrays%ocldfrol(i,l)
        reocfpak(ioff+i,l) = phys_arrays%reocfrow(i,l)
        reocfpal(ioff+i,l) = phys_arrays%reocfrol(i,l)
        veocfpak(ioff+i,l) = phys_arrays%veocfrow(i,l)
        veocfpal(ioff+i,l) = phys_arrays%veocfrol(i,l)
        zcdnfpak(ioff+i,l) = phys_arrays%zcdnfrow(i,l)
        zcdnfpal(ioff+i,l) = phys_arrays%zcdnfrol(i,l)
        bcicfpak(ioff+i,l) = phys_arrays%bcicfrow(i,l)
        bcicfpal(ioff+i,l) = phys_arrays%bcicfrol(i,l)
      end do
      end do
#endif
  if (switch_gas_chem) then
    chdgpak(ioff+il1:ioff+il2,:,:) = phys_arrays%chdgrow(il1:il2,:,:)
    csadpak(ioff+il1:ioff+il2,:,:) = phys_arrays%csadrow(il1:il2,:,:)

    ssadpak(ioff+il1:ioff+il2,:) = phys_arrays%ssadrow(il1:il2,:)
    ssadpal(ioff+il1:ioff+il2,:) = phys_arrays%ssadrol(il1:il2,:)
    wflxpak(ioff+il1:ioff+il2,:) = phys_arrays%wflxrow(il1:il2,:)
    vddppak(ioff+il1:ioff+il2,:) = phys_arrays%vddprow(il1:il2,:)
    sfempak(ioff+il1:ioff+il2,:) = phys_arrays%sfemrow(il1:il2,:)
    sfempal(ioff+il1:ioff+il2,:) = phys_arrays%sfemrol(il1:il2,:)
    acempak(ioff+il1:ioff+il2,:) = phys_arrays%acemrow(il1:il2,:)
    acempal(ioff+il1:ioff+il2,:) = phys_arrays%acemrol(il1:il2,:)
    sfsapak(ioff+il1:ioff+il2,:) = phys_arrays%sfsarow(il1:il2,:)
    sfsapal(ioff+il1:ioff+il2,:) = phys_arrays%sfsarol(il1:il2,:)
    chltpak(ioff+il1:ioff+il2,:) = phys_arrays%chltrow(il1:il2,:)
    chlspak(ioff+il1:ioff+il2,:) = phys_arrays%chlsrow(il1:il2,:)
    cmhfpak(ioff+il1:ioff+il2,:) = phys_arrays%cmhfrow(il1:il2,:)
    clifpak(ioff+il1:ioff+il2,:) = phys_arrays%clifrow(il1:il2,:)

    schmpak(ioff+il1:ioff+il2) = phys_arrays%schmrow(il1:il2)
    vdsfpak(ioff+il1:ioff+il2) = phys_arrays%vdsfrow(il1:il2)
    elnipak(ioff+il1:ioff+il2) = phys_arrays%elnirow(il1:il2)
    elcgpak(ioff+il1:ioff+il2) = phys_arrays%elcgrow(il1:il2)
    elccpak(ioff+il1:ioff+il2) = phys_arrays%elccrow(il1:il2)
    avsppak(ioff+il1:ioff+il2) = phys_arrays%avsprow(il1:il2)
  endif
!
!     * tracer arrays.
!
do n=1,ntrac
  do i=il1,il2
    xfspak (ioff+i,n) = phys_arrays%xfsrow (i,n)
    xsfxpak(ioff+i,n) = phys_arrays%xsfxrow(i,n)
    xsfxpal(ioff+i,n) = phys_arrays%xsfxrol(i,n)
    xsrfpak(ioff+i,n) = phys_arrays%xsrfrow(i,n)
    xsrfpal(ioff+i,n) = phys_arrays%xsrfrol(i,n)

    xtpfpam(ioff+i,n) = phys_arrays%xtpfrom(i,n)
    xtphpam(ioff+i,n) = phys_arrays%xtphrom(i,n)
    xtvipas(ioff+i,n) = phys_arrays%xtviros(i,n)
    xtvipak(ioff+i,n) = phys_arrays%xtvirow(i,n)
    xtvipam(ioff+i,n) = phys_arrays%xtvirom(i,n)
    xtptpam(ioff+i,n) = phys_arrays%xtptrom(i,n)
  end do
  do l=1,ilev
    do i=il1,il2
      xwf0pal(ioff+i,l,n) = phys_arrays%xwf0rol(i,l,n)
      xwfmpal(ioff+i,l,n) = phys_arrays%xwfmrol(i,l,n)
    end do
  end do
end do
!
do l=1,levox
  do i=il1,il2
    h2o2pak(ioff+i,l) = phys_arrays%h2o2row(i,l)
    h2o2pal(ioff+i,l) = phys_arrays%h2o2rol(i,l)
    hno3pak(ioff+i,l) = phys_arrays%hno3row(i,l)
    hno3pal(ioff+i,l) = phys_arrays%hno3rol(i,l)
    nh3pak (ioff+i,l) = phys_arrays%nh3row (i,l)
    nh3pal (ioff+i,l) = phys_arrays%nh3rol (i,l)
    nh4pak (ioff+i,l) = phys_arrays%nh4row (i,l)
    nh4pal (ioff+i,l) = phys_arrays%nh4rol (i,l)
    no3pak (ioff+i,l) = phys_arrays%no3row (i,l)
    no3pal (ioff+i,l) = phys_arrays%no3rol (i,l)
    o3pak  (ioff+i,l) = phys_arrays%o3row  (i,l)
    o3pal  (ioff+i,l) = phys_arrays%o3rol  (i,l)
    ohpak  (ioff+i,l) = phys_arrays%ohrow  (i,l)
    ohpal  (ioff+i,l) = phys_arrays%ohrol  (i,l)
  end do
end do
do l=1,ilev
  do i=il1,il2
    tbndpak(ioff+i,l) = phys_arrays%tbndrow(i,l)
    tbndpal(ioff+i,l) = phys_arrays%tbndrol(i,l)
    ea55pak(ioff+i,l) = phys_arrays%ea55row(i,l)
    ea55pal(ioff+i,l) = phys_arrays%ea55rol(i,l)
    rkmpak (ioff+i,l) = phys_arrays%rkmrow (i,l)
    rkhpak (ioff+i,l) = phys_arrays%rkhrow (i,l)
    rkqpak (ioff+i,l) = phys_arrays%rkqrow (i,l)
  end do
end do
!
do i=il1,il2
  spotpak(ioff+i) = phys_arrays%spotrow(i)
  st01pak(ioff+i) = phys_arrays%st01row(i)
  st02pak(ioff+i) = phys_arrays%st02row(i)
  st03pak(ioff+i) = phys_arrays%st03row(i)
  st04pak(ioff+i) = phys_arrays%st04row(i)
  st06pak(ioff+i) = phys_arrays%st06row(i)
  st13pak(ioff+i) = phys_arrays%st13row(i)
  st14pak(ioff+i) = phys_arrays%st14row(i)
  st15pak(ioff+i) = phys_arrays%st15row(i)
  st16pak(ioff+i) = phys_arrays%st16row(i)
  st17pak(ioff+i) = phys_arrays%st17row(i)
  suz0pak(ioff+i) = phys_arrays%suz0row(i)
  suz0pal(ioff+i) = phys_arrays%suz0rol(i)
  pdsfpak(ioff+i) = phys_arrays%pdsfrow(i)
  pdsfpal(ioff+i) = phys_arrays%pdsfrol(i)
end do
!
!     * conditional arrays.
!
#if defined rad_flux_profs
      do l = 1, ilev+2
         do i = il1, il2
            fsaupak(ioff+i,l) = phys_arrays%fsaurow(i,l)
            fsadpak(ioff+i,l) = phys_arrays%fsadrow(i,l)
            flaupak(ioff+i,l) = phys_arrays%flaurow(i,l)
            fladpak(ioff+i,l) = phys_arrays%fladrow(i,l)
            fscupak(ioff+i,l) = phys_arrays%fscurow(i,l)
            fscdpak(ioff+i,l) = phys_arrays%fscdrow(i,l)
            flcupak(ioff+i,l) = phys_arrays%flcurow(i,l)
            flcdpak(ioff+i,l) = phys_arrays%flcdrow(i,l)
         end do ! i
      end do ! l
#endif
do l = 1, ilev+2
  do i = il1, il2
    fsaupal(ioff+i,l) = phys_arrays%fsaurol(i,l)
    fsadpal(ioff+i,l) = phys_arrays%fsadrol(i,l)
    flaupal(ioff+i,l) = phys_arrays%flaurol(i,l)
    fladpal(ioff+i,l) = phys_arrays%fladrol(i,l)
    fscupal(ioff+i,l) = phys_arrays%fscurol(i,l)
    fscdpal(ioff+i,l) = phys_arrays%fscdrol(i,l)
    flcupal(ioff+i,l) = phys_arrays%flcurol(i,l)
    flcdpal(ioff+i,l) = phys_arrays%flcdrol(i,l)
  end do ! i
end do ! l


#if defined radforce

      !--- upward and downward all-sky and clear-sky fluxes
      !--- fl* thermal, fs* solar

      fsaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fsaurow_r (il1:il2,1:ilev+2,1:nrfp)
      fsadpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fsadrow_r (il1:il2,1:ilev+2,1:nrfp)
      flaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flaurow_r (il1:il2,1:ilev+2,1:nrfp)
      fladpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fladrow_r (il1:il2,1:ilev+2,1:nrfp)
      fscupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fscurow_r (il1:il2,1:ilev+2,1:nrfp)
      fscdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fscdrow_r (il1:il2,1:ilev+2,1:nrfp)
      flcupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flcurow_r (il1:il2,1:ilev+2,1:nrfp)
      flcdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flcdrow_r (il1:il2,1:ilev+2,1:nrfp)

      fsaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fsaurol_r (il1:il2,1:ilev+2,1:nrfp)
      fsadpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fsadrol_r (il1:il2,1:ilev+2,1:nrfp)
      flaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flaurol_r (il1:il2,1:ilev+2,1:nrfp)
      fladpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fladrol_r (il1:il2,1:ilev+2,1:nrfp)
      fscupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fscurol_r (il1:il2,1:ilev+2,1:nrfp)
      fscdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%fscdrol_r (il1:il2,1:ilev+2,1:nrfp)
      flcupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flcurol_r (il1:il2,1:ilev+2,1:nrfp)
      flcdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          phys_arrays%flcdrol_r (il1:il2,1:ilev+2,1:nrfp)

      !--- temperature perturbation (t_* - t_o) for each perturbation
      rdtpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%rdtrow_r (il1:il2,1:ilev,1:nrfp)
      rdtpal_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%rdtrol_r (il1:il2,1:ilev,1:nrfp)
      rdtpam_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%rdtrom_r (il1:il2,1:ilev,1:nrfp)

      !--- heating rates for each perturbation
      hrspak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%hrsrow_r (il1:il2,1:ilev,1:nrfp)
      hrlpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%hrlrow_r (il1:il2,1:ilev,1:nrfp)

      hrscpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%hrscrow_r (il1:il2,1:ilev,1:nrfp)
      hrlcpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           phys_arrays%hrlcrow_r (il1:il2,1:ilev,1:nrfp)

      !--- surface fluxes from secondary radiation calls
      csbpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%csbrol_r(il1:il2,1:nrfp)
      fsgpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%fsgrol_r(il1:il2,1:nrfp)
      fsspal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%fssrol_r(il1:il2,1:nrfp)
      fsscpal_r(ioff+il1:ioff+il2,1:nrfp) = phys_arrays%fsscrol_r(il1:il2,1:nrfp)
      clbpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%clbrol_r(il1:il2,1:nrfp)
      flgpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%flgrol_r(il1:il2,1:nrfp)
      fdlpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%fdlrol_r(il1:il2,1:nrfp)
      fdlcpal_r(ioff+il1:ioff+il2,1:nrfp) = phys_arrays%fdlcrol_r(il1:il2,1:nrfp)
      fsrpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%fsrrol_r(il1:il2,1:nrfp)
      fsrcpal_r(ioff+il1:ioff+il2,1:nrfp) = phys_arrays%fsrcrol_r(il1:il2,1:nrfp)
      fsopal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%fsorol_r(il1:il2,1:nrfp)
      olrpal_r(ioff+il1:ioff+il2,1:nrfp)  = phys_arrays%olrrol_r(il1:il2,1:nrfp)
      olrcpal_r(ioff+il1:ioff+il2,1:nrfp) = phys_arrays%olrcrol_r(il1:il2,1:nrfp)
      fslopal_r(ioff+il1:ioff+il2,1:nrfp) = phys_arrays%fslorol_r(il1:il2,1:nrfp)

      do m = 1, im
       csbpat_r(ioff+il1:ioff+il2,m,1:nrfp) = phys_arrays%csbrot_r(il1:il2,m,1:nrfp)
       clbpat_r(ioff+il1:ioff+il2,m,1:nrfp) = phys_arrays%clbrot_r(il1:il2,m,1:nrfp)
       fsgpat_r(ioff+il1:ioff+il2,m,1:nrfp) = phys_arrays%fsgrot_r(il1:il2,m,1:nrfp)
       flgpat_r(ioff+il1:ioff+il2,m,1:nrfp) = phys_arrays%flgrot_r(il1:il2,m,1:nrfp)
      end do ! m

      !--- vertical level index of tropopause height
      kthpal(ioff+il1:ioff+il2) = phys_arrays%kthrol(il1:il2)
#endif
#if defined qconsav
      do i=il1,il2
          qaddpak(ioff+i,1) = phys_arrays%qaddrow(i,1)
          qaddpak(ioff+i,2) = phys_arrays%qaddrow(i,2)
          qtphpak(ioff+i,1) = phys_arrays%qtphrow(i,1)
          qtphpak(ioff+i,2) = phys_arrays%qtphrow(i,2)
      end do
#endif
!
do l=1,ilev
  do i=il1,il2
#if defined tprhs
          ttppak (ioff+i,l) = phys_arrays%ttprow (i,l)
#endif
#if defined qprhs
          qtppak (ioff+i,l) = phys_arrays%qtprow (i,l)
#endif
#if defined uprhs
          utppak (ioff+i,l) = phys_arrays%utprow (i,l)
#endif
#if defined vprhs
          vtppak (ioff+i,l) = phys_arrays%vtprow (i,l)
#endif
#if defined qconsav
          qtpfpak(ioff+i,l) = phys_arrays%qtpfrow(i,l)
#endif
#if defined tprhsc
          ttpcpak(ioff+i,l) = phys_arrays%ttpcrow(i,l)
          ttpkpak(ioff+i,l) = phys_arrays%ttpkrow(i,l)
          ttpmpak(ioff+i,l) = phys_arrays%ttpmrow(i,l)
          ttpnpak(ioff+i,l) = phys_arrays%ttpnrow(i,l)
          ttpppak(ioff+i,l) = phys_arrays%ttpprow(i,l)
          ttpvpak(ioff+i,l) = phys_arrays%ttpvrow(i,l)
#endif
#if (defined (tprhsc) || defined (radforce))
          ttplpak(ioff+i,l) = phys_arrays%ttplrow(i,l)
          ttpspak(ioff+i,l) = phys_arrays%ttpsrow(i,l)
          ttscpak(ioff+i,l) = phys_arrays%ttscrow(i,l)
          ttlcpak(ioff+i,l) = phys_arrays%ttlcrow(i,l)
#endif
#if defined qprhsc
          qtpcpak(ioff+i,l) = phys_arrays%qtpcrow(i,l)
          qtpmpak(ioff+i,l) = phys_arrays%qtpmrow(i,l)
          qtpppak(ioff+i,l) = phys_arrays%qtpprow(i,l)
          qtpvpak(ioff+i,l) = phys_arrays%qtpvrow(i,l)
#endif
#if defined uprhsc
          utpcpak(ioff+i,l) = phys_arrays%utpcrow(i,l)
          utpgpak(ioff+i,l) = phys_arrays%utpgrow(i,l)
          utpnpak(ioff+i,l) = phys_arrays%utpnrow(i,l)
          utpspak(ioff+i,l) = phys_arrays%utpsrow(i,l)
          utpvpak(ioff+i,l) = phys_arrays%utpvrow(i,l)
#endif
#if defined vprhsc
          vtpcpak(ioff+i,l) = phys_arrays%vtpcrow(i,l)
          vtpgpak(ioff+i,l) = phys_arrays%vtpgrow(i,l)
          vtpnpak(ioff+i,l) = phys_arrays%vtpnrow(i,l)
          vtpspak(ioff+i,l) = phys_arrays%vtpsrow(i,l)
          vtpvpak(ioff+i,l) = phys_arrays%vtpvrow(i,l)
#endif
  end do
end do
!
do n=1,ntrac
#if defined xconsav
        do i=il1,il2
            xaddpak(ioff+i,1,n) = phys_arrays%xaddrow(i,1,n)
            xaddpak(ioff+i,2,n) = phys_arrays%xaddrow(i,2,n)
            xtphpak(ioff+i,1,n) = phys_arrays%xtphrow(i,1,n)
            xtphpak(ioff+i,2,n) = phys_arrays%xtphrow(i,2,n)
        end do
#endif
  do l=1,ilev
    do i=il1,il2
#if defined xprhs
            xtppak (ioff+i,l,n) = phys_arrays%xtprow (i,l,n)
#endif
#if defined xconsav
            xtpfpak(ioff+i,l,n) = phys_arrays%xtpfrow(i,l,n)
#endif
#if defined xprhsc
            xtpcpak(ioff+i,l,n) = phys_arrays%xtpcrow(i,l,n)
            xtpmpak(ioff+i,l,n) = phys_arrays%xtpmrow(i,l,n)
            xtpppak(ioff+i,l,n) = phys_arrays%xtpprow(i,l,n)
            xtpvpak(ioff+i,l,n) = phys_arrays%xtpvrow(i,l,n)
#endif
#if defined x01
            x01pak (ioff+i,l,n) = phys_arrays%x01row (i,l,n)
#endif
#if defined x02
            x02pak (ioff+i,l,n) = phys_arrays%x02row (i,l,n)
#endif
#if defined x03
            x03pak (ioff+i,l,n) = phys_arrays%x03row (i,l,n)
#endif
#if defined x04
            x04pak (ioff+i,l,n) = phys_arrays%x04row (i,l,n)
#endif
#if defined x05
            x05pak (ioff+i,l,n) = phys_arrays%x05row (i,l,n)
#endif
#if defined x06
            x06pak (ioff+i,l,n) = phys_arrays%x06row (i,l,n)
#endif
#if defined x07
            x07pak (ioff+i,l,n) = phys_arrays%x07row (i,l,n)
#endif
#if defined x08
            x08pak (ioff+i,l,n) = phys_arrays%x08row (i,l,n)
#endif
#if defined x09
            x09pak (ioff+i,l,n) = phys_arrays%x09row (i,l,n)
#endif
    end do
  end do
end do
#if defined xtraconv
      do i=il1,il2
        bcdpak (ioff+i) = phys_arrays%bcdrow (i)
        bcspak (ioff+i) = phys_arrays%bcsrow (i)
        capepak(ioff+i) = phys_arrays%caperow(i)
        cdcbpak(ioff+i) = phys_arrays%cdcbrow(i)
        cinhpak(ioff+i) = phys_arrays%cinhrow(i)
        cscbpak(ioff+i) = phys_arrays%cscbrow(i)
        tcdpak (ioff+i) = phys_arrays%tcdrow (i)
        tcspak (ioff+i) = phys_arrays%tcsrow (i)
      end do
!
      do l=1,ilev
      do i=il1,il2
          dmcpak (ioff+i,l) = phys_arrays%dmcrow (i,l)
          smcpak (ioff+i,l) = phys_arrays%smcrow (i,l)
          dmcdpak(ioff+i,l) = phys_arrays%dmcdrow(i,l)
          dmcupak(ioff+i,l) = phys_arrays%dmcurow(i,l)

      end do
      end do
#endif
#if defined xtrachem
      do i=il1,il2
        dd4pak (ioff+i) = phys_arrays%dd4row (i)
        dox4pak(ioff+i) = phys_arrays%dox4row(i)
        doxdpak(ioff+i) = phys_arrays%doxdrow(i)
        esdpak (ioff+i) = phys_arrays%esdrow (i)
        esfspak(ioff+i) = phys_arrays%esfsrow(i)
        eaispak(ioff+i) = phys_arrays%eaisrow(i)
        estspak(ioff+i) = phys_arrays%estsrow(i)
        efispak(ioff+i) = phys_arrays%efisrow(i)
        esfbpak(ioff+i) = phys_arrays%esfbrow(i)
        eaibpak(ioff+i) = phys_arrays%eaibrow(i)
        estbpak(ioff+i) = phys_arrays%estbrow(i)
        efibpak(ioff+i) = phys_arrays%efibrow(i)
        esfopak(ioff+i) = phys_arrays%esforow(i)
        eaiopak(ioff+i) = phys_arrays%eaiorow(i)
        estopak(ioff+i) = phys_arrays%estorow(i)
        efiopak(ioff+i) = phys_arrays%efiorow(i)
        edslpak(ioff+i) = phys_arrays%edslrow(i)
        edsopak(ioff+i) = phys_arrays%edsorow(i)
        esvcpak(ioff+i) = phys_arrays%esvcrow(i)
        esvepak(ioff+i) = phys_arrays%esverow(i)
        noxdpak(ioff+i) = phys_arrays%noxdrow(i)
        wdd4pak(ioff+i) = phys_arrays%wdd4row(i)
        wdl4pak(ioff+i) = phys_arrays%wdl4row(i)
        wds4pak(ioff+i) = phys_arrays%wds4row(i)
      end do
#ifndef pla
      do i=il1,il2
        dddpak (ioff+i) = phys_arrays%dddrow (i)
        ddbpak (ioff+i) = phys_arrays%ddbrow (i)
        ddopak (ioff+i) = phys_arrays%ddorow (i)
        ddspak (ioff+i) = phys_arrays%ddsrow (i)
        wdldpak(ioff+i) = phys_arrays%wdldrow(i)
        wdlbpak(ioff+i) = phys_arrays%wdlbrow(i)
        wdlopak(ioff+i) = phys_arrays%wdlorow(i)
        wdlspak(ioff+i) = phys_arrays%wdlsrow(i)
        wdddpak(ioff+i) = phys_arrays%wdddrow(i)
        wddbpak(ioff+i) = phys_arrays%wddbrow(i)
        wddopak(ioff+i) = phys_arrays%wddorow(i)
        wddspak(ioff+i) = phys_arrays%wddsrow(i)
        wdsdpak(ioff+i) = phys_arrays%wdsdrow(i)
        wdsbpak(ioff+i) = phys_arrays%wdsbrow(i)
        wdsopak(ioff+i) = phys_arrays%wdsorow(i)
        wdsspak(ioff+i) = phys_arrays%wdssrow(i)
        dd6pak (ioff+i) = phys_arrays%dd6row (i)
        sdhppak(ioff+i) = phys_arrays%sdhprow(i)
        sdo3pak(ioff+i) = phys_arrays%sdo3row(i)
        slhppak(ioff+i) = phys_arrays%slhprow(i)
        slo3pak(ioff+i) = phys_arrays%slo3row(i)
        sshppak(ioff+i) = phys_arrays%sshprow(i)
        sso3pak(ioff+i) = phys_arrays%sso3row(i)
        wdd6pak(ioff+i) = phys_arrays%wdd6row(i)
        wdl6pak(ioff+i) = phys_arrays%wdl6row(i)
        wds6pak(ioff+i) = phys_arrays%wds6row(i)
      end do
!
      do l=1,ilev
        do i=il1,il2
          phdpak (ioff+i,l) = phys_arrays%phdrow (i,l)
          phlpak (ioff+i,l) = phys_arrays%phlrow (i,l)
          phspak (ioff+i,l) = phys_arrays%phsrow (i,l)
        end do
      end do
#endif
#endif
#if defined (xtradust)
      do i=il1,il2
        duwdpak(ioff+i) = phys_arrays%duwdrow(i)
        dustpak(ioff+i) = phys_arrays%dustrow(i)
        duthpak(ioff+i) = phys_arrays%duthrow(i)
        usmkpak(ioff+i) = phys_arrays%usmkrow(i)
        fallpak(ioff+i) = phys_arrays%fallrow(i)
        fa10pak(ioff+i) = phys_arrays%fa10row(i)
         fa2pak(ioff+i) =  phys_arrays%fa2row(i)
         fa1pak(ioff+i) =  phys_arrays%fa1row(i)
        gustpak(ioff+i) = phys_arrays%gustrow(i)
        zspdpak(ioff+i) = phys_arrays%zspdrow(i)
        vgfrpak(ioff+i) = phys_arrays%vgfrrow(i)
        smfrpak(ioff+i) = phys_arrays%smfrrow(i)
      end do
#endif
#if (defined(pla) && defined(pam))
      do l=1,ilev
      do i=il1,il2
        ssldpak(ioff+i,l)   = phys_arrays%ssldrow(i,l)
        resspak(ioff+i,l)   = phys_arrays%ressrow(i,l)
        vesspak(ioff+i,l)   = phys_arrays%vessrow(i,l)
        dsldpak(ioff+i,l)   = phys_arrays%dsldrow(i,l)
        redspak(ioff+i,l)   = phys_arrays%redsrow(i,l)
        vedspak(ioff+i,l)   = phys_arrays%vedsrow(i,l)
        bcldpak(ioff+i,l)   = phys_arrays%bcldrow(i,l)
        rebcpak(ioff+i,l)   = phys_arrays%rebcrow(i,l)
        vebcpak(ioff+i,l)   = phys_arrays%vebcrow(i,l)
        ocldpak(ioff+i,l)   = phys_arrays%ocldrow(i,l)
        reocpak(ioff+i,l)   = phys_arrays%reocrow(i,l)
        veocpak(ioff+i,l)   = phys_arrays%veocrow(i,l)
        amldpak(ioff+i,l)   = phys_arrays%amldrow(i,l)
        reampak(ioff+i,l)   = phys_arrays%reamrow(i,l)
        veampak(ioff+i,l)   = phys_arrays%veamrow(i,l)
        fr1pak (ioff+i,l)   = phys_arrays%fr1row (i,l)
        fr2pak (ioff+i,l)   = phys_arrays%fr2row (i,l)
        zcdnpak(ioff+i,l)   = phys_arrays%zcdnrow(i,l)
        bcicpak(ioff+i,l)   = phys_arrays%bcicrow(i,l)
        oednpak(ioff+i,l,:) = phys_arrays%oednrow(i,l,:)
        oercpak(ioff+i,l,:) = phys_arrays%oercrow(i,l,:)
        oidnpak(ioff+i,l)   = phys_arrays%oidnrow(i,l)
        oircpak(ioff+i,l)   = phys_arrays%oircrow(i,l)
        svvbpak(ioff+i,l)   = phys_arrays%svvbrow(i,l)
        psvvpak(ioff+i,l)   = phys_arrays%psvvrow(i,l)
        svmbpak(ioff+i,l,:) = phys_arrays%svmbrow(i,l,:)
        svcbpak(ioff+i,l,:) = phys_arrays%svcbrow(i,l,:)
        pnvbpak(ioff+i,l,:)  =phys_arrays%pnvbrow(i,l,:)
        pnmbpak(ioff+i,l,:,:)=phys_arrays%pnmbrow(i,l,:,:)
        pncbpak(ioff+i,l,:,:)=phys_arrays%pncbrow(i,l,:,:)
        psvbpak(ioff+i,l,:,:)  =phys_arrays%psvbrow(i,l,:,:)
        psmbpak(ioff+i,l,:,:,:)=phys_arrays%psmbrow(i,l,:,:,:)
        pscbpak(ioff+i,l,:,:,:)=phys_arrays%pscbrow(i,l,:,:,:)
        qnvbpak(ioff+i,l,:)  =phys_arrays%qnvbrow(i,l,:)
        qnmbpak(ioff+i,l,:,:)=phys_arrays%qnmbrow(i,l,:,:)
        qncbpak(ioff+i,l,:,:)=phys_arrays%qncbrow(i,l,:,:)
        qsvbpak(ioff+i,l,:,:)  =phys_arrays%qsvbrow(i,l,:,:)
        qsmbpak(ioff+i,l,:,:,:)=phys_arrays%qsmbrow(i,l,:,:,:)
        qscbpak(ioff+i,l,:,:,:)=phys_arrays%qscbrow(i,l,:,:,:)
        qgvbpak(ioff+i,l)   = phys_arrays%qgvbrow(i,l)
        qgmbpak(ioff+i,l,:) = phys_arrays%qgmbrow(i,l,:)
        qgcbpak(ioff+i,l,:) = phys_arrays%qgcbrow(i,l,:)
        qdvbpak(ioff+i,l)   = phys_arrays%qdvbrow(i,l)
        qdmbpak(ioff+i,l,:) = phys_arrays%qdmbrow(i,l,:)
        qdcbpak(ioff+i,l,:) = phys_arrays%qdcbrow(i,l,:)
        onvbpak(ioff+i,l,:)  =phys_arrays%onvbrow(i,l,:)
        onmbpak(ioff+i,l,:,:)=phys_arrays%onmbrow(i,l,:,:)
        oncbpak(ioff+i,l,:,:)=phys_arrays%oncbrow(i,l,:,:)
        osvbpak(ioff+i,l,:,:)  =phys_arrays%osvbrow(i,l,:,:)
        osmbpak(ioff+i,l,:,:,:)=phys_arrays%osmbrow(i,l,:,:,:)
        oscbpak(ioff+i,l,:,:,:)=phys_arrays%oscbrow(i,l,:,:,:)
        ogvbpak(ioff+i,l)   = phys_arrays%ogvbrow(i,l)
        ogmbpak(ioff+i,l,:) = phys_arrays%ogmbrow(i,l,:)
        ogcbpak(ioff+i,l,:) = phys_arrays%ogcbrow(i,l,:)
        devbpak(ioff+i,l,:) = phys_arrays%devbrow(i,l,:)
        pdevpak(ioff+i,l,:) = phys_arrays%pdevrow(i,l,:)
        dembpak(ioff+i,l,:,:)=phys_arrays%dembrow(i,l,:,:)
        decbpak(ioff+i,l,:,:)=phys_arrays%decbrow(i,l,:,:)
        divbpak(ioff+i,l)   = phys_arrays%divbrow(i,l)
        pdivpak(ioff+i,l)   = phys_arrays%pdivrow(i,l)
        dimbpak(ioff+i,l,:) = phys_arrays%dimbrow(i,l,:)
        dicbpak(ioff+i,l,:) = phys_arrays%dicbrow(i,l,:)
        revbpak(ioff+i,l,:) = phys_arrays%revbrow(i,l,:)
        prevpak(ioff+i,l,:) = phys_arrays%prevrow(i,l,:)
        rembpak(ioff+i,l,:,:)=phys_arrays%rembrow(i,l,:,:)
        recbpak(ioff+i,l,:,:)=phys_arrays%recbrow(i,l,:,:)
        rivbpak(ioff+i,l)   = phys_arrays%rivbrow(i,l)
        privpak(ioff+i,l)   = phys_arrays%privrow(i,l)
        rimbpak(ioff+i,l,:) = phys_arrays%rimbrow(i,l,:)
        ricbpak(ioff+i,l,:) = phys_arrays%ricbrow(i,l,:)
        sulipak(ioff+i,l)  = phys_arrays%sulirow(i,l)
        rsuipak(ioff+i,l)  = phys_arrays%rsuirow(i,l)
        vsuipak(ioff+i,l)  = phys_arrays%vsuirow(i,l)
        f1supak(ioff+i,l)  = phys_arrays%f1surow(i,l)
        f2supak(ioff+i,l)  = phys_arrays%f2surow(i,l)
        bclipak(ioff+i,l)  = phys_arrays%bclirow(i,l)
        rbcipak(ioff+i,l)  = phys_arrays%rbcirow(i,l)
        vbcipak(ioff+i,l)  = phys_arrays%vbcirow(i,l)
        f1bcpak(ioff+i,l)  = phys_arrays%f1bcrow(i,l)
        f2bcpak(ioff+i,l)  = phys_arrays%f2bcrow(i,l)
        oclipak(ioff+i,l)  = phys_arrays%oclirow(i,l)
        rocipak(ioff+i,l)  = phys_arrays%rocirow(i,l)
        vocipak(ioff+i,l)  = phys_arrays%vocirow(i,l)
        f1ocpak(ioff+i,l)  = phys_arrays%f1ocrow(i,l)
        f2ocpak(ioff+i,l)  = phys_arrays%f2ocrow(i,l)
      end do
      end do
#if defined (xtrapla1)
      do l=1,ilev
      do i=il1,il2
        sncnpak(ioff+i,l) = phys_arrays%sncnrow(i,l)
        ssunpak(ioff+i,l) = phys_arrays%ssunrow(i,l)
        scndpak(ioff+i,l) = phys_arrays%scndrow(i,l)
        sgsppak(ioff+i,l) = phys_arrays%sgsprow(i,l)
        ccnpak (ioff+i,l) = phys_arrays%ccnrow (i,l)
        cc02pak(ioff+i,l) = phys_arrays%cc02row(i,l)
        cc04pak(ioff+i,l) = phys_arrays%cc04row(i,l)
        cc08pak(ioff+i,l) = phys_arrays%cc08row(i,l)
        cc16pak(ioff+i,l) = phys_arrays%cc16row(i,l)
        ccnepak(ioff+i,l) = phys_arrays%ccnerow(i,l)
        acaspak(ioff+i,l) = phys_arrays%acasrow(i,l)
        acoapak(ioff+i,l) = phys_arrays%acoarow(i,l)
        acbcpak(ioff+i,l) = phys_arrays%acbcrow(i,l)
        acsspak(ioff+i,l) = phys_arrays%acssrow(i,l)
        acmdpak(ioff+i,l) = phys_arrays%acmdrow(i,l)
        ntpak  (ioff+i,l) = phys_arrays%ntrow(i,l)
        n20pak (ioff+i,l) = phys_arrays%n20row(i,l)
        n50pak (ioff+i,l) = phys_arrays%n50row(i,l)
        n100pak(ioff+i,l) = phys_arrays%n100row(i,l)
        n200pak(ioff+i,l) = phys_arrays%n200row(i,l)
        wtpak  (ioff+i,l) = phys_arrays%wtrow(i,l)
        w20pak (ioff+i,l) = phys_arrays%w20row(i,l)
        w50pak (ioff+i,l) = phys_arrays%w50row(i,l)
        w100pak(ioff+i,l) = phys_arrays%w100row(i,l)
        w200pak(ioff+i,l) = phys_arrays%w200row(i,l)
        rcripak(ioff+i,l) = phys_arrays%rcrirow(i,l)
        supspak(ioff+i,l) = phys_arrays%supsrow(i,l)
        henrpak(ioff+i,l) = phys_arrays%henrrow(i,l)
        o3frpak(ioff+i,l) = phys_arrays%o3frrow(i,l)
        h2o2frpak(ioff+i,l) = phys_arrays%h2o2frrow(i,l)
        wparpak(ioff+i,l) = phys_arrays%wparrow(i,l)
        pm25pak(ioff+i,l) = phys_arrays%pm25row(i,l)
        pm10pak(ioff+i,l) = phys_arrays%pm10row(i,l)
        dm25pak(ioff+i,l) = phys_arrays%dm25row(i,l)
        dm10pak(ioff+i,l) = phys_arrays%dm10row(i,l)
      end do
      end do
      do i=il1,il2
        vncnpak(ioff+i) = phys_arrays%vncnrow(i)
        vasnpak(ioff+i) = phys_arrays%vasnrow(i)
        vscdpak(ioff+i) = phys_arrays%vscdrow(i)
        vgsppak(ioff+i) = phys_arrays%vgsprow(i)
        voaepak(ioff+i) = phys_arrays%voaerow(i)
        vbcepak(ioff+i) = phys_arrays%vbcerow(i)
        vasepak(ioff+i) = phys_arrays%vaserow(i)
        vmdepak(ioff+i) = phys_arrays%vmderow(i)
        vssepak(ioff+i) = phys_arrays%vsserow(i)
        voawpak(ioff+i) = phys_arrays%voawrow(i)
        vbcwpak(ioff+i) = phys_arrays%vbcwrow(i)
        vaswpak(ioff+i) = phys_arrays%vaswrow(i)
        vmdwpak(ioff+i) = phys_arrays%vmdwrow(i)
        vsswpak(ioff+i) = phys_arrays%vsswrow(i)
        voadpak(ioff+i) = phys_arrays%voadrow(i)
        vbcdpak(ioff+i) = phys_arrays%vbcdrow(i)
        vasdpak(ioff+i) = phys_arrays%vasdrow(i)
        vmddpak(ioff+i) = phys_arrays%vmddrow(i)
        vssdpak(ioff+i) = phys_arrays%vssdrow(i)
        voagpak(ioff+i) = phys_arrays%voagrow(i)
        vbcgpak(ioff+i) = phys_arrays%vbcgrow(i)
        vasgpak(ioff+i) = phys_arrays%vasgrow(i)
        vmdgpak(ioff+i) = phys_arrays%vmdgrow(i)
        vssgpak(ioff+i) = phys_arrays%vssgrow(i)
        voacpak(ioff+i) = phys_arrays%voacrow(i)
        vbccpak(ioff+i) = phys_arrays%vbccrow(i)
        vascpak(ioff+i) = phys_arrays%vascrow(i)
        vmdcpak(ioff+i) = phys_arrays%vmdcrow(i)
        vsscpak(ioff+i) = phys_arrays%vsscrow(i)
        vasipak(ioff+i) = phys_arrays%vasirow(i)
        vas1pak(ioff+i) = phys_arrays%vas1row(i)
        vas2pak(ioff+i) = phys_arrays%vas2row(i)
        vas3pak(ioff+i) = phys_arrays%vas3row(i)
        vccnpak(ioff+i) = phys_arrays%vccnrow(i)
        vcnepak(ioff+i) = phys_arrays%vcnerow(i)
      end do
#endif
#if defined (xtrapla2)
      do l=1,ilev
      do i=il1,il2
        cornpak(ioff+i,l) = phys_arrays%cornrow(i,l)
        cormpak(ioff+i,l) = phys_arrays%cormrow(i,l)
        rsn1pak(ioff+i,l) = phys_arrays%rsn1row(i,l)
        rsm1pak(ioff+i,l) = phys_arrays%rsm1row(i,l)
        rsn2pak(ioff+i,l) = phys_arrays%rsn2row(i,l)
        rsm2pak(ioff+i,l) = phys_arrays%rsm2row(i,l)
        rsn3pak(ioff+i,l) = phys_arrays%rsn3row(i,l)
        rsm3pak(ioff+i,l) = phys_arrays%rsm3row(i,l)
        do isf=1,isdnum
          sdnupak(ioff+i,l,isf) = phys_arrays%sdnurow(i,l,isf)
          sdmapak(ioff+i,l,isf) = phys_arrays%sdmarow(i,l,isf)
          sdacpak(ioff+i,l,isf) = phys_arrays%sdacrow(i,l,isf)
          sdcopak(ioff+i,l,isf) = phys_arrays%sdcorow(i,l,isf)
          sssnpak(ioff+i,l,isf) = phys_arrays%sssnrow(i,l,isf)
          smdnpak(ioff+i,l,isf) = phys_arrays%smdnrow(i,l,isf)
          sianpak(ioff+i,l,isf) = phys_arrays%sianrow(i,l,isf)
          sssmpak(ioff+i,l,isf) = phys_arrays%sssmrow(i,l,isf)
          smdmpak(ioff+i,l,isf) = phys_arrays%smdmrow(i,l,isf)
          sewmpak(ioff+i,l,isf) = phys_arrays%sewmrow(i,l,isf)
          ssumpak(ioff+i,l,isf) = phys_arrays%ssumrow(i,l,isf)
          socmpak(ioff+i,l,isf) = phys_arrays%socmrow(i,l,isf)
          sbcmpak(ioff+i,l,isf) = phys_arrays%sbcmrow(i,l,isf)
          siwmpak(ioff+i,l,isf) = phys_arrays%siwmrow(i,l,isf)
        end do
      end do
      end do
      do i=il1,il2
        do isf=1,isdiag
          sdvlpak(ioff+i,isf) = phys_arrays%sdvlrow(i,isf)
        end do
        vrn1pak(ioff+i) = phys_arrays%vrn1row(i)
        vrm1pak(ioff+i) = phys_arrays%vrm1row(i)
        vrn2pak(ioff+i) = phys_arrays%vrn2row(i)
        vrm2pak(ioff+i) = phys_arrays%vrm2row(i)
        vrn3pak(ioff+i) = phys_arrays%vrn3row(i)
        vrm3pak(ioff+i) = phys_arrays%vrm3row(i)
      end do
      do i=il1,il2
        defapak(ioff+i) = phys_arrays%defarow(i)
        defcpak(ioff+i) = phys_arrays%defcrow(i)
        do isf=1,isdust
          dmacpak(ioff+i,isf) = phys_arrays%dmacrow(i,isf)
          dmcopak(ioff+i,isf) = phys_arrays%dmcorow(i,isf)
          dnacpak(ioff+i,isf) = phys_arrays%dnacrow(i,isf)
          dncopak(ioff+i,isf) = phys_arrays%dncorow(i,isf)
        end do
        do isf=1,isdiag
          defxpak(ioff+i,isf) = phys_arrays%defxrow(i,isf)
          defnpak(ioff+i,isf) = phys_arrays%defnrow(i,isf)
        end do
      end do
      do i=il1,il2
        tnsspak(ioff+i) = phys_arrays%tnssrow(i)
        tnmdpak(ioff+i) = phys_arrays%tnmdrow(i)
        tniapak(ioff+i) = phys_arrays%tniarow(i)
        tmsspak(ioff+i) = phys_arrays%tmssrow(i)
        tmmdpak(ioff+i) = phys_arrays%tmmdrow(i)
        tmocpak(ioff+i) = phys_arrays%tmocrow(i)
        tmbcpak(ioff+i) = phys_arrays%tmbcrow(i)
        tmsppak(ioff+i) = phys_arrays%tmsprow(i)
        do n=1,isdnum
          snsspak(ioff+i,n) = phys_arrays%snssrow(i,n)
          snmdpak(ioff+i,n) = phys_arrays%snmdrow(i,n)
          sniapak(ioff+i,n) = phys_arrays%sniarow(i,n)
          smsspak(ioff+i,n) = phys_arrays%smssrow(i,n)
          smmdpak(ioff+i,n) = phys_arrays%smmdrow(i,n)
          smocpak(ioff+i,n) = phys_arrays%smocrow(i,n)
          smbcpak(ioff+i,n) = phys_arrays%smbcrow(i,n)
          smsppak(ioff+i,n) = phys_arrays%smsprow(i,n)
          siwhpak(ioff+i,n) = phys_arrays%siwhrow(i,n)
          sewhpak(ioff+i,n) = phys_arrays%sewhrow(i,n)
        end do
      end do
#endif
#endif
#if defined xtrals
      do l=1,ilev
      do i=il1,il2
        aggpak (ioff+i,l) = phys_arrays%aggrow (i,l)
        autpak (ioff+i,l) = phys_arrays%autrow (i,l)
        cndpak (ioff+i,l) = phys_arrays%cndrow (i,l)
        deppak (ioff+i,l) = phys_arrays%deprow (i,l)
        evppak (ioff+i,l) = phys_arrays%evprow (i,l)
        frhpak (ioff+i,l) = phys_arrays%frhrow (i,l)
        frkpak (ioff+i,l) = phys_arrays%frkrow (i,l)
        frspak (ioff+i,l) = phys_arrays%frsrow (i,l)
        mltipak(ioff+i,l) = phys_arrays%mltirow(i,l)
        mltspak(ioff+i,l) = phys_arrays%mltsrow(i,l)
        raclpak(ioff+i,l) = phys_arrays%raclrow(i,l)
        rainpak(ioff+i,l) = phys_arrays%rainrow(i,l)
        sacipak(ioff+i,l) = phys_arrays%sacirow(i,l)
        saclpak(ioff+i,l) = phys_arrays%saclrow(i,l)
        snowpak(ioff+i,l) = phys_arrays%snowrow(i,l)
        subpak (ioff+i,l) = phys_arrays%subrow (i,l)
        sedipak(ioff+i,l) = phys_arrays%sedirow(i,l)
        rliqpak(ioff+i,l) = phys_arrays%rliqrow(i,l)
        ricepak(ioff+i,l) = phys_arrays%ricerow(i,l)
        cliqpak(ioff+i,l) = phys_arrays%cliqrow(i,l)
        cicepak(ioff+i,l) = phys_arrays%cicerow(i,l)
        rlncpak(ioff+i,l) = phys_arrays%rlncrow(i,l)

        rliqpal(ioff+i,l) = phys_arrays%rliqrol(i,l)
        ricepal(ioff+i,l) = phys_arrays%ricerol(i,l)
        cliqpal(ioff+i,l) = phys_arrays%cliqrol(i,l)
        cicepal(ioff+i,l) = phys_arrays%cicerol(i,l)
        rlncpal(ioff+i,l) = phys_arrays%rlncrol(i,l)


      end do
      end do
!
      do i=il1,il2
        vaggpak(ioff+i)   = phys_arrays%vaggrow(i)
        vautpak(ioff+i)   = phys_arrays%vautrow(i)
        vcndpak(ioff+i)   = phys_arrays%vcndrow(i)
        vdeppak(ioff+i)   = phys_arrays%vdeprow(i)
        vevppak(ioff+i)   = phys_arrays%vevprow(i)
        vfrhpak(ioff+i)   = phys_arrays%vfrhrow(i)
        vfrkpak(ioff+i)   = phys_arrays%vfrkrow(i)
        vfrspak(ioff+i)   = phys_arrays%vfrsrow(i)
        vmlipak(ioff+i)   = phys_arrays%vmlirow(i)
        vmlspak(ioff+i)   = phys_arrays%vmlsrow(i)
        vrclpak(ioff+i)   = phys_arrays%vrclrow(i)
        vscipak(ioff+i)   = phys_arrays%vscirow(i)
        vsclpak(ioff+i)   = phys_arrays%vsclrow(i)
        vsubpak(ioff+i)   = phys_arrays%vsubrow(i)
        vsedipak(ioff+i)  = phys_arrays%vsedirow(i)
        reliqpak(ioff+i)  = phys_arrays%reliqrow(i)
        reicepak(ioff+i)  = phys_arrays%reicerow(i)
        cldliqpak(ioff+i) = phys_arrays%cldliqrow(i)
        cldicepak(ioff+i) = phys_arrays%cldicerow(i)
        ctlncpak(ioff+i)  = phys_arrays%ctlncrow(i)
        cllncpak(ioff+i)  = phys_arrays%cllncrow(i)

        reliqpal(ioff+i)  = phys_arrays%reliqrol(i)
        reicepal(ioff+i)  = phys_arrays%reicerol(i)
        cldliqpal(ioff+i) = phys_arrays%cldliqrol(i)
        cldicepal(ioff+i) = phys_arrays%cldicerol(i)
        ctlncpal(ioff+i)  = phys_arrays%ctlncrol(i)
        cllncpal(ioff+i)  = phys_arrays%cllncrol(i)

      end do
#endif
#if defined (aodpth)
       do i=il1,il2
!
        exb1pak(ioff+i) = phys_arrays%exb1row(i)
        exb2pak(ioff+i) = phys_arrays%exb2row(i)
        exb3pak(ioff+i) = phys_arrays%exb3row(i)
        exb4pak(ioff+i) = phys_arrays%exb4row(i)
        exb5pak(ioff+i) = phys_arrays%exb5row(i)
        exbtpak(ioff+i) = phys_arrays%exbtrow(i)
        odb1pak(ioff+i) = phys_arrays%odb1row(i)
        odb2pak(ioff+i) = phys_arrays%odb2row(i)
        odb3pak(ioff+i) = phys_arrays%odb3row(i)
        odb4pak(ioff+i) = phys_arrays%odb4row(i)
        odb5pak(ioff+i) = phys_arrays%odb5row(i)
        odbtpak(ioff+i) = phys_arrays%odbtrow(i)
        odbvpak(ioff+i) = phys_arrays%odbvrow(i)
        ofb1pak(ioff+i) = phys_arrays%ofb1row(i)
        ofb2pak(ioff+i) = phys_arrays%ofb2row(i)
        ofb3pak(ioff+i) = phys_arrays%ofb3row(i)
        ofb4pak(ioff+i) = phys_arrays%ofb4row(i)
        ofb5pak(ioff+i) = phys_arrays%ofb5row(i)
        ofbtpak(ioff+i) = phys_arrays%ofbtrow(i)
        abb1pak(ioff+i) = phys_arrays%abb1row(i)
        abb2pak(ioff+i) = phys_arrays%abb2row(i)
        abb3pak(ioff+i) = phys_arrays%abb3row(i)
        abb4pak(ioff+i) = phys_arrays%abb4row(i)
        abb5pak(ioff+i) = phys_arrays%abb5row(i)
        abbtpak(ioff+i) = phys_arrays%abbtrow(i)
!
        exb1pal(ioff+i) = phys_arrays%exb1rol(i)
        exb2pal(ioff+i) = phys_arrays%exb2rol(i)
        exb3pal(ioff+i) = phys_arrays%exb3rol(i)
        exb4pal(ioff+i) = phys_arrays%exb4rol(i)
        exb5pal(ioff+i) = phys_arrays%exb5rol(i)
        exbtpal(ioff+i) = phys_arrays%exbtrol(i)
        odb1pal(ioff+i) = phys_arrays%odb1rol(i)
        odb2pal(ioff+i) = phys_arrays%odb2rol(i)
        odb3pal(ioff+i) = phys_arrays%odb3rol(i)
        odb4pal(ioff+i) = phys_arrays%odb4rol(i)
        odb5pal(ioff+i) = phys_arrays%odb5rol(i)
        odbtpal(ioff+i) = phys_arrays%odbtrol(i)
        odbvpal(ioff+i) = phys_arrays%odbvrol(i)
        ofb1pal(ioff+i) = phys_arrays%ofb1rol(i)
        ofb2pal(ioff+i) = phys_arrays%ofb2rol(i)
        ofb3pal(ioff+i) = phys_arrays%ofb3rol(i)
        ofb4pal(ioff+i) = phys_arrays%ofb4rol(i)
        ofb5pal(ioff+i) = phys_arrays%ofb5rol(i)
        ofbtpal(ioff+i) = phys_arrays%ofbtrol(i)
        abb1pal(ioff+i) = phys_arrays%abb1rol(i)
        abb2pal(ioff+i) = phys_arrays%abb2rol(i)
        abb3pal(ioff+i) = phys_arrays%abb3rol(i)
        abb4pal(ioff+i) = phys_arrays%abb4rol(i)
        abb5pal(ioff+i) = phys_arrays%abb5rol(i)
        abbtpal(ioff+i) = phys_arrays%abbtrol(i)
!
        exs1pak(ioff+i) = phys_arrays%exs1row(i)
        exs2pak(ioff+i) = phys_arrays%exs2row(i)
        exs3pak(ioff+i) = phys_arrays%exs3row(i)
        exs4pak(ioff+i) = phys_arrays%exs4row(i)
        exs5pak(ioff+i) = phys_arrays%exs5row(i)
        exstpak(ioff+i) = phys_arrays%exstrow(i)
        ods1pak(ioff+i) = phys_arrays%ods1row(i)
        ods2pak(ioff+i) = phys_arrays%ods2row(i)
        ods3pak(ioff+i) = phys_arrays%ods3row(i)
        ods4pak(ioff+i) = phys_arrays%ods4row(i)
        ods5pak(ioff+i) = phys_arrays%ods5row(i)
        odstpak(ioff+i) = phys_arrays%odstrow(i)
        odsvpak(ioff+i) = phys_arrays%odsvrow(i)
        ofs1pak(ioff+i) = phys_arrays%ofs1row(i)
        ofs2pak(ioff+i) = phys_arrays%ofs2row(i)
        ofs3pak(ioff+i) = phys_arrays%ofs3row(i)
        ofs4pak(ioff+i) = phys_arrays%ofs4row(i)
        ofs5pak(ioff+i) = phys_arrays%ofs5row(i)
        ofstpak(ioff+i) = phys_arrays%ofstrow(i)
        abs1pak(ioff+i) = phys_arrays%abs1row(i)
        abs2pak(ioff+i) = phys_arrays%abs2row(i)
        abs3pak(ioff+i) = phys_arrays%abs3row(i)
        abs4pak(ioff+i) = phys_arrays%abs4row(i)
        abs5pak(ioff+i) = phys_arrays%abs5row(i)
        abstpak(ioff+i) = phys_arrays%abstrow(i)
!
        exs1pal(ioff+i) = phys_arrays%exs1rol(i)
        exs2pal(ioff+i) = phys_arrays%exs2rol(i)
        exs3pal(ioff+i) = phys_arrays%exs3rol(i)
        exs4pal(ioff+i) = phys_arrays%exs4rol(i)
        exs5pal(ioff+i) = phys_arrays%exs5rol(i)
        exstpal(ioff+i) = phys_arrays%exstrol(i)
        ods1pal(ioff+i) = phys_arrays%ods1rol(i)
        ods2pal(ioff+i) = phys_arrays%ods2rol(i)
        ods3pal(ioff+i) = phys_arrays%ods3rol(i)
        ods4pal(ioff+i) = phys_arrays%ods4rol(i)
        ods5pal(ioff+i) = phys_arrays%ods5rol(i)
        odstpal(ioff+i) = phys_arrays%odstrol(i)
        odsvpal(ioff+i) = phys_arrays%odsvrol(i)
        ofs1pal(ioff+i) = phys_arrays%ofs1rol(i)
        ofs2pal(ioff+i) = phys_arrays%ofs2rol(i)
        ofs3pal(ioff+i) = phys_arrays%ofs3rol(i)
        ofs4pal(ioff+i) = phys_arrays%ofs4rol(i)
        ofs5pal(ioff+i) = phys_arrays%ofs5rol(i)
        ofstpal(ioff+i) = phys_arrays%ofstrol(i)
        abs1pal(ioff+i) = phys_arrays%abs1rol(i)
        abs2pal(ioff+i) = phys_arrays%abs2rol(i)
        abs3pal(ioff+i) = phys_arrays%abs3rol(i)
        abs4pal(ioff+i) = phys_arrays%abs4rol(i)
        abs5pal(ioff+i) = phys_arrays%abs5rol(i)
        abstpal(ioff+i) = phys_arrays%abstrol(i)
!
       end do
#endif
#if defined use_cosp
! cosp input
      do l=1,ilev
         do i=il1,il2
            rmixpak (ioff+i,l) = phys_arrays%rmixrow (i,l)
            smixpak (ioff+i,l) = phys_arrays%smixrow (i,l)
            rrefpak (ioff+i,l) = phys_arrays%rrefrow (i,l)
            srefpak (ioff+i,l) = phys_arrays%srefrow (i,l)
         end do ! i
      end do ! l

! cosp output

! put the accumulating arrays from cosp into the output arrays

      do i = il1, il2

! isccp fields
         if (lalbisccp) &
         o_albisccp(ioff+i) = albisccp(i)

         if (ltauisccp) &
         o_tauisccp(ioff+i) = tauisccp(i)

         if (lpctisccp) &
         o_pctisccp(ioff+i) = pctisccp(i)

         if (lcltisccp) &
         o_cltisccp(ioff+i) = cltisccp(i)


         if (lmeantbisccp) &
         o_meantbisccp(ioff+i) = meantbisccp(i)

         if (lmeantbclrisccp) &
         o_meantbclrisccp(ioff+i) = meantbclrisccp(i)

         if (lisccp_sim) &
         o_sunl(ioff+i) = sunl(i)

! calipso fields

         if (lclhcalipso) then
            o_clhcalipso(ioff+i)    = clhcalipso(i)
            ocnt_clhcalipso(ioff+i) = cnt_clhcalipso(i)
         end if

         if (lclmcalipso) then
            o_clmcalipso(ioff+i)    = clmcalipso(i)
            ocnt_clmcalipso(ioff+i) = cnt_clmcalipso(i)
         end if

         if (lcllcalipso) then
            o_cllcalipso(ioff+i)    = cllcalipso(i)
            ocnt_cllcalipso(ioff+i) = cnt_cllcalipso(i)
         end if

         if (lcltcalipso) then
            o_cltcalipso(ioff+i)    = cltcalipso(i)
            ocnt_cltcalipso(ioff+i) = cnt_cltcalipso(i)
         end if

         if (lclhcalipsoliq) then
            o_clhcalipsoliq(ioff+i)    = clhcalipsoliq(i)
            ocnt_clhcalipsoliq(ioff+i) = cnt_clhcalipsoliq(i)
         end if

         if (lclmcalipsoliq) then
            o_clmcalipsoliq(ioff+i)    = clmcalipsoliq(i)
            ocnt_clmcalipsoliq(ioff+i) = cnt_clmcalipsoliq(i)
         end if

         if (lcllcalipsoliq) then
            o_cllcalipsoliq(ioff+i)    = cllcalipsoliq(i)
            ocnt_cllcalipsoliq(ioff+i) = cnt_cllcalipsoliq(i)
         end if

         if (lcltcalipsoliq) then
            o_cltcalipsoliq(ioff+i)    = cltcalipsoliq(i)
            ocnt_cltcalipsoliq(ioff+i) = cnt_cltcalipsoliq(i)
         end if

         if (lclhcalipsoice) then
            o_clhcalipsoice(ioff+i)    = clhcalipsoice(i)
            ocnt_clhcalipsoice(ioff+i) = cnt_clhcalipsoice(i)
         end if

         if (lclmcalipsoice) then
            o_clmcalipsoice(ioff+i)    = clmcalipsoice(i)
            ocnt_clmcalipsoice(ioff+i) = cnt_clmcalipsoice(i)
         end if

         if (lcllcalipsoice) then
            o_cllcalipsoice(ioff+i)    = cllcalipsoice(i)
            ocnt_cllcalipsoice(ioff+i) = cnt_cllcalipsoice(i)
         end if

         if (lcltcalipsoice) then
            o_cltcalipsoice(ioff+i)    = cltcalipsoice(i)
            ocnt_cltcalipsoice(ioff+i) = cnt_cltcalipsoice(i)
         end if

         if (lclhcalipsoun) then
            o_clhcalipsoun(ioff+i)    = clhcalipsoun(i)
            ocnt_clhcalipsoun(ioff+i) = cnt_clhcalipsoun(i)
         end if

         if (lclmcalipsoun) then
            o_clmcalipsoun(ioff+i)    = clmcalipsoun(i)
            ocnt_clmcalipsoun(ioff+i) = cnt_clmcalipsoun(i)
         end if

         if (lcllcalipsoun) then
            o_cllcalipsoun(ioff+i)    = cllcalipsoun(i)
            ocnt_cllcalipsoun(ioff+i) = cnt_cllcalipsoun(i)
         end if

         if (lcltcalipsoun) then
            o_cltcalipsoun(ioff+i)    = cltcalipsoun(i)
            ocnt_cltcalipsoun(ioff+i) = cnt_cltcalipsoun(i)
         end if


! cloudsat+calipso fields
         if (lcltlidarradar) then
            o_cltlidarradar(ioff+i)    = cltlidarradar(i)
            ocnt_cltlidarradar(ioff+i) = cnt_cltlidarradar(i)
         end if
      end do


! several special fields > 1d

! isccp
      if (lclisccp) then
         do ip = 1, 7           ! nptop
            do it = 1, 7        ! ntaucld
               do i = il1, il2
                  o_clisccp(ioff+i,it,ip) = clisccp(i,it,ip)

               end do
            end do
         end do
      end if

! parasol
      if (lparasolrefl) then
         do ip = 1, parasol_nrefl
            do i = il1, il2
               o_parasol_refl(ioff+i,ip)    =  parasol_refl(i,ip)
               ocnt_parasol_refl(ioff+i,ip) =  cnt_parasol_refl(i,ip)
            end do
         end do
      end if

! 3d fields that might be interpolated to special levels or not
      if (use_vgrid) then       ! interpolate to specific heights
         do n = 1, nlr
            do i = il1, il2
               if (lclcalipso) then
                  o_clcalipso(ioff+i,n)    = clcalipso(i,n)
                  ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                  o_clcalipso2(ioff+i,n)    = clcalipso2(i,n)
                  ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      else
         do n = 1, ilev
            do i = il1, il2
               if (lclcalipso) then
                   o_clcalipso(ioff+i,n) =  clcalipso(i,n)
                   ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                   o_clcalipso2(ioff+i,n) = clcalipso2(i,n)
                   ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      end if

         do n = 1, lidar_ntemp
            do i = il1, il2
               if (lclcalipsotmp) then
                  o_clcalipsotmp(ioff+i,n) = clcalipsotmp(i,n)
                  ocnt_clcalipsotmp(ioff+i,n) = cnt_clcalipsotmp(i,n)
               end if

               if (lclcalipsotmpliq) then
                  o_clcalipsotmpliq(ioff+i,n) = clcalipsotmpliq(i,n)
                  ocnt_clcalipsotmpliq(ioff+i,n) = &
                                        cnt_clcalipsotmpliq(i,n)
               end if

               if (lclcalipsotmpice) then
                  o_clcalipsotmpice(ioff+i,n) = clcalipsotmpice(i,n)
                  ocnt_clcalipsotmpice(ioff+i,n) = &
                                        cnt_clcalipsotmpice(i,n)
               end if

               if (lclcalipsotmpun) then
                  o_clcalipsotmpun(ioff+i,n) = clcalipsotmpun(i,n)
                  ocnt_clcalipsotmpun(ioff+i,n) = &
                                         cnt_clcalipsotmpun(i,n)
               end if
            end do ! i
         end do ! n

! cfads (2d histograms)

! calipso

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      else
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      end if

! cloudsat

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize)= &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      else
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize) = &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      end if
      if (lceres_sim) then
         do ip = 1, nceres
            do i = il1, il2
               o_ceres_cf(ioff+i,ip)    = ceres_cf(i,ip)
               o_ceres_cnt(ioff+i,ip)   = ceres_cnt(i,ip)
               o_ceres_ctp(ioff+i,ip)   = ceres_ctp(i,ip)
               o_ceres_tau(ioff+i,ip)   = ceres_tau(i,ip)
               o_ceres_lntau(ioff+i,ip) = ceres_lntau(i,ip)
               o_ceres_lwp(ioff+i,ip)   = ceres_lwp(i,ip)
               o_ceres_iwp(ioff+i,ip)   = ceres_iwp(i,ip)
               o_ceres_cfl(ioff+i,ip)   = ceres_cfl(i,ip)
               o_ceres_cfi(ioff+i,ip)   = ceres_cfi(i,ip)
               o_ceres_cntl(ioff+i,ip)  = ceres_cntl(i,ip)
               o_ceres_cnti(ioff+i,ip)  = ceres_cnti(i,ip)
               o_ceres_rel(ioff+i,ip)   = ceres_rel(i,ip)
               o_ceres_cdnc(ioff+i,ip)  = ceres_cdnc(i,ip)
               o_ceres_clm(ioff+i,ip)   = ceres_clm(i,ip)
               o_ceres_cntlm(ioff+i,ip) = ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if
      if (lceres_sim .and. lswath) then
         do ip = 1, nceres
            do i = il1, il2
               os_ceres_cf(ioff+i,ip)    = s_ceres_cf(i,ip)
               os_ceres_cnt(ioff+i,ip)   = s_ceres_cnt(i,ip)
               os_ceres_ctp(ioff+i,ip)   = s_ceres_ctp(i,ip)
               os_ceres_tau(ioff+i,ip)   = s_ceres_tau(i,ip)
               os_ceres_lntau(ioff+i,ip) = s_ceres_lntau(i,ip)
               os_ceres_lwp(ioff+i,ip)   = s_ceres_lwp(i,ip)
               os_ceres_iwp(ioff+i,ip)   = s_ceres_iwp(i,ip)
               os_ceres_cfl(ioff+i,ip)   = s_ceres_cfl(i,ip)
               os_ceres_cfi(ioff+i,ip)   = s_ceres_cfi(i,ip)
               os_ceres_cntl(ioff+i,ip)  = s_ceres_cntl(i,ip)
               os_ceres_cnti(ioff+i,ip)  = s_ceres_cnti(i,ip)
               os_ceres_rel(ioff+i,ip)   = s_ceres_rel(i,ip)
               os_ceres_cdnc(ioff+i,ip)  = s_ceres_cdnc(i,ip)
               os_ceres_clm(ioff+i,ip)   = s_ceres_clm(i,ip)
               os_ceres_cntlm(ioff+i,ip) = s_ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if

! misr
      if (lclmisr) then
         do i = il1, il2
            o_misr_cldarea(ioff+i)   = misr_cldarea(i)
            o_misr_mean_ztop(ioff+i) = misr_mean_ztop(i)
         end do ! i

         do ip = 1,misr_n_cth
            do i = il1, il2
               o_dist_model_layertops(ioff+i,ip) = &
                                     dist_model_layertops(i,ip)
            end do ! i
         end do ! ip

         ibox = 1
         do ip = 1,misr_n_cth
            do it = 1, 7
               do i = il1, il2
                  o_fq_misr_tau_v_cth(ioff+i,ibox) = &
                                     fq_misr_tau_v_cth(i,ibox)
               end do ! i
               ibox = ibox + 1
            end do ! it
         end do ! ip
      end if ! lclmisr

! modis
      if (lcltmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_total_mean(ioff+i) = &
                              modis_cloud_fraction_total_mean(i)
         end do ! i
      end if

      if (lclwmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_water_mean(ioff+i) = &
                              modis_cloud_fraction_water_mean(i)
         end do ! i
      end if

      if (lclimodis) then
         do i = il1, il2
            o_modis_cloud_fraction_ice_mean(ioff+i) = &
                              modis_cloud_fraction_ice_mean(i)
         end do ! i
      end if

      if (lclhmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_high_mean(ioff+i) = &
                              modis_cloud_fraction_high_mean(i)
         end do ! i
      end if

      if (lclmmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_mid_mean(ioff+i) = &
                              modis_cloud_fraction_mid_mean(i)
         end do ! i
      end if

      if (lcllmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_low_mean(ioff+i) = &
                              modis_cloud_fraction_low_mean(i)
         end do ! i
      end if

      if (ltautmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_mean(ioff+i) = &
                            modis_optical_thickness_total_mean(i)
         end do ! i
      end if

      if (ltauwmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_mean(ioff+i) = &
                            modis_optical_thickness_water_mean(i)
         end do ! i
      end if

      if (ltauimodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_mean(ioff+i) = &
                            modis_optical_thickness_ice_mean(i)
         end do ! i
      end if

      if (ltautlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_logmean(ioff+i) = &
                        modis_optical_thickness_total_logmean(i)
         end do ! i
      end if

      if (ltauwlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_logmean(ioff+i) = &
                         modis_optical_thickness_water_logmean(i)
         end do ! i
      end if

      if (ltauilogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_logmean(ioff+i) = &
                          modis_optical_thickness_ice_logmean(i)
         end do ! i
      end if

      if (lreffclwmodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_water_mean(ioff+i) = &
                        modis_cloud_particle_size_water_mean(i)
         end do ! i
      end if

      if (lreffclimodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_ice_mean(ioff+i) = &
                        modis_cloud_particle_size_ice_mean(i)
         end do ! i
      end if

      if (lpctmodis) then
         do i = il1, il2
            o_modis_cloud_top_pressure_total_mean(ioff+i) = &
                       modis_cloud_top_pressure_total_mean(i)
         end do ! i
      end if

      if (llwpmodis) then
         do i = il1, il2
            o_modis_liquid_water_path_mean(ioff+i) = &
                                 modis_liquid_water_path_mean(i)
         end do ! i
      end if

      if (liwpmodis) then
         do i = il1, il2
            o_modis_ice_water_path_mean(ioff+i) = &
                                 modis_ice_water_path_mean(i)
         end do ! i
      end if

      if (lclmodis) then
         ibox = 1
         do ip = 1, a_nummodispressurebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
          o_modis_optical_thickness_vs_cloud_top_pressure(ioff+i,ibox)= &
              modis_optical_thickness_vs_cloud_top_pressure(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrimodis) then
         ibox = 1
         do ip = 1, a_nummodisrefficebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffice(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffice(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrlmodis) then
         ibox = 1
         do ip = 1, a_nummodisreffliqbins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffliq(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffliq(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (llcdncmodis) then
         do i = il1, il2
            o_modis_liq_cdnc_mean(ioff+i) = &
                                 modis_liq_cdnc_mean(i)
            o_modis_liq_cdnc_gcm_mean(ioff+i) = &
                                 modis_liq_cdnc_gcm_mean(i)
            o_modis_cloud_fraction_warm_mean(ioff+i) = &
                      modis_cloud_fraction_warm_mean(i)
         end do ! i
      end if

#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
