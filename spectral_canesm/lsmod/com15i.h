!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * oct 01/2018 - s.kharin. add xsfx/xtvi to ajust.
!     * jul 27/2019 - m.lazare. remove {com,csp,csn} parmsub variables.
!     * feb 27/2018 - m.lazare. replace {nups,nsups} by {bwgk,qfso} in
!     *                         ajust common block shared between
!     *                         gcm18.dk and core18p.dk (hence don't have
!     *                         to be passed in the call).
!     * may 05/2010 - m.lazare. new version for gcm15i:
!     *                         - add "MW" array to "IXCONT" common
!     *                           block (read-in as namelist).
!     * feb 23/2009 - m.lazare. previous version com15h for gcm15h:
!     *                         - remove qctoff from consq common block.
!     * dec 23/2007 - m.lazare. previous version com15g for gcm15g:
!     *                         - "SL" and "SLSAVE" conditional
!     *                            code removed.
!     *                         - unused workspace pointers for
!     *                           s/l and physics removed.
!     * dec 23/2006 - m.lazare. previous version com15f for gcm15f.
!     *                         - add ihfsave common deck.
!     * jul 02/2006 - m.lazare. - paramsd common block (native real)
!     *                           instead of params (real*8 :: for now,
!     *                           physics).
!     *                         - unused common block "ECCENT" removed.
!     *                         - common block "EPSICE" removed since
!     *                           only qmin was used and it is now
!     *                           passed directly through all the calls
!     *                           to the core routines.
!     * apr 01/2004 - j.scinocca/ previous version com13c for gcm15c/d/e.
!     *               m.lazare. - "CONTRAC" added to "IXCONT" common block.
!     *                         - "ITRLVS" and "ITRLVF" added to ixcont.
!     *                         - "FIZ" removed.
!     *                         - lc,lg,lo moved to gcm driver.
!     *                         - "ISTR" added to "INTVLS" common block.
!     *                         - "IT" component of "SCPNT" common block
!     *                           removed (unused). same for its length.
!     *                         - "CONSQ" common block modified to add
!     *                           additional parameters for conservation.
!     *                         - ntraca used instead of ntrac for calculation
!     *                           of spectral advection and swapping arrays.
!     * nov 28/2003 - m.lazare/  new version for the ibm (com13b):
!     *               l.solheim.
!     *                       - substitute parameter statement variables
!     *                         instead of gcmparm variables.
!     *                       - revise "GAUSS" common block to now contain
!     *                         "A" fields ("A" for "ALL" nodes) and
!     *                         remove "X" workspace.
!     *                       - add "SPEC1", "SPEC2", "SPEC3" workspace.
!     *                       - "P2" removed.
!     *                       - call to "COMPAK7A" removed.
!     *                       - ilat->nlat in "SIZES" common block.
!     *                       - "ILG" -> "ILGSL" for s/l.
!     *                       - "MULTI" conditional code removed.
!     *                       - all "THREADPRIVATE" common blocks moved to new
!     *                         com13pb or com13db. code reorganized.
!     *                       - "TIMING" code removed.
!     *                       - TASKLOCAL'S CONVERTED TO THREADPRIVATE'S.
!     *                       - declaration of sptp,sptp_p,sptp_d,iptn
!     *                         moved to gcm13b since dma moved outside
!     *                         of core routines.
!     *                       - ozzx,ozzxj removed.
!     *                       - "LO" added to "LSCOM" common block for
!     *                         level information of input ozone field.
!     * oct 02/01 - m.lazare. previous common deck com13a for multi-tasked s/l
!     *                       for gcm13a.
!====================================================================
!     * complex :: spectral arrays.
!
complex :: p !<
complex :: c !<
complex :: t !<
complex :: es !<
complex :: pt !<
complex :: ct !<
complex :: tt !<
complex :: est !<
complex :: phis !<
complex :: press !<
complex :: ps !<
complex :: pst !<
complex :: trac !<
complex :: tract !<
!==================================================================
! physical (adjustable) parameters
!
! define and document here any adjustable parameters.
! this should be variable described using the doxygen format above as
! well as a description of its minimum/default/maximum.
!
! here is an example,
!
! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
!           !! It is compute differently when using bulk or PAM aerosols.
!           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
!==================================================================
common /sp/ phis (la)
common /sp/ tt   (rl),est  (rs)
common /sp/ tract(rl,ntraca)
common /sp/ pt   (rl),ct   (rl)
common /sp/ pst  (la)
common /sp/ press(la)
common /sp/ ps   (la)
common /sp/ p    (rl),c    (rl)
common /sp/ t    (rl),es   (rs)
common /sp/ trac (rl,ntraca)
!====================================================================
!     * arrays used to hold intermediate values of transforms.
!     * "SPEC1" is filled prior to forward legendre transform loops.
!
!     * "SPEC2" is similar to "SPEC1" but only applies to {p,c} as
!     * part of the transform process.
!
!     * "WRKT" is work space used in swapping routines.
!
complex :: spec1(3 * ilev + levs + 1 + ntraca * ilev,la) !<
complex :: spec2(2 * ilev,la) !<
complex :: wrkt( (la + 1) * (3 * ilev + levs + 2 + ntraca * ilev)   ) !<
!====================================================================
!     * gaussian fields (physics grid).
!
!     * these are complete sets.
!
real*8 :: sla !<
real*8 :: cla !<
real*8 :: wla !<
real*8 :: wocsla !<
real*8 :: radla !<
common /gaus/ sla    (nlat)
common /gaus/ cla    (nlat)
common /gaus/ wla    (nlat)
common /gaus/ wocsla (nlat)
common /gaus/ radla  (nlat)
!
real*8 :: slda !<
real*8 :: clda !<
real*8 :: wlda !<
real*8 :: wocslda !<
real*8 :: radlda !<
common /gaus/ slda   (nlatd)
common /gaus/ clda   (nlatd)
common /gaus/ wlda   (nlatd)
common /gaus/ wocslda(nlatd)
common /gaus/ radlda (nlatd)
!
real*8 :: slx !<
real*8 :: clx !<
real*8 :: wlx !<
real*8 :: wocslx !<
real*8 :: radlx !<
common /gaus/ slx    (nlat)
common /gaus/ clx    (nlat)
common /gaus/ wlx    (nlat)
common /gaus/ wocslx (nlat)
common /gaus/ radlx  (nlat)
!
real*8 :: sldx !<
real*8 :: cldx !<
real*8 :: wldx !<
real*8 :: wocsldx !<
real*8 :: radldx !<
common /gaus/ sldx   (nlatd)
common /gaus/ cldx   (nlatd)
common /gaus/ wldx   (nlatd)
common /gaus/ wocsldx(nlatd)
common /gaus/ radldx (nlatd)
!
!     * these are defined locally on each node.
!
real*8 :: sl !<
real*8 :: cl !<
real*8 :: wl !<
real*8 :: wocsl !<
real*8 :: radl !<
common /gaus/ sl     (nlatj,ntask_tp)
common /gaus/ cl     (nlatj,ntask_tp)
common /gaus/ wl     (nlatj,ntask_tp)
common /gaus/ wocsl  (nlatj,ntask_tp)
common /gaus/ radl   (nlatj,ntask_tp)
!
real*8 :: sld !<
real*8 :: cld !<
real*8 :: wld !<
real*8 :: wocsld !<
real*8 :: radld !<
common /gaus/ sld    (nlatjd,ntask_td)
common /gaus/ cld    (nlatjd,ntask_td)
common /gaus/ wld    (nlatjd,ntask_td)
common /gaus/ wocsld (nlatjd,ntask_td)
common /gaus/ radld  (nlatjd,ntask_td)

real*8 :: radbu !<
real*8 :: radbd !<
real*8 :: radbxu !<
real*8 :: radbxd !<
real*8 :: radbx !<
common /gaus/ radbu (nlatj,ntask_tp)
common /gaus/ radbd (nlatj,ntask_tp)
common /gaus/ radbxu(nlat)
common /gaus/ radbxd(nlat)
common /gaus/ radbx (nlat + 1)

!====================================================================
!     * transforms and implicit arrays.  alp=(la+lm), ai=(nsmax*ilev)
!
real*8 :: alp !<
real*8 :: dalp !<
real*8 :: delalp !<
real*8 :: epsi !<
real*8 :: alpd !<
real*8 :: dalpd !<
real*8 :: delalpd !<
real*8 :: alpx !<
real*8 :: dalpx !<
real*8 :: delalpx !<
real*8 :: alpdx !<
real*8 :: dalpdx !<
real*8 :: delalpdx !<
!
common /alpcom/ alp    (nlat,iram)
common /alpcom/ dalp   (nlat,iram)
common /alpcom/ delalp (nlat,iram)
common /alpcom/ epsi   (iram)
!
common /alpcomd/alpd   (nlatd,iram)
common /alpcomd/dalpd  (nlatd,iram)
common /alpcomd/delalpd(nlatd,iram)
!
common /alpcomx/alpx   (nlat,iram), dalpx(nlat,iram)
common /alpcomx/delalpx(nlat,iram)
!
common /alpcomdx/alpdx   (nlatd,iram), dalpdx(nlatd,iram)
common /alpcomdx/delalpdx(nlatd,iram)
!====================================================================
!     * water budget conservation fields aggregated-up are dimensioned by the
!     * number of physics tasks.
!
common /ajust/bwgk(ntask_p),qfso(ntask_p), &
      xsfx(ntrac,ntask_p),xtvi(ntrac,ntask_p)
!====================================================================
!     * pointers to scratch arrays used in vectorization.
!     * now defined in the common block /scpnt/
!
common /scpnt/id (60), ir (60), ie (60)
common /scpnt/ixd(60), ixp(60)
!
common /length_sc/ len_ir, len_ie, len_maxd, len_maxp
!
common /lscom/ lsrtotal(2,lmtotal + 1), mindex(lm)
common /lscom/ lsr(2,lm + 1), ls(ilev), lh(ilev)
!
!     * various fields associated to the vertical dicretization.
!
common /vertcd/ sgb(ilev),shb(ilev),sg(ilev)  ,sh(ilev)
common /vertcd/ acg(ilev),bcg(ilev),ach (ilev),bch (ilev)
common /vertcd/ ag (ilev),ah (ilev),bg  (ilev),bh  (ilev)
common /vertcd/ db (ilev),d2b(ilev)
!=====================================================================
!     * common blocks for model parameters and trigo. func. for vfft.
!
logical :: lrcm !<
common /fftrig/ ifax(10),trigs(ilgsl)
common/fftrigd/ ifaxd(10),trigsd(ilgsld)
common /proces/ idiv
common /parahd/ disp,disc,dist,dises,disx, scrit
common /sizes/  lonslx,nlatx,ilevx,levsx,lrlmt,icoord,lay, &
                 ptoit
common /intvls/ issp,isgg,israd,isbeg,isen,istr,iepr,icsw,iclw
common /ihfsave/ishf,ismon,isst
common/ioption/ iocean,icemod,itrac
common /model/  lrcm
!
!     * common block for moisture conservation (shared with
!     * physic_,rstart_,ingcm_ and enersv_).
!
common /consq/ qsrcrat,qsrcrm1,wpp,wp0,wpm,tficqm,qsrc0,qsrcm, &
                qscl0,qsclm
!
!     * common block for keeping track of "REAL" time, to be used
!     * in amip-like runs.
!
common /keeptim/ iyear,imdh,myrssti,isavdts
!
! * indxna, indxa - index values of nonadvected/advected spectral tracers
!
integer :: indxna !<
integer :: indxa !<
common /index/ indxna(ntracn)
common /index/ indxa (ntraca)
!
!     * tracna - two time-level storage of non-advected tracers on the grid.
!
common /naflds/tracna(ip0j * ilev * ntracn + 1,2)

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
