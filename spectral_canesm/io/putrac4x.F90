!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putrac4x(nf,trac,la,lrlmt,ilev,lh,kount,lstr, &
                    lsrtotal,latotal,lmtotal,gll, &
                    itrac,ntrspec,ntrac,indxa)

  !     * nov 04/03 - m.lazare.   new version to support mpi, which uses
  !     *                         new variable "IPIOSW" and work array "GLL".
  !     * oct 31/02 - j.scinocca. previous version putrac4a.

  !     * save tracer fields onto model spectral history file,
  !     * if itrac/=0.
  !
  use tracers_info_mod, only: modl_tracers

  implicit none
  integer :: ibuf2
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: imdh
  integer :: ipio
  integer :: isavdts
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: iyear
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: lmtotal
  integer, intent(in) :: lrlmt
  integer :: max
  integer :: myrssti
  integer :: n
  integer :: na
  integer, intent(in) :: nf
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(in) :: ntrspec
  integer :: nc4to8
  !
  integer, intent(in), dimension(2,lmtotal + 1) :: lsrtotal !< Variable description \f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description \f$[units]\f$
  integer, dimension(8) :: ibuf !< Variable description \f$[units]\f$
  !
  complex, intent(inout) :: trac(la,ilev,ntrspec)  !< Variable description \f$[units]\f$
  integer, intent(in), dimension(ntrspec) :: indxa !< Variable description \f$[units]\f$
  !
  complex, intent(inout) :: gll(latotal)           !< Variable description \f$[units]\f$
  !
  logical, intent(in) :: lstr !< Variable description \f$[units]\f$

  logical :: ok

  common /ipario/  ipio
  common /keeptim/ iyear,imdh,myrssti,isavdts
  !-------------------------------------------------------------------------
  if (itrac == 0 .or. .not.lstr)return
  !
  max = 2 * la
  !
  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts /= 0) then
    !        * in 32-bit, this only works until iyear=2147 !
    ibuf2 = 1000000 * iyear + imdh
  else
    ibuf2 = kount
  end if
  !
  call setlab(ibuf,nc4to8("SPEC"),ibuf2,nc4to8("TRAC"),0, - 1,1, &
              lrlmt,0)
  !
  do na = 1,ntrspec
    n = indxa(na)
    ibuf(3) = nc4to8(modl_tracers(n)%name)
    do l = 1,ilev
      ibuf(4) = lh(l)
      call putspn (nf,trac(1,l,na),la, &
                   lsrtotal,latotal,lmtotal,gll, &
                   ibuf,max,ipio,ok)
    end do ! loop 200
  end do ! loop 300
  !-----------------------------------------------------------------------
6026 format(' ',60x,a4,1x,i10,2x,a4,5i6)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
