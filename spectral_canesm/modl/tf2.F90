!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tf2 (x,xm,f,isiz)
  !
  !     * oct 26/87 - r.laprise.
  !     * apply second part of time filter to spectral variable x(isiz).
  !     * f  is the time filter coefficient.
  !     * XM IS X'(K-1) ON INPUT AND X''(K-1) ON OUTPUT.
  !     * x  is x (k).
  !
  implicit none
  real, intent(inout) :: f
  integer :: i
  integer, intent(inout) :: isiz

  complex, intent(in) :: x(isiz) !< Variable description\f$[units]\f$
  complex, intent(inout) :: xm(isiz) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  do i=1,isiz
    xm(i) = xm(i) + f*x(i)
  end do ! loop 100
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
