!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oxilab(lx,levox,ioxtyp)

  !     * aug 15/2020 - m.lazare. new routine based on ozlab2.
  !
  !     * defines level index values for input oxidants fields.
  !
  implicit none
  !
  integer, intent(in) :: levox  !< Variable description\f$[units]\f$
  integer, intent(in) :: ioxtyp !< Variable description\f$[units]\f$
  integer, intent(out) , dimension(levox) :: lx !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * pressure-level data for ioztyp=1.
  !
  integer, parameter, dimension(19) :: p1 = (/ -100, -500,   10,   20,   30,    &
                                                50,   70,    100,  150,  200,   &
                                                250,  300,   400,  500,  600,   &
                                                700,  850,   925, 1000 /) !<
  !
  integer :: l !<
  !
  !-----------------------------------------------------------------------
  if (ioxtyp==1) then
    do l=1,levox
      lx(l)=p1(l)
    end do
  else
    call xit('OXILAB',-1)
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
