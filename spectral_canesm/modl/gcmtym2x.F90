!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine gcmtym2x(iday,kount,ndays,nsecs,kfinal,ktotal, &
                          delt,incd,lday,sec2)
  !
  !     * may 24/17 - m.lazare.  for conversion to xc40:
  !     *                        - replace "DTIME" by "CPU_TIME".
  !     * jun 28/04 - l.solheim. expand number of digits for printout of
  !     *                        kount.
  !     * sep 23/03 - m.lazare. add "MPINFO" common block to control only
  !     *                       writing to standard output on node 0.
  !     * jun 16/95 - m.lazare.  - previous version gcmtymc2.
  !     * nov 22/94 - f.majaess. - previous version gcmtymc.

  !     * iday = day of the year
  !     * kount = current timestep number
  !     * ndays = number of days elapsed since the start (kount=0).
  !     * nsecs = number of seconds elapsed in the current day.
  !     * kfinal,ktotal = last step number of this job,run.
  !     * delt = model timestep length (seconds).
  !     * incd = iday progress switch.
  !     * lday = date of next available physics grids.
  !     * sec2 = current c.p. clock time
  !
  implicit none
  real :: cpsec
  real, intent(inout) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: hour
  integer, intent(inout) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer :: idd
  integer :: ihh
  integer :: ihhold
  integer :: imdh
  integer :: imo
  integer :: imoprev
  integer, intent(inout) :: incd
  integer :: isavdts
  integer :: iyear
  integer :: jumpd
  integer, intent(inout) :: kfinal
  integer, intent(inout) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(inout) :: ktotal
  integer, intent(inout) :: lday  !< Julian calendar day of next first-day-of-month \f$[day]\f$
  integer :: mday
  integer :: myrssti
  integer, intent(inout) :: ndays
  integer, intent(inout) :: nsecs
  integer :: nusecs
  real :: sec1
  real, intent(inout) :: sec2

  real*4 , dimension(4) :: tarray !<
  integer*4 :: mynode !<
  character*3 :: month !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  common /mpinfo/ mynode
  common /keeptim/ iyear,imdh,myrssti,isavdts
  !--------------------------------------------------------------------
  !     * calculate "OLD" value of hour.
  !     * incrememt kount by 1 and nsecs by delt.
  !
  ihhold=nsecs/3600
  jumpd=0
  kount=kount+1
  nsecs=nsecs+int(delt)
  !
  !     * if the day changes, increment ndays and reset nsecs.
  !     * iday and imdh are then incremented if incd is not zero.
  !     * there are 86400 seconds in one day.
  !
  imoprev=imdh/10000
  if (nsecs>=86400) then
    nusecs=mod(nsecs,86400)
    jumpd=(nsecs-nusecs)/86400
    nsecs=nusecs
    ndays=ndays+jumpd
    if (incd/=0) then
      iday  = iday+jumpd
    end if
    !
    !     * handle end-of-year case, based on "MYRSSTI".
    !
    if (iday>365) then
      iday=mod(iday,365)
      imo=1
      iyear=iyear+1
      ihh=nsecs/3600
      imdh=10000*imo + 100*iday + ihh
      ihhold=ihh
      jumpd=0
      if (mynode==0) write(6,6030) iyear,imdh
    end if
    !
    !     * increment imdh for possible change in hour.
    !     * print the current time information.
  end if
  hour=float(nsecs)/3600.
  ihh=nsecs/3600
  imdh=imdh + 100*jumpd + (ihh-ihhold)
  !     sec1=sec2
  !     call clock(sec2)
  !     cpsec=sec2-sec1
  !     sec2=dtime(tarray)
  !     cpsec=tarray(1)
  !     sysec=tarray(2)
  !     sec2=cpsec+sysec
  call flush(6)
  sec1=sec2
  call cpu_time(sec2)
  cpsec=sec2-sec1
  !     write(6,*) 'gcmtym2x-aftr- MYNODE,SEC1,SEC2,CPSEC=',
  !    +    mynode,sec1,sec2,cpsec
  call flush(6)
  call idatec(month,mday,iday)
  if (mynode==0) then
    write(6,6010) kount,ndays,cpsec,iday,month,mday,hour
  end if
  !
  !     * if iday increments to lday the model must get
  !     * new physics fields before it can proceed.
  !
  if (incd==0) return
  if (iday/=lday) return
  if (mynode==0) write(6,6020) iday
  if (iday/=1) then
    !
    !        * new month boundary so imdh must be incremented by 1.
    !        * DON'T DO IF JAN 1, SINCE ALREADY DONE IN RE-SETTING ABOVE.
    !
    imo=imoprev+1
    idd=1
    ihh=nsecs/3600
    imdh=10000*imo + 100*idd + ihh
    if (mynode==0) write(6,6030) iyear,imdh
  end if
  return
  !--------------------------------------------------------------------
  6010 format(' ---KOUNT =',i9,'  NDAYS =',i9,'  CP SEC= ',f7.2, &
        3x,'IDAY =',i4,49x,a3,i3,f8.2,' HRS')
  6020 format('0*** NEW PHYSICS FIELDS REQUIRED FOR DAY',i6)
  6030 format('0NEW IYEAR,IMDH = ',i5,5x,i6)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
