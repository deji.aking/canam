!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine find_stations(lond, lonu, latd, latu, & ! input
                               invar, ilg1,ilat, &
                               nstations, lon_stat, lat_stat, &
                               stn_loc) ! output

  implicit none

  !
  ! locate the position of the stations on the gaussian grid
  ! this subroutine can be called in different ways
  ! if the points are not moving with time (fixed observing stations)
  ! call once at the beginning of the run but if the points are moving
  ! (satellite ground track or maybe a ship) call as frequently as needed
  !

  !
  ! parameters
  !
  integer, parameter :: ng = 205120 ! number of grid points in a t213 grid

  !
  ! input data
  !

  integer, intent(in) :: invar !< FLAG IF THE POINTS ARE NOT CHANGING (GROUND SITES)
  ! invar=0 or they are moving (satellite path) invar=1\f$[units]\f$
  integer, intent(in) :: nstations !< NUMBER OF STATIONS\f$[units]\f$
  integer, intent(in) :: ilg1   !< Variable description\f$[units]\f$
  integer, intent(in) :: ilat !< Variable description\f$[units]\f$

  real, intent(in) , dimension(ng) :: latd !< LOWER LATITUDE IN DEGREES\f$[units]\f$
  real, intent(in) , dimension(ng) :: latu !< UPPER LATITUDE IN DEGREES\f$[units]\f$

  real, intent(in) , dimension(ng) :: lond !< LOWER LONGITUDE IN DEGREES\f$[units]\f$
  real, intent(in) , dimension(ng) :: lonu !< UPPER LONGITUDE IN DEGREES\f$[units]\f$

  real, intent(in) , dimension(nstations) :: lon_stat !< STATION LONGITUDES (DEGREES)\f$[units]\f$
  real, intent(in) , dimension(nstations) :: lat_stat !< STATION LATITUDES (DEGREES)\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !
  ! output data
  !

  integer, intent(out) , dimension(nstations) :: stn_loc !< INDEX OF STATION, BASED ON LIST\f$[units]\f$

  !
  ! local data
  !

  integer :: istat !<
  integer :: ij !<
  integer :: maxx !<

  real :: lond_max !<
  real :: lonu_min !<

  ! common block data

  real :: ptoit !<
  integer :: lonsl !<
  integer :: nlat !<
  integer :: ilev !<
  integer :: levs !<
  integer :: lrlmt !<
  integer :: icoord !<
  integer :: lay !<

  common /sizes/  lonsl,nlat,ilev,levs,lrlmt,icoord,lay,ptoit

  maxx = ilg1*nlat

  ! find the maximum and minimum points
  lond_max = maxval(lond(1:maxx))
  lonu_min = minval(lonu(1:maxx))

  !
  ! try to find the stations in the section of the earth on the local processor
  do istat = 1, nstations
    do ij = 1, maxx
      if (lat_stat(istat) <= latu(ij) .and. &
          lat_stat(istat) > latd(ij)) then
        ! works for most points
        if (lon_stat(istat) >= lond(ij) .and. &
            lon_stat(istat) < lonu(ij)) then
          stn_loc(istat) = ij
        end if
        ! special cases for a longitude straddling zero longitude
        if (lon_stat(istat) <= lond_max .and. &
            lon_stat(istat) < lonu_min) then
          stn_loc(istat) = ij
        end if
        if (lon_stat(istat) >= lond_max) then
          stn_loc(istat) = ij
        end if
      end if
    end do ! ij
  end do ! istat

  return
end subroutine find_stations
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
