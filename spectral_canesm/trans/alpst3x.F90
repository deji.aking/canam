!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine alpst3x(alp,lsr,lm,sinlat,epsi,nlat,iram)
  !
  !     * oct 29/03 - m. lazare. reverse order of polynomials
  !     *                        and do over all latitudes.
  !     * oct 15/92 - previous version alpst3.
  !     * jul 14/92 - previous version alpst2.
  !
  !     * puts legendre polynomials in alp for one latitude.
  !     * sinlat is the sine of the latitude(s).
  !     * epsi is a field of precomputed constants.
  !     * lsr contains row length info for alp,epsi.
  !
  implicit none
  real*8 :: a
  real*8 :: b
  real*8 :: cos2
  real*8 :: fm
  integer, intent(in) :: iram
  integer :: k
  integer :: kl
  integer :: kl2
  integer :: kr
  integer, intent(in) :: lm
  integer :: m
  integer :: nj
  integer, intent(in) :: nlat
  real*8 :: prod
  integer, intent(in) :: lsr (2,lm+1) !<
  real*8, intent(inout) , dimension(nlat,iram) :: alp !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(iram) :: epsi !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: sinlat !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  do nj=1,nlat
    cos2=1.-sinlat(nj)**2
    prod=1.
    a=1.
    b=0.
    !
    !       * loop over longitudinal wave numbers.
    !
    do m=1,lm
      fm=float(m-1)
      if (m/=1) then
        a=a+2.
        b=b+2.
        prod=prod*cos2*a/b
      end if
      !         * compute the first value in the row.
      !         * compute the second element only if needed.
      !
      kl=lsr(2,m)
      kr=lsr(2,m+1)-1
      alp(nj,kl)=sqrt(.5*prod)
      if (kr>kl) alp(nj,kl+1)=sqrt(2.*fm+3.)*sinlat(nj)*alp(nj,kl)
      !
      !         * compute the rest of the values in the row if needed.
      !
      kl2=kl+2
      if (kl2>kr) cycle
      do k=kl2,kr
        alp(nj,k)=(sinlat(nj)*alp(nj,k-1)-epsi(k-1)*alp(nj,k-2))/ &
                 epsi(k)
      end do ! loop 220
    end do ! loop 230
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
