!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swaps2f(s,wrks,s1,la,ilev,hoskf)

  !     * dec 10/2004 - j.scinocca. add hoskins spectral filter of
  !     *                           input fields.
  !     * nov 20/98 - m.lazare. previous version swaps2.
  !     *                       like routine swaps except
  !     *                       explicitly coded to go from
  !     *                       s to wrks. also added work
  !     *                       array "WRKS1" with index
  !     *                       of "LA+1" to avoid potential
  !     *                       bank conflicts.
  !
  !     * transpose s(2,la,ilev) to wrks(2,ilev,la)
  !
  implicit none
  real :: flt
  integer :: i
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: j
  integer, intent(in) :: la

  real, intent(in), dimension(2,la,ilev) :: s !< Variable description\f$[units]\f$
  real, intent(inout), dimension(2,ilev,la) :: wrks !< Variable description\f$[units]\f$
  real, intent(inout) :: s1(2,la+1,ilev) !< Variable description\f$[units]\f$
  real, intent(in), dimension(2,la) :: hoskf !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !     * first determine if la is an exact multiple of 32 to
  !     * see if bank conflicts may be a problem.
  !
  if (mod(la,32)/=0) then
    do j=1,la
      flt=hoskf(1,j)
      do i=1,ilev
        wrks(1,i,j) = s(1,j,i)*flt
        wrks(2,i,j) = s(2,j,i)*flt
      end do
    end do ! loop 10
  else
    do i=1,ilev
      do j=1,la
        s1(1,j,i) = s(1,j,i)
        s1(2,j,i) = s(2,j,i)
      end do
    end do ! loop 20
    !
    do j=1,la
      flt=hoskf(1,j)
      do i=1,ilev
        wrks(1,i,j) = s1(1,j,i)*flt
        wrks(2,i,j) = s1(2,j,i)*flt
      end do
    end do ! loop 30
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
