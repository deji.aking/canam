!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine legbs2(spec,four1,alp, &
                        wl,wocsl,levs,nlat, &
                        m,ntot)
  !
  !     * sep 11/06 - m. lazare. new version for gcm15f:
  !     *                        - calls new vfastx2 to support 32-bit driver.
  !     * oct 29/03 - m. lazare. previous version legbs2 from ibm conversion:
  !     * oct 29/03 - m. lazare. new routine to do exclusively the backward
  !     *                        legendre transform for moisture, based on
  !     *                        old code in mhanlq_ and using dgemm.
  !     *                        this is used only in semi-lag initializtion
  !     *                        loop.
  !     *
  !     * transform fourier coefficients for moisture to spectral tendencies
  !     * for this (possibly chained) gaussian latitude.
  !
  !     * this is called for a given zonal wavenumber index "M", ie inside
  !     * a loop over m.
  !     *
  !     *    alp = legendre polynomials *gaussian weight
  !     *
  !     * input fields
  !     * ------------
  !     *
  !     * this routine is called within a loop over zonal index "M" and
  !     * thus accesses the input for that particular zonal index from
  !     * the calling array.
  !     *
  !     *    esg --> four1(2,levs,nlat)
  !     *
  !     * output fields
  !     * -------------
  !     *
  !     * the output data fields are the spectral moisture work field,
  !     * accumulated into the following data structure:
  !     *
  !     *           spec(2,levs,ntot)
  !     *
  !
  implicit none
  real :: beta
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: m
  integer, intent(in) :: nlat
  integer, intent(in) :: ntot
  !
  !     * input fourier coefficients.
  !
  real, intent(in), dimension(2,levs,nlat) :: four1 !< Variable description\f$[units]\f$
  !
  !     * output spectral tendencies.
  !
  real, intent(in), dimension(2,levs,ntot) :: spec !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real*8, intent(in) :: alp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wl !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wocsl !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  beta=0.
  call vfastx2(four1,spec,alp, &
                   beta,levs,nlat,ntot)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
