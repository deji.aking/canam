# Makefile that calls mkmf generates all the makefiles and orchestrates compilation of the AGCM
CPP_CONFIG_FILE ?=
CPP_SIZES_FILE ?=
MKMF_TEMPLATE ?=
AGCM_32BIT ?= 1
AGCM_EXE ?= canam.exe
JOBS ?= 16

.PHONY = folders clean
.DEFAULT_GOAL = bin/$(AGCM_EXE)

MKMFDIR = $(abspath ../../CCCma_tools/mkmf/)
MKMF = $(MKMFDIR)/bin/mkmf -v
AGCM_INCLUDE_DIR = $(abspath ./include)

# warn if no cpp/template files were given
ifndef CPP_CONFIG_FILE
$(warning For all targets but 'clean' CPP_CONFIG_FILE must be given to the make call)
endif
ifndef CPP_SIZES_FILE
$(warning For all targets but 'clean' CPP_CONFIG_FILE must be given at the make call)
endif
ifndef MKMF_TEMPLATE
$(warning For all targets but 'clean' MKMF_TEMPLATE must be given at the make call)
endif

# handle 32bit/64bit option
ifeq ($(agcm32bit),off)
    AGCM_32BIT = 0
endif
ifeq ($(agcm32bit),on)
    AGCM_32BIT = 1
endif

CANDIAG_SOURCE  	 = $(foreach f,$(shell sed '/^\#/d' path_names_diag), $(abspath $(f)))
ALWAYS64_SOURCE 	 = $(foreach f,$(shell sed '/^\#/d' path_names_64bit),$(abspath $(f)))
AGCM_SOURCE     	 = $(shell sed '/^\#/d' path_names_agcm)

TARGET_DEPS = folders include/cppdef_config.h include/cppdef_sizes.h mkmf.template

folders:
	mkdir -p deps/CanDIAG
	mkdir -p deps/64bit
	mkdir -p bin

# link in cpp header files to the include directory
include/cppdef_config.h: $(CPP_CONFIG_FILE) folders
	ln -sf $(<) $(@)
include/cppdef_sizes.h:  $(CPP_SIZES_FILE) folders
	ln -sf $(<) $(@)

# link in the general mkmf template
mkmf.template: $(MKMF_TEMPLATE)
	ln -sf $(<) $(@)

# Routines from diagnostics library
deps/CanDIAG/libCanDIAG.a: $(TARGET_DEPS) $(CANDIAG_SOURCE)
	$(MKMF) -t $(abspath mkmf.template) -p $(notdir $(@)) \
			-m $(@D)/Makefile.CanDIAG -o "-I$(AGCM_INCLUDE_DIR)" $(CANDIAG_SOURCE)
	cd $(@D); make -j$(JOBS) -f Makefile.CanDIAG COMPILE_32BIT=$(AGCM_32BIT)

# Routines that MUST be compiled with 64bit
deps/64bit/lib64bit.a: $(TARGET_DEPS) $(ALWAYS64_SOURCE)
	$(MKMF) -t $(abspath mkmf.template) -p $(notdir $(@)) \-m $(@D)/Makefile.64bit -o "-I$(AGCM_INCLUDE_DIR) -D_STATIC_ALLOCATION" $(ALWAYS64_SOURCE)
	cd $(@D); make -j$(JOBS) -f Makefile.64bit COMPILE_32BIT=0

# AGCM
bin/path_names_agcm: folders
	touch $(@)
	$(foreach f,$(AGCM_SOURCE),$(shell echo ../$(f) >> bin/path_names_agcm))

bin/$(AGCM_EXE):  deps/CanDIAG/libCanDIAG.a deps/64bit/lib64bit.a bin/path_names_agcm $(AGCM_SOURCE) $(TARGET_DEPS)
	cd  $(@D); $(MKMF) -t $(abspath mkmf.template) -p $(notdir $(@)) -m Makefile.AGCM		\
		-l "-L../deps/CanDIAG/ -L../deps/64bit/ -lCanDIAG -l64bit"							\
		-o "-I$(AGCM_INCLUDE_DIR) -I../deps/64bit/ -I../deps/CanDIAG -D_STATIC_ALLOCATION"	\
		path_names_agcm
	cd $(@D); make -j$(JOBS) -f Makefile.AGCM COMPILE_32BIT=$(AGCM_32BIT)

# Clean all compiled objects
clean:
	rm -rf bin/ deps/
