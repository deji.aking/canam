!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine init_phys_parm(phys_parm_iu, mynode)
  !***********************************************************************
  ! Read physical parameter values from a file named PHYS_PARM
  !
  ! Input
  !   integer*4 mynode  ...mpi node (used to restrict I/O to node 0)
  !
  ! Jason Cole ...8 Dec 2017
  !***********************************************************************

  use phys_parm_defs
  use iso_fortran_env, only : STDERR=>error_unit
  use iso_fortran_env, only : STDOUT=>output_unit

  implicit none

  !--- Input
  integer*4, intent(in) :: mynode       !<Variable description\f$[units]\f$
  integer,   intent(in) :: phys_parm_iu !<PHYS_PARM file unit number\f$[unit-less]\f$
  !==================================================================
  ! PHYSICAL (ADJUSTABLE) PARAMETERS
  !
  ! Define and document here any adjustable parameters.
  ! This should be variable described using the Doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! Here is an example,
  !
  ! REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--- Local
  integer :: phys_parm_err = 0

  !-----------------------------------------------------------------------
  !--- Set the physical parameter defaults
  pp_solar_const = 1361.0                   ! Units => W/m^2
  pp_rdm_num_pert = 0                       ! Unitless
  pp_mam_damp_gw_time = 1209600.0           ! Units => Seconds
  pp_vtau_background = 1.0E-4               ! Unitless (pre-CMIP6 value)
  pp_ext_sa_background = 14.0E-5            ! Units => m^-1
  pp_e_folding_strat_aerosol = 31536000.0   ! Units => seconds

  print_phys_checksums = .false.            ! If true, print checksums of physici

  !--- Set the default switches
  switch_decay_strat_aerosol = .false. ! Default is .false., no decay.
  switch_solar_refraction = .true.     ! Default is .true., refract direct-beam in solar radiative transfer
  switch_zero_thermal_source = .false. ! Default is .false., include thermal sources in thermal radiative transfer
  switch_gas_chem = .false.            ! Default is .false., do not run gas chemistry
  switch_use_tke = .false.             ! Default is .false., do not use tke
  switch_new_duinit = .false.          ! Default is .false., do not use updated version of routine to initialize soil fields for dust emission

  !--- Set the adjustable parameter defaults
  ap_uicefac = 4200.0
  ap_facaut = 0.1203704
  ap_facacc = 15.0
  ap_cdnc_fac = 100.0
  ap_cdnc_exp = 0.2
  ap_cdnc_scale = 0.6
  ap_ct = 1.00E-3
  ap_scale_scmbf = 0.03
  ap_weight = 0.75
  ap_fmax = 0.0002
  ap_alf = 5.0E8
  ap_taus1 = 21600.0
  ap_c0fac = 0.02
  ap_rkxmin = 0.1
  ap_rkmn = 0.1
  ap_rkhmin = 0.1
  ap_rkqmin = 0.1
  ap_xlmin = 10.0
  ap_almc_min = 600.0
  ap_csigma = 0.2
  ap_scale_cvsg = 3.0E-3
  ap_drngat = 0.1

  !--- Read physical parameter set in parmsub from a namelist
  rewind(phys_parm_iu)
  read(phys_parm_iu,nml=phys_parm)
  if (mynode == 0) then
    write(STDOUT,*)' '
    write(STDOUT,phys_parm)
    write(STDOUT,*)' '
    call flush(STDOUT)
  endif
  rewind(PHYS_PARM_IU)
  read(PHYS_PARM_IU,nml=phys_debug,iostat=phys_parm_err)
  if (mynode == 0 .and. phys_parm_err /= 0) then
    write(STDOUT,*) "WARNING: Unable to read phys_debug namelist from PHYS_PARM"
  endif

  rewind(phys_parm_iu)
  read(phys_parm_iu,nml=switches,iostat=phys_parm_err)
  if (mynode == 0) then
    write(STDOUT,*)' '
    ! make sure the switches namelist was actually found
    if ( phys_parm_err == 0 ) then
      write(STDOUT,*) "init_phys_parm.F90: switches are"
      write(STDOUT,switches)
    else
      write(STDOUT,*) "WARNING: Unable to read switches namelist from", &
                  " PHYS_PARM - using the default values."
    endif
    write(STDOUT,*)' '
    call flush(STDOUT)
  end if

  rewind(phys_parm_iu)
  read(phys_parm_iu,nml=adjustable_parm, iostat=phys_parm_err)
  if (mynode == 0) then
    write(STDOUT,*)' '
    if ( phys_parm_err.eq.0 ) then
      write(STDOUT,adjustable_parm)
    else
      write(6,*) "WARNING: Unable to read the adjustable_parm", &
           " namelist from PHYS_PARM - using the default values."
    end if
    write(STDOUT,*)' '
    call flush(6)
  end if

end subroutine init_phys_parm
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
