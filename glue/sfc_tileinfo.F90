#include "cppdef_config.h"
!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine sfc_tileinfo(nml_funit, mynode)
  !
  !     * aug 10/2017 - m.lazare.    add support for lakes.
  !     * aug 07/2017 - m.lazare.    initial git version.
  !
  use psizes_19

  implicit none

  integer, intent(in) :: nml_funit  !< Namelist input file unit
  integer, intent(in) :: mynode     !<

  integer :: i
  integer :: iktric
  integer :: iktrwt
  integer :: iktulk
  integer :: iltgen
  integer :: iosic
  integer :: iowat
  integer :: irlic
  integer :: irlwt

  integer :: iulak
  integer :: iwtsic
  integer :: iwtwat

  !
  !     * Namelist information for surface tiling:
  !
  namelist /surface_tiles_list/ iltgen, iktulk, iktrwt, iktric, iwtwat, iwtsic
  !
  common /istind/ iulak, irlwt, irlic, iowat, iosic
  !--------------------------------------------------------------------
  !
  !     * tiles.
  !
  !     * first, set defaults.
  !
  iltgen = 1
  iktulk = 1
  iktrwt = 2
  iktric = 3
  iwtwat = 1
  iwtsic = 2
  !
  !     * read namelists.
  !
  rewind(nml_funit)
  read(nml_funit, surface_tiles_list, end=1001)
  !
  !     * write namelists out to log file (master node only).
  !
  if (mynode==0) then
    write(6, nml = surface_tiles_list)
  end if
  !
  !
  !     * calculate tile index locations for both unresolved and
  !     * resolved lake water/ice tiles referenced by the pat/rot
  !     * arrays like farepat/farerot.
  !
  !     * these are stored in the "ISTIND" common block
  !     * which is used in the physics.
  !
  if (iktulk>0) then
     iulak = ntld + iktulk
  else
     iulak = 0
  end if
  !
  if (iktrwt>0) then
     irlwt = ntld + iktrwt
  else
     irlwt = 0
  end if
  !
  if (iktric>0) then
     irlic = ntld + iktric
  else
     irlic = 0
  end if
  !
  !     * calculate tile index locations for ocean
  !     * water/ice tiles referenced by the pat/rot
  !     * arrays like farepat/farerot.
  !
  !     * these are stored in the "ISTIND" common block
  !     * which is used in the physics.
  !
  iowat = ntld + ntlk + iwtwat
  iosic = ntld + ntlk + iwtsic
  !
  return
  !=======================================================================
  1001 call xit('SFC_TILEINFO', -1)

end subroutine sfc_tileinfo
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
