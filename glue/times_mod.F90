!> \file
!> \brief Stores the various model timing and time-interval values.
!!
!! @author Ayodeji Akingunola
!
module times_mod
  implicit none
  real :: delt               !< Timestep for atmospheric model \f$[seconds]\f$
  integer :: iday            !< Julian calendar day of year \f$[day]\f$
  integer :: ifdiff          !<  1 for forward step, 0 for leapfrog step.
  integer :: incd            !<  0 to keep iday fixed, 1 to increment iday normally.
  integer :: kstart
  integer :: ktotal
  integer :: lday            !<  Julian calendar day of next first-day-of-month \f$[day]\f$
  integer :: mday            !< Julian calendar day of next mid-month \f$[day]\f$
  integer :: mdayt           !< Julian calendar day of next mid-month for forcing fields \f$[day]\f$
  integer :: ndays
  integer :: newrun          !< 1 at the start of a new run
                             !< 0 for restart inside a run
  integer :: nsecs

  save
end module times_mod
!> \file
